webpackJsonp([15],{

/***/ 100:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SearchPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_auth_auth__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_api_api__ = __webpack_require__(62);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_http__ = __webpack_require__(84);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__comp_view_comp_view__ = __webpack_require__(50);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






/**
 * Generated class for the SearchPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var SearchPage = (function () {
    function SearchPage(navCtrl, navParams, auth, http, api) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.auth = auth;
        this.http = http;
        this.api = api;
        this.giveawaysInitial = []; //initialize your countriesInitial array empty
        this.giveaways = []; //initialize your countries array empty
        this.searchString = ''; // initialize your searchCountryString string empty
        this.mainview = true;
        var url = "giveaway/giveaway_names";
        if (this.navParams.get('mainview') == undefined) {
            this.mainview = true;
        }
        else {
            this.mainview = this.navParams.get('mainview');
        }
        console.log("mainview", this.mainview);
        this.auth.getData(url).then(function (data) {
            _this.giveawaysInitial = data;
            _this.giveaways = data;
        });
    }
    SearchPage.prototype.searchGiveaway = function (searchbar) {
        // reset countries list with initial call
        this.giveaways = this.giveawaysInitial;
        // set q to the value of the searchbar
        var q = searchbar.target.value;
        // if the value is an empty string don't filter the items
        if (q.trim() == '') {
            return;
        }
        this.giveaways = this.giveaways.filter(function (v) {
            if (v.giveaway_title.toLowerCase().indexOf(q.toLowerCase()) > -1) {
                return true;
            }
            if (v.giveaway_code.toLowerCase().indexOf(q.toLowerCase()) > -1) {
                return true;
            }
            return false;
        });
    };
    SearchPage.prototype.viewCompView = function (id) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__comp_view_comp_view__["a" /* CompViewPage */], { 'giveaway_code': id });
    };
    SearchPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-search',template:/*ion-inline-start:"/Users/localuser/gift-prod/src/pages/search/search.html"*/'<!--\n  Generated template for the SearchPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header no-border>\n\n  <ion-navbar>\n      <button *ngIf="mainview" ion-button menuToggle>\n          <ion-icon name="menu"></ion-icon>\n        </button>\n      <ion-searchbar animated="true" showCancelButton="true" autocomplete="on" autocorrect="on" [(ngModel)]="searchString" (input)="searchGiveaway($event)" \n      placeholder="Search"></ion-searchbar>\n     \n    \n    <!-- <ion-title>Search</ion-title> -->\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n   \n    <ion-list>\n        <button (click)="viewCompView(giveaway.giveaway_code)" class="search-items" ion-item *ngFor="let giveaway of giveaways">\n          {{giveaway.giveaway_code}} - {{giveaway.giveaway_title}}\n        </button>\n    </ion-list>\n</ion-content>\n'/*ion-inline-end:"/Users/localuser/gift-prod/src/pages/search/search.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__providers_auth_auth__["a" /* AuthProvider */], __WEBPACK_IMPORTED_MODULE_4__angular_http__["b" /* Http */], __WEBPACK_IMPORTED_MODULE_3__providers_api_api__["a" /* ApiProvider */]])
    ], SearchPage);
    return SearchPage;
}());

//# sourceMappingURL=search.js.map

/***/ }),

/***/ 101:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return WinningsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_auth_auth__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_storage__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__comp_view_comp_view__ = __webpack_require__(50);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





/**
 * Generated class for the WinningsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var WinningsPage = (function () {
    function WinningsPage(loadingCtrl, navCtrl, navParams, auth, storage) {
        this.loadingCtrl = loadingCtrl;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.auth = auth;
        this.storage = storage;
        this.giveaway_list = [];
        this.has_winnings = false;
        this.presentLoading();
        this.getWonList();
    }
    WinningsPage.prototype.presentLoading = function () {
        this.loader = this.loadingCtrl.create({
            content: "Please wait...",
            duration: 3000
        });
        this.loader.present();
    };
    WinningsPage.prototype.getWonList = function () {
        var _this = this;
        this.storage.get('user_id').then(function (user_id) {
            var url = "giveaway/get_won_list?user_code=" + user_id;
            _this.auth.getData(url).then(function (list) {
                if (list.auth) {
                    _this.has_winnings = true;
                    _this.giveaway_list = list.result;
                }
                else {
                    _this.has_winnings = false;
                }
            });
        }).then(function () {
            _this.loader.dismiss();
        });
    };
    WinningsPage.prototype.viewInfo = function (giveaway_code) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__comp_view_comp_view__["a" /* CompViewPage */], { "giveaway_code": giveaway_code });
    };
    WinningsPage.prototype.doRefresh = function (refresher) {
        this.getWonList();
        setTimeout(function () {
            console.log('Async operation has ended');
            refresher.complete();
        }, 2000);
    };
    WinningsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-winnings',template:/*ion-inline-start:"/Users/localuser/gift-prod/src/pages/winnings/winnings.html"*/'<!--\n  Generated template for the WinningsPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>Winnings</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content  >\n    <p *ngIf="has_winnings == false" text-center>\n        No winnings yet\n      </p>\n    <ion-refresher (ionRefresh)="doRefresh($event)" >\n        <ion-refresher-content></ion-refresher-content>\n      </ion-refresher>\n\n  <ion-list no-lines class="px-10">\n    <ion-item class="winning-items" *ngFor="let item of giveaway_list">\n      <ion-thumbnail item-start class="flex-div">\n          <ion-spinner name="crescent" class="spinner-center"></ion-spinner>\n        <img src="{{ item.giveaway_media_link}}">\n      </ion-thumbnail>\n      <div class="text-giftin">\n          <h4 class="text-16 text-elepsis" >{{ item.giveaway_title }}</h4>\n          <p><small>by {{ item.com_name }}</small></p>\n      </div>\n      \n      <button ion-button outline item-end class="btn-gift-outline" (click)="viewInfo(item.giveaway_code)">View</button>\n    </ion-item>\n  </ion-list>\n\n</ion-content>'/*ion-inline-end:"/Users/localuser/gift-prod/src/pages/winnings/winnings.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__providers_auth_auth__["a" /* AuthProvider */], __WEBPACK_IMPORTED_MODULE_3__ionic_storage__["b" /* Storage */]])
    ], WinningsPage);
    return WinningsPage;
}());

//# sourceMappingURL=winnings.js.map

/***/ }),

/***/ 176:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return EditUserPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_session_session__ = __webpack_require__(34);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_forms__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_auth_auth__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_api_api__ = __webpack_require__(62);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_native_file__ = __webpack_require__(63);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ionic_native_transfer__ = __webpack_require__(133);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__ionic_native_file_path__ = __webpack_require__(134);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__ionic_native_camera__ = __webpack_require__(135);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__ionic_storage__ = __webpack_require__(24);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};











/**
 * Generated class for the EditUserPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var EditUserPage = (function () {
    function EditUserPage(navCtrl, navParams, actionSheetCtrl, session, formBuilder, authProvider, camera, transfer, file, filePath, toastCtrl, platform, loadingCtrl, api, storage, alertCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.actionSheetCtrl = actionSheetCtrl;
        this.session = session;
        this.formBuilder = formBuilder;
        this.authProvider = authProvider;
        this.camera = camera;
        this.transfer = transfer;
        this.file = file;
        this.filePath = filePath;
        this.toastCtrl = toastCtrl;
        this.platform = platform;
        this.loadingCtrl = loadingCtrl;
        this.api = api;
        this.storage = storage;
        this.alertCtrl = alertCtrl;
        //profile pic related
        this.lastImage = null;
        this.isServerImage = true;
        this.submitAttempt = false;
        this.submitPwdAttempt = false;
        //exsisting user data
        this.user_data = [];
        // this.session.getUser().then((res:any) => {
        this.user_data = this.session.user_data;
        // });
        this.lastImage = this.user_data.photo;
        // this.storage.get('user_photo').then((res:any)=>{
        //   this.lastImage = res;
        // });
        console.log("this email", this.user_data.photo);
        // this.user.controls['email'].setValue(this.user_data.email);
        this.user = this.formBuilder.group({
            email: [__WEBPACK_IMPORTED_MODULE_3__angular_forms__["f" /* Validators */].compose([__WEBPACK_IMPORTED_MODULE_3__angular_forms__["f" /* Validators */].required, __WEBPACK_IMPORTED_MODULE_3__angular_forms__["f" /* Validators */].email])],
            first_name: [__WEBPACK_IMPORTED_MODULE_3__angular_forms__["f" /* Validators */].compose([__WEBPACK_IMPORTED_MODULE_3__angular_forms__["f" /* Validators */].required,])],
            last_name: [__WEBPACK_IMPORTED_MODULE_3__angular_forms__["f" /* Validators */].compose([__WEBPACK_IMPORTED_MODULE_3__angular_forms__["f" /* Validators */].required,])],
            mobile_no: [__WEBPACK_IMPORTED_MODULE_3__angular_forms__["f" /* Validators */].compose([__WEBPACK_IMPORTED_MODULE_3__angular_forms__["f" /* Validators */].required,])],
            city: [__WEBPACK_IMPORTED_MODULE_3__angular_forms__["f" /* Validators */].compose([__WEBPACK_IMPORTED_MODULE_3__angular_forms__["f" /* Validators */].required,])],
            country: [__WEBPACK_IMPORTED_MODULE_3__angular_forms__["f" /* Validators */].compose([__WEBPACK_IMPORTED_MODULE_3__angular_forms__["f" /* Validators */].required,])],
            age: [__WEBPACK_IMPORTED_MODULE_3__angular_forms__["f" /* Validators */].compose([__WEBPACK_IMPORTED_MODULE_3__angular_forms__["f" /* Validators */].required,])],
            user_code: this.user_data.user_code
        });
        this.pwdForm = this.formBuilder.group({
            user_code: this.user_data.user_code,
            oldPassword: ['', __WEBPACK_IMPORTED_MODULE_3__angular_forms__["f" /* Validators */].compose([__WEBPACK_IMPORTED_MODULE_3__angular_forms__["f" /* Validators */].required])],
            newPassword: ['', __WEBPACK_IMPORTED_MODULE_3__angular_forms__["f" /* Validators */].compose([__WEBPACK_IMPORTED_MODULE_3__angular_forms__["f" /* Validators */].required])],
            confPassword: ['', __WEBPACK_IMPORTED_MODULE_3__angular_forms__["f" /* Validators */].compose([__WEBPACK_IMPORTED_MODULE_3__angular_forms__["f" /* Validators */].required])],
        }, { validator: this.passwordMismatch });
        // this.lastImage = this.user_data.photo;
    }
    EditUserPage.prototype.passwordMismatch = function (cg) {
        console.log("password checking");
        var pwd1 = cg.get('newPassword');
        var pwd2 = cg.get('confPassword');
        var rv = {};
        if ((pwd1.touched || pwd2.touched) && pwd1.value !== pwd2.value) {
            rv['passwordMismatch'] = true;
        }
        // console.log(rv);
        return rv;
    };
    EditUserPage.prototype.checkOldPassword = function (data) {
        var _this = this;
        var url = "user/check_password";
        this.submitPwdAttempt = true;
        this.authProvider.postAuthData(data, url).then(function (result) {
            if (result.auth) {
                _this.resetPassword(data);
            }
            else {
                var alert_1 = _this.alertCtrl.create({
                    title: "Error",
                    subTitle: result.msg,
                    enableBackdropDismiss: false,
                    buttons: ['OK']
                });
                alert_1.present();
            }
        });
    };
    EditUserPage.prototype.resetPassword = function (data) {
        var _this = this;
        // console.log(data);
        var url = "user/reset_pwd";
        this.submitAttempt = true;
        this.authProvider.postAuthData(data, url).then(function (result) {
            console.log(result);
            if (result.auth) {
                // this.navCtrl.push(LoginPage);
                var alert_2 = _this.alertCtrl.create({
                    title: "Success",
                    subTitle: result.msg,
                    enableBackdropDismiss: false,
                    buttons: ['OK']
                });
                alert_2.present();
            }
            else {
                var alert_3 = _this.alertCtrl.create({
                    title: "Error",
                    subTitle: result.msg,
                    enableBackdropDismiss: false,
                    buttons: ['OK']
                });
                alert_3.present();
            }
        }).catch(function (err) {
            console.log(err);
        });
    };
    EditUserPage.prototype.editProfile = function (data) {
        var _this = this;
        var url = "user/edit_user";
        //  this.submitAttempt = true;
        this.authProvider.postAuthData(data, url).then(function (res) {
            //  console.log(res);
            _this.resetSession();
            _this.presentToast("You have updated your personal infomation successfully");
        });
    };
    EditUserPage.prototype.resetSession = function () {
        var _this = this;
        this.session.getUser().then(function (res) {
            _this.user_data = _this.session.user_data;
        });
    };
    EditUserPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad EditUserPage');
    };
    EditUserPage.prototype.presentActionSheet = function () {
        var _this = this;
        var actionSheet = this.actionSheetCtrl.create({
            title: 'Edit Profile Photo',
            buttons: [
                {
                    text: 'Pick From Gallery',
                    icon: 'images',
                    // role: 'destructive',
                    handler: function () {
                        _this.takePicture(_this.camera.PictureSourceType.PHOTOLIBRARY);
                        console.log('Destructive clicked');
                    }
                }, {
                    text: 'Take a Picture',
                    icon: 'camera',
                    handler: function () {
                        _this.takePicture(_this.camera.PictureSourceType.CAMERA);
                        console.log('Archive clicked');
                    }
                }, {
                    text: 'Delete',
                    icon: 'trash',
                    // role: 'cancel',
                    handler: function () {
                        console.log('Cancel clicked');
                    }
                },
                {
                    text: 'Cancel',
                    role: 'cancel',
                    icon: 'close',
                    handler: function () {
                        console.log('Cancel clicked');
                    }
                }
            ]
        });
        actionSheet.present();
    };
    EditUserPage.prototype.takePicture = function (sourceType) {
        var _this = this;
        // Create options for the Camera Dialog
        var options = {
            quality: 50,
            sourceType: sourceType,
            saveToPhotoAlbum: true,
            correctOrientation: true,
            allowEdit: true,
        };
        // Get the data of an image
        this.camera.getPicture(options).then(function (imagePath) {
            // Special handling for Android library
            if (_this.platform.is('android') && sourceType === _this.camera.PictureSourceType.PHOTOLIBRARY) {
                _this.filePath.resolveNativePath(imagePath)
                    .then(function (filePath) {
                    var correctPath = filePath.substr(0, filePath.lastIndexOf('/') + 1);
                    var currentName = imagePath.substring(imagePath.lastIndexOf('/') + 1, imagePath.lastIndexOf('?'));
                    _this.copyFileToLocalDir(correctPath, currentName, _this.createFileName());
                });
            }
            else {
                var currentName = imagePath.substr(imagePath.lastIndexOf('/') + 1);
                var correctPath = imagePath.substr(0, imagePath.lastIndexOf('/') + 1);
                _this.copyFileToLocalDir(correctPath, currentName, _this.createFileName());
            }
        }, function (err) {
            _this.presentToast('Error while selecting image.');
        });
    };
    // Create a new name for the image
    EditUserPage.prototype.createFileName = function () {
        var d = new Date(), n = d.getTime(), newFileName = n + ".jpg";
        return newFileName;
    };
    // Copy the image to a local folder
    EditUserPage.prototype.copyFileToLocalDir = function (namePath, currentName, newFileName) {
        var _this = this;
        // this.lastImage = null;
        this.file.copyFile(namePath, currentName, cordova.file.dataDirectory, newFileName).then(function (success) {
            _this.lastImage = newFileName;
            // this.lastImage = this.pathForImage(this.lastImage);
            _this.isServerImage = false;
            _this.uploadImage();
        }, function (error) {
            _this.presentToast('Error while storing file.');
        });
    };
    EditUserPage.prototype.presentToast = function (text) {
        var toast = this.toastCtrl.create({
            message: text,
            duration: 3000,
            position: 'top'
        });
        toast.present();
    };
    // Always get the accurate path to your apps folder
    EditUserPage.prototype.pathForImage = function (img) {
        if (img === null) {
            return '';
        }
        else {
            return cordova.file.dataDirectory + img;
        }
    };
    //upload image 
    EditUserPage.prototype.uploadImage = function () {
        var _this = this;
        var base_url = this.api.init();
        // Destination URL
        var url = base_url + "file_upload/dp_upload";
        // File for Upload
        var targetPath = this.pathForImage(this.lastImage);
        //  console.log(targetPath);
        // File name only
        var filename = this.lastImage;
        var options = {
            fileKey: "file",
            fileName: filename,
            chunkedMode: false,
            mimeType: "multipart/form-data",
            params: { 'fileName': filename, 'user_code': this.user_data.user_code }
        };
        var fileTransfer = this.transfer.create();
        this.loading = this.loadingCtrl.create({
            content: 'Uploading...',
        });
        this.loading.present();
        // Use the FileTransfer to upload the image
        fileTransfer.upload(targetPath, url, options).then(function (data) {
            _this.resetSession();
            // console.log(data);
            _this.loading.dismissAll();
            _this.presentToast('Image succesful uploaded.');
        }, function (err) {
            console.log(err);
            _this.loading.dismissAll();
            _this.presentToast('Error while uploading file.');
        });
    };
    EditUserPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-edit-user',template:/*ion-inline-start:"/Users/localuser/gift-prod/src/pages/edit-user/edit-user.html"*/'<!--\n  Generated template for the EditUserPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header no-border>\n  <!-- <button ion-button menuToggle>\n        <ion-icon name="menu"></ion-icon>\n      </button> -->\n  <ion-navbar>\n    <ion-title>Edit My Info</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content class="edit-user-content">\n  <div class="user-info-container">\n    <div class="img-container">\n      <div class="avatar center-cropped" *ngIf="!isServerImage" [ngStyle]="{\'background-image\': \'url(\'+pathForImage(lastImage)+\')\'}"\n        alt="" [hidden]="lastImage === null"></div>\n      <div class="avatar center-cropped" *ngIf="isServerImage" [ngStyle]="{\'background-image\': \'url(\'+lastImage+\')\'}" [hidden]="lastImage === null">\n\n      </div>\n      <!-- <img class="avatar" src="http://demos.creative-tim.com/material-kit/assets/img/kit/faces/christian.jpg" [hidden]="lastImage != null"> -->\n      <button ion-fab color="light" class="img-upload-btn-fab" (click)="presentActionSheet()">\n        <ion-icon name="camera"></ion-icon>\n      </button>\n      <!-- <button class="" ion-button color="light" icon-only>\n              <ion-icon name="camera"></ion-icon>\n            </button> -->\n    </div>\n\n  </div>\n\n  <div class="profile-container">\n\n    <ion-row class="mt-40">\n      <ion-col class="px-0">\n        <ion-card class="about-info">\n          <ion-card-content>\n            <div class="edit-form-container">\n              <div>\n                <form [formGroup]="user" (ngSubmit)="editProfile(user.value)">\n                  <ion-item class="item-divider-header">\n                    <h5>Personal information</h5>\n                    <button ion-button outline item-end class="btn-gift-outline" [disabled]="!user.valid">save</button>\n                  </ion-item>\n                  <hr class="mt-m-10">\n                  <div class="form-container">\n                    <ion-list>\n                      <!-- <form [formGroup]="user"  (ngSubmit)="editProfile(user.value)"> -->\n                      <ion-row>\n                        <ion-col>\n                          <ion-item class="px-10">\n                            <ion-label stacked>First Name</ion-label>\n                            <ion-input type="text" [(ngModel)]="user.first_name" name="first_name" formControlName="first_name" value="{{user_data.first_name}}"></ion-input>\n                          </ion-item>\n                        </ion-col>\n                        <ion-col>\n                          <ion-item class="px-10">\n                            <ion-label stacked>Last Name</ion-label>\n                            <ion-input type="text" [(ngModel)]="user.last_name" name="last_name" formControlName="last_name" value="{{user_data.last_name}}"></ion-input>\n                          </ion-item>\n                        </ion-col>\n                      </ion-row>\n                      <ion-row>\n                        <ion-col>\n                          <ion-item class="px-10">\n                            <ion-label stacked>Age</ion-label>\n                            <ion-input type="text" [(ngModel)]="user.age" name="age" formControlName="age" value="{{user_data.age}}"></ion-input>\n                          </ion-item>\n                          <ion-item class="px-10">\n                            <ion-label stacked>Email</ion-label>\n                            <ion-input type="text" [(ngModel)]="user.email" name="email" formControlName="email" value="{{user_data.email}}"></ion-input>\n                          </ion-item>\n                          <ion-item class="px-10">\n                            <ion-label stacked>Contact No</ion-label>\n                            <ion-input type="number" [(ngModel)]="user.mobile_no" name="mobile_no" formControlName="mobile_no" value="{{user_data.mobile_no}}"></ion-input>\n                          </ion-item>\n                          <ion-item class="px-10">\n                            <ion-label stacked>City</ion-label>\n                            <ion-input type="text" [(ngModel)]="user.city" name="city" formControlName="city" value="{{user_data.city}}"></ion-input>\n                          </ion-item>\n                          <ion-item class="px-10">\n                            <ion-label stacked>Country</ion-label>\n                            <ion-input type="text" [(ngModel)]="user.country" name="country" formControlName="country" value="{{user_data.country}}"></ion-input>\n                          </ion-item>\n                        </ion-col>\n\n                      </ion-row>\n\n                      <!-- </form> -->\n\n                    </ion-list>\n\n                  </div>\n                </form>\n              </div>\n\n\n            </div>\n          </ion-card-content>\n        </ion-card>\n\n        <ion-card>\n          <form [formGroup]="pwdForm" (ngSubmit)="checkOldPassword(pwdForm.value)">\n            <ion-card-content>\n              <ion-item class="item-divider-header">\n                <h5>Reset Password</h5>\n                <button ion-button outline item-end class="btn-gift-outline" [disabled]="!pwdForm.valid" >Submit</button>\n              </ion-item>\n              <hr class="mt-m-10">\n              <ion-row>\n                <ion-item class="px-10">\n                  <!-- <ion-label stacked>Old password</ion-label> -->\n                  <ion-input placeholder="Old Password" type="password" [(ngModel)]="pwdForm.oldPassword" name="oldPassword" formControlName="oldPassword"></ion-input>\n                </ion-item>\n                <ion-item no-lines class="error-msg" text-wrap *ngIf="!pwdForm.controls.oldPassword.valid  && (pwdForm.controls.oldPassword.dirty || submitPwdAttempt)">\n                    <small>This field is required and should be valid Password</small>\n                  </ion-item>\n                <ion-item class="px-10">\n                  <!-- <ion-label stacked>New Password</ion-label> -->\n                  <ion-input placeholder="New Password" type="password" [(ngModel)]="pwdForm.newPassword" name="newPassword" formControlName="newPassword"></ion-input>\n                </ion-item>\n                <ion-item no-lines class="error-msg" text-wrap *ngIf="!pwdForm.controls.newPassword.valid  && (pwdForm.controls.newPassword.dirty || submitPwdAttempt)">\n                    <small>This field is required and should be valid Password</small>\n                  </ion-item>\n                <ion-item class="px-10">\n                  <!-- <ion-label stacked>Retype new Password</ion-label> -->\n                  <ion-input placeholder="Retype Password" type="password" [(ngModel)]="pwdForm.confPassword" name="confPassword" formControlName="confPassword"></ion-input>\n                </ion-item>\n                <ion-item no-lines class="error-msg" text-wrap *ngIf="!pwdForm.controls.confPassword.valid  && (pwdForm.controls.confPassword.dirty || submitPwdAttempt)">\n                    <small>This field is required and should be valid Password</small>\n                    \n                  </ion-item>\n                <ion-item no-lines class="error-msg" text-wrap *ngIf="pwdForm.hasError(\'passwordMismatch\')">\n                    <small>Password mismatch</small>\n                    \n                  </ion-item>\n              </ion-row>\n            </ion-card-content>\n          </form>\n        </ion-card>\n      </ion-col>\n    </ion-row>\n\n\n  </div>\n\n\n</ion-content>'/*ion-inline-end:"/Users/localuser/gift-prod/src/pages/edit-user/edit-user.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* ActionSheetController */], __WEBPACK_IMPORTED_MODULE_2__providers_session_session__["a" /* SessionProvider */],
            __WEBPACK_IMPORTED_MODULE_3__angular_forms__["a" /* FormBuilder */], __WEBPACK_IMPORTED_MODULE_4__providers_auth_auth__["a" /* AuthProvider */],
            __WEBPACK_IMPORTED_MODULE_9__ionic_native_camera__["a" /* Camera */], __WEBPACK_IMPORTED_MODULE_7__ionic_native_transfer__["a" /* Transfer */], __WEBPACK_IMPORTED_MODULE_6__ionic_native_file__["a" /* File */], __WEBPACK_IMPORTED_MODULE_8__ionic_native_file_path__["a" /* FilePath */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* ToastController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* Platform */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_5__providers_api_api__["a" /* ApiProvider */], __WEBPACK_IMPORTED_MODULE_10__ionic_storage__["b" /* Storage */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */]])
    ], EditUserPage);
    return EditUserPage;
}());

//# sourceMappingURL=edit-user.js.map

/***/ }),

/***/ 177:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FullViewPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_auth_auth__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_session_session__ = __webpack_require__(34);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_http__ = __webpack_require__(84);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_Rx__ = __webpack_require__(417);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_Rx___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_rxjs_Rx__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__company_company__ = __webpack_require__(336);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ionic_native_social_sharing__ = __webpack_require__(337);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



// import { Storage } from '@ionic/storage';





/**
 * Generated class for the FullViewPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var FullViewPage = (function () {
    function FullViewPage(navCtrl, navParams, authProvider, session, social, http, events) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.authProvider = authProvider;
        this.session = session;
        this.social = social;
        this.http = http;
        this.events = events;
        this.user_id = "";
        this.showFabs = true;
        //like count and entry no
        this.like_count = 0;
        this.entry_index = 0;
        this.data_giveaway = [];
        this.data_media = [];
        this.data_link = [];
        this.company = [];
        this.session.getUser();
        this.user_id = this.session.user_data.user_code;
        this.card_id = this.navParams.get('giveaway_id');
        this.index = this.navParams.get('index');
        // this.getInfo("43");
        this.getInfo(this.card_id);
    }
    FullViewPage.prototype.onImageLoad = function (imgLoader) {
        // do something with the loader
        console.log("do something with the loader");
    };
    FullViewPage.prototype.getInfo = function (giveaway) {
        var _this = this;
        var url = "giveaway/giveaway?id=" + giveaway + "&user_id=" + this.user_id;
        this.authProvider.getData(url).then(function (res) {
            _this.data_giveaway = res.data_giveaway[0];
            _this.data_media = res.data_media[0];
            _this.data_link = res.data_link;
            _this.company = res.company[0];
            if (res.like_count.length > 0) {
                _this.like_count = res.like_count[0].like_count;
            }
            // this.like_count = res.like_count;
            // this.entry_index = res.entry_no;
        });
        console.log(this.data_giveaway.giveaway_title);
    };
    FullViewPage.prototype.openUrl = function (link) {
        window.open(link, '_system');
    };
    FullViewPage.prototype.trythis = function () {
        this.events.publish('giveaway:added', this.index);
        this.navCtrl.pop();
    };
    FullViewPage.prototype.goToCompanyPage = function (company) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_6__company_company__["a" /* CompanyPage */], { "company_id": company });
    };
    FullViewPage.prototype.socialShare = function () {
        this.social.canShareVia('facebook');
    };
    FullViewPage.prototype.shareSheetShare = function () {
        // alert('ssss');
        // this.social.share("Share message", "Share subject", "URL to file or image", "A URL to share").then(() => {
        //   console.log("shareSheetShare: Success");
        // }).catch(() => {
        //   console.error("shareSheetShare: failed");
        // });
        this.social.shareWithOptions({
            message: "this is test",
            subject: "test subject",
            files: "http://via.placeholder.com/350x150",
            url: "http://giftinapp.com",
            chooserTitle: "facebook"
        }).then(function () {
        });
    };
    FullViewPage.prototype.facebookShare = function () {
        alert('facebook share');
        this.social.shareViaFacebook("message", "http://via.placeholder.com/350x150", "http://giftinapp.com").then(function () {
        });
    };
    FullViewPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-full-view',template:/*ion-inline-start:"/Users/localuser/gift-prod/src/pages/full-view/full-view.html"*/'<!--\n  Generated template for the FullViewPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header no-border>\n\n  <ion-navbar>\n    <ion-title>Giveaway Infomation</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content class="full-view-content">\n  <div class="image-container flex-div" style="min-height:200px;">\n    <ion-spinner name="crescent" class="spinner-center"></ion-spinner>\n    <img-loader src="{{ data_media.giveaway_media_link }}" (load)="onImageLoad($event)" display="block" width="100%" height="400px"\n      spinner useImg></img-loader>\n    <!-- <op-lazy-img [source]="\'data_media.giveaway_media_link\'"></op-lazy-img> -->\n    <!-- <img  src="{{ data_media.giveaway_media_link }}"> -->\n  </div>\n\n  <div class="meta-container px-10">\n    <div class="grab-button-container">\n      <button ion-fab class="grab-button gift-button" (click)="trythis()">\n        <ion-icon name="basket"></ion-icon>\n      </button>\n    </div>\n    <h3 class="text-giftin">{{data_giveaway.giveaway_title}} </h3>\n    <p>Posted by\n      <b>\n        <span class="text-giftin" (click)="goToCompanyPage(data_giveaway.com_code)">{{company.com_name}}</span>\n      </b> on\n      <span class="text-giftin">{{data_giveaway.start_point}}</span>\n      Expires on <span class="text-giftin">{{data_giveaway.end_point}}</span>\n    </p>\n    <p class="dflex-ai-center text-giftin" *ngIf="like_count != 0">\n      <ion-icon name="heart-outline" class="text-25 "></ion-icon>\n      <span class="ml-5"> {{like_count}} </span>\n    </p>\n    <!-- <p class="text-giftin" *ngIf="entry_index != 0">\n      <strong>Your entry No: </strong> <span> {{entry_index[0].entry_no}} </span>\n    </p> -->\n  </div>\n  <div class="info-container px-10">\n    <p>\n      {{data_giveaway.description}}\n    </p>\n    <button ion-button block (click)="shareSheetShare()"> Share with friends. </button>\n    <!-- <button ion-button block (click)="facebookShare()"> Share with facebook. </button> -->\n    \n    <hr>\n    <div class="link-container" *ngIf="data_link">\n      <button (click)="openUrl(data_giveaway.website)" clear color="light" ion-fab class="link-icons">\n        <i class="fa fa-globe"></i>\n      </button>\n      <button (click)="openUrl(item.link)" *ngFor="let item of data_link" clear color="light" ion-fab class="link-icons">\n        <i class="fa fa-{{item.link_media}}"></i>\n      </button>\n      <!-- <button clear color="light" ion-fab  class="link-icons" ><i class="fa fa-facebook-f"></i></button>\n    <button clear color="light" ion-fab  class="link-icons" ><i class="fa fa-twitter"></i></button>\n    <button clear color="light" ion-fab  class="link-icons" ><i class="fa fa-youtube"></i></button>\n    <button clear color="light" ion-fab  class="link-icons" ><i class="fa fa-instagram"></i></button> -->\n    </div>\n  </div>\n\n</ion-content>'/*ion-inline-end:"/Users/localuser/gift-prod/src/pages/full-view/full-view.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__providers_auth_auth__["a" /* AuthProvider */], __WEBPACK_IMPORTED_MODULE_3__providers_session_session__["a" /* SessionProvider */],
            __WEBPACK_IMPORTED_MODULE_7__ionic_native_social_sharing__["a" /* SocialSharing */],
            __WEBPACK_IMPORTED_MODULE_4__angular_http__["b" /* Http */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* Events */]])
    ], FullViewPage);
    return FullViewPage;
}());

//# sourceMappingURL=full-view.js.map

/***/ }),

/***/ 179:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FgtPassPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_firebase__ = __webpack_require__(70);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_firebase__ = __webpack_require__(71);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_firebase___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_firebase__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__reset_pwd_reset_pwd__ = __webpack_require__(180);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__login_login__ = __webpack_require__(43);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







/**
 * Generated class for the FgtPassPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var FgtPassPage = (function () {
    function FgtPassPage(navCtrl, navParams, fire, formBuilder, alertCtrl, loadCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.fire = fire;
        this.formBuilder = formBuilder;
        this.alertCtrl = alertCtrl;
        this.loadCtrl = loadCtrl;
        this.submitAttempt = false;
        //otp verification process
        this.verificationId = '';
        this.otp = '';
        this.user = this.formBuilder.group({
            // email:['',Validators.compose([Validators.required, Validators.email])],
            mobile_no: ['', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].compose([__WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required, __WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].minLength(10)])],
        });
        this.loader = this.loadCtrl.create({
            spinner: 'crescent',
            content: "We are Setting things up to you.",
        });
    }
    FgtPassPage.prototype.sendCode = function (data) {
        var _this = this;
        this.showLoader();
        this.submitAttempt = true;
        var phoneNumber = data.mobile_no;
        this.phone_number = phoneNumber;
        this.fire.verifyPhoneNumber("+" + phoneNumber, 60).then(function (credential) {
            _this.verificationId = credential.verificationId;
            _this.loaderDismiss();
            _this.presentPrompt();
        }).catch(function (err) {
            alert(JSON.stringify(err));
        });
    };
    //sfire base authentication 
    FgtPassPage.prototype.authFirebase = function () {
        var _this = this;
        var signInCredential = __WEBPACK_IMPORTED_MODULE_4_firebase___default.a.auth.PhoneAuthProvider.credential(this.verificationId, this.otp);
        __WEBPACK_IMPORTED_MODULE_4_firebase___default.a.auth().signInWithCredential(signInCredential).then(function (result) {
            // console.log("otp data",JSON.stringify(result));
            // this.loaderDismiss();
            if (JSON.stringify(result).length > 0) {
                _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__reset_pwd_reset_pwd__["a" /* ResetPwdPage */], { phoneNumber: _this.phone_number });
            }
            else {
                var alert_1 = _this.alertCtrl.create({
                    title: "Error",
                    subTitle: "Wrong Passcode. Please check again",
                    enableBackdropDismiss: false,
                    buttons: [{
                            text: 'OK',
                        }]
                });
                alert_1.present();
            }
        }).catch(function (err) {
            console.log('Erorr in OTP');
        });
    };
    FgtPassPage.prototype.presentPrompt = function () {
        var _this = this;
        var alert = this.alertCtrl.create({
            title: 'Enter Passcode',
            enableBackdropDismiss: false,
            inputs: [
                {
                    name: 'otp',
                    type: 'number',
                    placeholder: 'Passcode'
                },
            ],
            buttons: [
                // {
                //   text: 'Cancel',
                //   role: 'cancel',
                //   handler: data => {
                //     console.log('Cancel clicked');
                //   }
                // },
                {
                    text: 'Submit',
                    handler: function (data) {
                        // this.showLoader();
                        if (data.otp != "") {
                            _this.otp = data.otp;
                            _this.authFirebase();
                        }
                        else {
                            return false;
                        }
                    }
                }
            ]
        });
        alert.present();
    };
    FgtPassPage.prototype.showLoader = function () {
        this.loader.present();
    };
    FgtPassPage.prototype.loaderDismiss = function () {
        this.loader.dismiss();
    };
    FgtPassPage.prototype.goToLogin = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_6__login_login__["a" /* LoginPage */]);
    };
    FgtPassPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-fgt-pass',template:/*ion-inline-start:"/Users/localuser/gift-prod/src/pages/fgt-pass/fgt-pass.html"*/'<!--\n  Generated template for the FgtPassPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n\n\n\n<ion-content padding>\n    <ion-grid >\n        <div>\n          <div text-center class="text-white">\n          <h4>Forgot Your Password?</h4>\n          <p>Enter your mobile number to reset your password</p>\n        </div>\n          <ion-row>\n            <ion-col>\n              <ion-list>\n                <form [formGroup]="user"  (ngSubmit)="sendCode(user.value)">\n                  <div class="item-container">\n                  <ion-item class="px-10">\n                    <ion-input type="number" placeholder="Contact Number" clearInput [(ngModel)]="user.mobile_no" name="mobile_no" formControlName="mobile_no"></ion-input>\n                  </ion-item>\n                  <ion-item no-lines class="error-msg" text-wrap *ngIf="!user.controls.mobile_no.valid  && (user.controls.mobile_no.dirty || submitAttempt)">\n                    <small>This field is required and should be valid mobile number</small>\n                  </ion-item>\n                </div>\n                  <ion-row>\n                    <ion-col class="px-0">\n                      <button type="submit" ion-button class="btn-gift" color="light" block [disabled]="!user.valid">Submit</button>\n                    </ion-col>\n                  </ion-row>\n                \n                </form>\n                <p text-center class="text-white">Want to go back?\n                    <span (click)="goToLogin()"><b>Sign in</b></span>\n                  </p>\n              </ion-list>\n            </ion-col>\n          </ion-row>\n        </div>\n      </ion-grid>\n</ion-content>\n'/*ion-inline-end:"/Users/localuser/gift-prod/src/pages/fgt-pass/fgt-pass.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */], __WEBPACK_IMPORTED_MODULE_3__ionic_native_firebase__["a" /* Firebase */], __WEBPACK_IMPORTED_MODULE_2__angular_forms__["a" /* FormBuilder */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* LoadingController */]])
    ], FgtPassPage);
    return FgtPassPage;
}());

//# sourceMappingURL=fgt-pass.js.map

/***/ }),

/***/ 180:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ResetPwdPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_auth_auth__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__login_login__ = __webpack_require__(43);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





/**
 * Generated class for the ResetPwdPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var ResetPwdPage = (function () {
    // validator_data:any;
    function ResetPwdPage(navCtrl, navParams, formBuilder, authProvider, alertCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.formBuilder = formBuilder;
        this.authProvider = authProvider;
        this.alertCtrl = alertCtrl;
        this.submitAttempt = false;
        // this.phoneNumber = this.navParams.get('phoneNumber');
        this.phoneNumber = "94755653685";
        this.passwordData = this.formBuilder.group({
            new_pass: ['', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].compose([__WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required])],
            conf_pass: ['', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].compose([__WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required])],
            phoneNumber: [this.phoneNumber],
        }, { validator: this.passwordMismatch });
        // console.log(this.passwordData.controls);
        // this.validator_data = JSON.stringify(this.passwordData.controls);
    }
    ResetPwdPage.prototype.passwordMismatch = function (cg) {
        console.log("password checking");
        var pwd1 = cg.get('new_pass');
        var pwd2 = cg.get('conf_pass');
        var rv = {};
        if ((pwd1.touched || pwd2.touched) && pwd1.value !== pwd2.value) {
            rv['passwordMismatch'] = true;
        }
        // console.log(rv);
        return rv;
    };
    ResetPwdPage.prototype.resetPassword = function (data) {
        var _this = this;
        // console.log(data);
        var url = "user/reset_password";
        this.submitAttempt = true;
        this.authProvider.postAuthData(data, url).then(function (result) {
            console.log(result);
            if (result.auth) {
                _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__login_login__["a" /* LoginPage */]);
            }
            else {
                var alert_1 = _this.alertCtrl.create({
                    title: "Error",
                    subTitle: result.msg,
                    enableBackdropDismiss: false,
                    buttons: ['OK']
                });
                alert_1.present();
            }
        }).catch(function (err) {
            console.log(err);
        });
    };
    ResetPwdPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ResetPwdPage');
    };
    ResetPwdPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-reset-pwd',template:/*ion-inline-start:"/Users/localuser/gift-prod/src/pages/reset-pwd/reset-pwd.html"*/'<!--\n  Generated template for the ResetPwdPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-content padding>\n    <ion-grid >\n        <div>\n          <div text-center class="text-white">\n          <h4>Reset Your Password</h4>\n          <p>Enter your new password</p>\n        </div>\n          <ion-row>\n            <ion-col>\n              <ion-list>\n                <form [formGroup]="passwordData"  (ngSubmit)="resetPassword(passwordData.value)">\n                  <div class="item-container">\n                  <ion-item class="px-10">\n                    <ion-input type="password" placeholder="New Password" clearInput [(ngModel)]="passwordData.new_pass" name="new_pass" formControlName="new_pass"></ion-input>\n                  </ion-item>\n                  <ion-item no-lines class="error-msg" text-wrap *ngIf="!passwordData.controls.new_pass.valid  && (passwordData.controls.new_pass.dirty || submitAttempt)">\n                    <small>This field is required and should be valid Password</small>\n                  </ion-item>\n\n                  <ion-item class="px-10">\n                      <ion-input type="password" placeholder="Confirm Password" clearInput [(ngModel)]="passwordData.conf_pass" name="conf_pass" formControlName="conf_pass"></ion-input>\n                    </ion-item>\n                    <ion-item no-lines class="error-msg" text-wrap *ngIf="!passwordData.controls.conf_pass.valid  && (passwordData.controls.conf_pass.dirty || submitAttempt)">\n                      <small>This field is required and should be valid Password</small>\n                      \n                    </ion-item>\n                    <ion-item no-lines class="error-msg" text-wrap *ngIf="passwordData.hasError(\'passwordMismatch\')">\n                      <small>Password mismatch</small>\n                      \n                    </ion-item>\n                   \n                </div>\n                  <ion-row>\n                    <ion-col class="px-0">\n                      <button type="submit" ion-button class="btn-gift" color="light" block [disabled]="!passwordData.valid">Submit</button>\n                    </ion-col>\n                  </ion-row>\n                \n                </form>\n                <p text-center class="text-white">Want to go back?\n                    <span><b>Sign in</b></span>\n                  </p>\n              </ion-list>\n            </ion-col>\n          </ion-row>\n        </div>\n      </ion-grid>\n</ion-content>'/*ion-inline-end:"/Users/localuser/gift-prod/src/pages/reset-pwd/reset-pwd.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__angular_forms__["a" /* FormBuilder */], __WEBPACK_IMPORTED_MODULE_3__providers_auth_auth__["a" /* AuthProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */]])
    ], ResetPwdPage);
    return ResetPwdPage;
}());

//# sourceMappingURL=reset-pwd.js.map

/***/ }),

/***/ 181:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return OtpPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_firebase__ = __webpack_require__(71);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_firebase___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_firebase__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_firebase__ = __webpack_require__(70);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__login_login__ = __webpack_require__(43);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_storage__ = __webpack_require__(24);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






/**
 * Generated class for the OtpPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var OtpPage = (function () {
    function OtpPage(navCtrl, navParams, alertCtrl, fire, storage) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.alertCtrl = alertCtrl;
        this.fire = fire;
        this.storage = storage;
        this.otp = '';
        //assign verification id and the phone number 
        this.verification_id = this.navParams.get('verificationid');
        this.phoneNumber = this.navParams.get('phoneNumber');
    }
    //send verification code through the firebase and verify
    OtpPage.prototype.roleSelection = function () {
        var _this = this;
        var signInCredential = __WEBPACK_IMPORTED_MODULE_2_firebase___default.a.auth.PhoneAuthProvider.credential(this.verification_id, this.otp);
        __WEBPACK_IMPORTED_MODULE_2_firebase___default.a.auth().signInWithCredential(signInCredential).then(function (result) {
            console.log("otp data", JSON.stringify(result));
            if (JSON.stringify(result).length > 0) {
                _this.storage.set('fist_login', true);
                var alert_1 = _this.alertCtrl.create({
                    title: "Success",
                    subTitle: "Verified successfully",
                    enableBackdropDismiss: false,
                    buttons: [{
                            text: 'Continue',
                            handler: function () {
                                _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__login_login__["a" /* LoginPage */]);
                            }
                        }]
                });
                alert_1.present();
            }
        }).catch(function () {
            var alert = _this.alertCtrl.create({
                title: "Error",
                subTitle: "Invalid OTP number please check again",
                enableBackdropDismiss: false,
                buttons: [{
                        text: 'OK',
                    }]
            });
            alert.present();
        });
    };
    OtpPage.prototype.resendCode = function () {
        var _this = this;
        this.fire.verifyPhoneNumber("+" + this.phoneNumber, 60).then(function (credential) {
            _this.verification_id = credential.verificationId;
        }).catch(function (err) {
            alert(JSON.stringify(err));
        });
    };
    OtpPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-otp',template:/*ion-inline-start:"/Users/localuser/gift-prod/src/pages/otp/otp.html"*/'<!--\n  Generated template for the OtpPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-content padding>\n\n  <ion-grid>\n    <div text-center class="text-white">\n      <h4>Verify Mobile Number</h4>\n      <p>OTP has been sent to your mobile number. Please enter it below</p>\n    </div>\n    <ion-list>\n\n      <div class="item-container">\n        <ion-item class="px-10">\n          <ion-input type="number" clearInput placeholder="OTP" [(ngModel)]="otp"></ion-input>\n        </ion-item>\n      </div>\n      <ion-row>\n          <ion-col class="px-0">\n            <button type="submit" ion-button class="btn-gift" color="light" block (click)=\'roleSelection()\'>Verify</button>\n          </ion-col>\n        </ion-row>\n    </ion-list>\n    \n    <p text-center class="text-white">Didn\'t receive Verification Code?\n      <span (click)="resendCode()">\n        <b>Resend</b>\n      </span>\n    </p>\n  </ion-grid>\n\n\n\n</ion-content>'/*ion-inline-end:"/Users/localuser/gift-prod/src/pages/otp/otp.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_native_firebase__["a" /* Firebase */], __WEBPACK_IMPORTED_MODULE_5__ionic_storage__["b" /* Storage */]])
    ], OtpPage);
    return OtpPage;
}());

//# sourceMappingURL=otp.js.map

/***/ }),

/***/ 182:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProfilePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__edit_user_edit_user__ = __webpack_require__(176);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_session_session__ = __webpack_require__(34);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_auth_auth__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__entry_entry__ = __webpack_require__(98);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__winnings_winnings__ = __webpack_require__(101);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







/**
 * Generated class for the ProfilePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var ProfilePage = (function () {
    function ProfilePage(auth, navCtrl, navParams, session) {
        var _this = this;
        this.auth = auth;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.session = session;
        this.user_data = [];
        this.entry_count = 0;
        this.win_count = 0;
        this.profile = "General";
        this.session.getUser().then(function (res) {
            _this.user_data = _this.session.user_data;
            console.log("lslslsls", _this.session.user_data);
            _this.getUserDashboard(_this.session.user_data.user_code);
            // console.log("lslslsls",res);
        });
    }
    ProfilePage.prototype.getUserDashboard = function (user_id) {
        var _this = this;
        var url = "user/user_dash?user_code=" + user_id;
        this.auth.getData(url).then(function (res) {
            _this.entry_count = res.entry_count[0].entry_count;
            _this.win_count = res.win_count[0].win_count;
        });
    };
    ProfilePage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ProfilePage');
    };
    ProfilePage.prototype.gotoEditPage = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__edit_user_edit_user__["a" /* EditUserPage */]);
        console.log("Edit page");
    };
    ProfilePage.prototype.goEntry = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__entry_entry__["a" /* EntryPage */]);
    };
    ProfilePage.prototype.goWinning = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_6__winnings_winnings__["a" /* WinningsPage */]);
    };
    ProfilePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-profile',template:/*ion-inline-start:"/Users/localuser/gift-prod/src/pages/profile/profile.html"*/'<ion-header no-border>\n\n  <ion-navbar>\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>My Profile</ion-title>\n    <ion-buttons end (click)="gotoEditPage()">\n      <button ion-button icon-only>\n        <ion-icon name="create"></ion-icon>\n      </button>\n    </ion-buttons>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content>\n  <div class="user-info-container">\n    <div class="avatar center-cropped" [ngStyle]="{\'background-image\': \'url(\'+ user_data.photo +\')\'}"></div>\n\n    <!-- <img class="avatar" src="http://demos.creative-tim.com/material-kit/assets/img/kit/faces/christian.jpg"> -->\n    <div class="mt-m-50">\n      <h4>{{user_data.first_name}} {{user_data.last_name}}</h4>\n      <p>{{ user_data.city }}, {{ user_data.country }}</p>\n    </div>\n  </div>\n  <div class="profile-container">\n\n\n    <ion-row class="mt-40">\n      <ion-col class="px-0">\n        <ion-card class="about-info">\n\n          <ion-card-header>\n            <h4>Dashbord</h4>\n          </ion-card-header>\n\n          <ion-card-content>\n            <p>Hello {{user_data.first_name}}, followings are you activites. </p>\n            <hr>\n            <div class="user-stats">\n              <ion-row>\n                <ion-col id="l1" class="d-flex fd-r jc-sa ai-c" (click)="goEntry()">\n                  <i class="fa fa-heart" ></i>\n                  <div>\n                    <h4>{{entry_count}}</h4>\n                    <p>Entries</p>\n                  </div>\n\n                </ion-col>\n                <ion-col id="l2"  class="d-flex fd-r jc-sa ai-c" (click)="goWinning()">\n                  <i class="fa fa-trophy"></i>\n                  <div>\n                    <h4>{{win_count}}</h4>\n                    <p>Wins</p>\n                  </div>\n\n                </ion-col>\n                <!-- <ion-col id="l3" >\n                  <h4>65</h4>\n                  <p>Interests</p>\n                </ion-col> -->\n              </ion-row>\n            </div>\n          </ion-card-content>\n        </ion-card>\n      </ion-col>\n    </ion-row>\n\n    <ion-row class="mt-20">\n      <ion-col class="px-0">\n        <ion-card class="general-container">\n          <ion-card-header>\n            <h4>Basic Info</h4>\n          </ion-card-header>\n          <ion-card-content>\n\n            <hr>\n            <div class="user-info">\n              <ion-item>\n                <p>\n                  <b>Age</b>\n                </p>\n                <p>{{ user_data.age }}</p>\n              </ion-item>\n              <ion-item>\n                <p>\n                  <b>Email</b>\n                </p>\n                <p>{{ user_data.email }}</p>\n              </ion-item>\n              <ion-item>\n                <p>\n                  <b>Contact No</b>\n                </p>\n                <p>{{ user_data.mobile_no }}</p>\n              </ion-item>\n              <ion-item>\n                <p>\n                  <b>City</b>\n                </p>\n                <p>{{ user_data.city }}</p>\n              </ion-item>\n              <ion-item>\n                <p>\n                  <b>Country</b>\n                </p>\n                <p>{{ user_data.country }}</p>\n              </ion-item>\n            </div>\n          </ion-card-content>\n        </ion-card>\n      </ion-col>\n    </ion-row>\n  </div>\n\n</ion-content>'/*ion-inline-end:"/Users/localuser/gift-prod/src/pages/profile/profile.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_4__providers_auth_auth__["a" /* AuthProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */], __WEBPACK_IMPORTED_MODULE_3__providers_session_session__["a" /* SessionProvider */]])
    ], ProfilePage);
    return ProfilePage;
}());

//# sourceMappingURL=profile.js.map

/***/ }),

/***/ 183:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UserPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_session_session__ = __webpack_require__(34);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_forms__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_auth_auth__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_api_api__ = __webpack_require__(62);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__menu_menu__ = __webpack_require__(51);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ionic_native_file__ = __webpack_require__(63);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__ionic_native_transfer__ = __webpack_require__(133);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__ionic_native_file_path__ = __webpack_require__(134);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__ionic_native_camera__ = __webpack_require__(135);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__ionic_storage__ = __webpack_require__(24);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};












/**
 * Generated class for the UserPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var UserPage = (function () {
    function UserPage(navCtrl, navParams, actionSheetCtrl, session, formBuilder, authProvider, camera, transfer, file, filePath, toastCtrl, platform, loadingCtrl, api, storage) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.actionSheetCtrl = actionSheetCtrl;
        this.session = session;
        this.formBuilder = formBuilder;
        this.authProvider = authProvider;
        this.camera = camera;
        this.transfer = transfer;
        this.file = file;
        this.filePath = filePath;
        this.toastCtrl = toastCtrl;
        this.platform = platform;
        this.loadingCtrl = loadingCtrl;
        this.api = api;
        this.storage = storage;
        //profile pic related
        this.lastImage = null;
        this.isServerImage = true;
        this.submitAttempt = false;
        this.country_list = [];
        // this.user_code = "64";
        this.getcountries();
        this.user_code = this.navParams.get('user_code');
        this.user = this.formBuilder.group({
            first_name: ['', __WEBPACK_IMPORTED_MODULE_3__angular_forms__["f" /* Validators */].compose([__WEBPACK_IMPORTED_MODULE_3__angular_forms__["f" /* Validators */].required,])],
            last_name: ['', __WEBPACK_IMPORTED_MODULE_3__angular_forms__["f" /* Validators */].compose([__WEBPACK_IMPORTED_MODULE_3__angular_forms__["f" /* Validators */].required,])],
            city: ['', __WEBPACK_IMPORTED_MODULE_3__angular_forms__["f" /* Validators */].compose([__WEBPACK_IMPORTED_MODULE_3__angular_forms__["f" /* Validators */].required,])],
            // country: ['', Validators.compose([Validators.required,])],
            age: ['', __WEBPACK_IMPORTED_MODULE_3__angular_forms__["f" /* Validators */].compose([__WEBPACK_IMPORTED_MODULE_3__angular_forms__["f" /* Validators */].required,])],
            user_code: this.user_code
        });
    }
    UserPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad UserPage');
    };
    UserPage.prototype.getcountries = function () {
        var _this = this;
        var url = "settings/countries";
        this.authProvider.getData(url).then(function (country_list) {
            country_list.forEach(function (element) {
                _this.country_list.push(element);
                // console.log(element);
            });
        });
    };
    UserPage.prototype.addUserData = function (data) {
        var _this = this;
        var url = "user/update_user_info";
        //  this.submitAttempt = true;
        this.authProvider.postAuthData(data, url).then(function (res) {
            if (res.auth) {
                _this.uploadImage();
                _this.presentToast("Personal Infomation updated successfully");
                _this.storage.set('fist_login', false);
                _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_6__menu_menu__["a" /* MenuPage */]);
            }
            //  console.log(res);
            // this.resetSession();
        });
    };
    UserPage.prototype.presentActionSheet = function () {
        var _this = this;
        var actionSheet = this.actionSheetCtrl.create({
            title: 'Edit Profile Photo',
            buttons: [
                {
                    text: 'Pick From Gallery',
                    icon: 'images',
                    // role: 'destructive',
                    handler: function () {
                        _this.takePicture(_this.camera.PictureSourceType.PHOTOLIBRARY);
                        console.log('Destructive clicked');
                    }
                }, {
                    text: 'Take a Picture',
                    icon: 'camera',
                    handler: function () {
                        _this.takePicture(_this.camera.PictureSourceType.CAMERA);
                        console.log('Archive clicked');
                    }
                }, {
                    text: 'Delete',
                    icon: 'trash',
                    // role: 'cancel',
                    handler: function () {
                        console.log('Cancel clicked');
                    }
                },
                {
                    text: 'Cancel',
                    role: 'cancel',
                    icon: 'close',
                    handler: function () {
                        console.log('Cancel clicked');
                    }
                }
            ]
        });
        actionSheet.present();
    };
    UserPage.prototype.takePicture = function (sourceType) {
        var _this = this;
        // Create options for the Camera Dialog
        var options = {
            quality: 50,
            sourceType: sourceType,
            saveToPhotoAlbum: true,
            correctOrientation: true,
            allowEdit: true,
        };
        // Get the data of an image
        this.camera.getPicture(options).then(function (imagePath) {
            // Special handling for Android library
            if (_this.platform.is('android') && sourceType === _this.camera.PictureSourceType.PHOTOLIBRARY) {
                _this.filePath.resolveNativePath(imagePath)
                    .then(function (filePath) {
                    var correctPath = filePath.substr(0, filePath.lastIndexOf('/') + 1);
                    var currentName = imagePath.substring(imagePath.lastIndexOf('/') + 1, imagePath.lastIndexOf('?'));
                    _this.copyFileToLocalDir(correctPath, currentName, _this.createFileName());
                });
            }
            else {
                var currentName = imagePath.substr(imagePath.lastIndexOf('/') + 1);
                var correctPath = imagePath.substr(0, imagePath.lastIndexOf('/') + 1);
                _this.copyFileToLocalDir(correctPath, currentName, _this.createFileName());
            }
        }, function (err) {
            _this.presentToast('Error while selecting image.');
        });
    };
    // Create a new name for the image
    UserPage.prototype.createFileName = function () {
        var d = new Date(), n = d.getTime(), newFileName = n + ".jpg";
        return newFileName;
    };
    // Copy the image to a local folder
    UserPage.prototype.copyFileToLocalDir = function (namePath, currentName, newFileName) {
        var _this = this;
        // this.lastImage = null;
        this.file.copyFile(namePath, currentName, cordova.file.dataDirectory, newFileName).then(function (success) {
            _this.lastImage = newFileName;
            // this.lastImage = this.pathForImage(this.lastImage);
            _this.isServerImage = false;
        }, function (error) {
            _this.presentToast('Error while storing file.');
        });
    };
    UserPage.prototype.presentToast = function (text) {
        var toast = this.toastCtrl.create({
            message: text,
            duration: 3000,
            position: 'top'
        });
        toast.present();
    };
    // Always get the accurate path to your apps folder
    UserPage.prototype.pathForImage = function (img) {
        if (img === null) {
            return '';
        }
        else {
            return cordova.file.dataDirectory + img;
        }
    };
    //upload image 
    UserPage.prototype.uploadImage = function () {
        var _this = this;
        var base_url = this.api.init();
        // Destination URL
        var url = base_url + "file_upload/dp_upload";
        // File for Upload
        var targetPath = this.pathForImage(this.lastImage);
        //  console.log(targetPath);
        // File name only
        var filename = this.lastImage;
        var options = {
            fileKey: "file",
            fileName: filename,
            chunkedMode: false,
            mimeType: "multipart/form-data",
            params: { 'fileName': filename, 'user_code': this.user_code }
        };
        var fileTransfer = this.transfer.create();
        this.loading = this.loadingCtrl.create({
            content: 'Uploading...',
        });
        this.loading.present();
        // Use the FileTransfer to upload the image
        fileTransfer.upload(targetPath, url, options).then(function (data) {
            // console.log(data);
            _this.loading.dismissAll();
            // this.presentToast('Image succesful uploaded.');
        }, function (err) {
            console.log(err);
            _this.loading.dismissAll();
            _this.presentToast('Error while uploading file.');
        });
    };
    UserPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-user',template:/*ion-inline-start:"/Users/localuser/gift-prod/src/pages/user/user.html"*/'<!--\n  Generated template for the EditUserPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-content class="user-content">\n    <div text-center class="text-white mt-40">\n        <h4>One More Step</h4>\n        <p>Please fill your information</p>\n      </div>\n\n  <div class="add-user-info-container">\n    <div class="img-container">\n        <div class="avatar center-cropped" *ngIf="!isServerImage"  [ngStyle]="{\'background-image\': \'url(\'+pathForImage(lastImage)+\')\'}" alt="" [hidden]="lastImage === null"></div>\n        <div class="avatar center-cropped" *ngIf="isServerImage" style="background-image: url(http://demos.creative-tim.com/material-kit/assets/img/kit/faces/christian.jpg)" ></div>\n      <!-- <img class="avatar" src="http://demos.creative-tim.com/material-kit/assets/img/kit/faces/christian.jpg"> -->\n      <button ion-fab color="light" class="img-upload-btn-fab" (click)="presentActionSheet()">\n        <ion-icon name="camera"></ion-icon>\n      </button>\n      <!-- <button class="" ion-button color="light" icon-only>\n            <ion-icon name="camera"></ion-icon>\n          </button> -->\n    </div>\n  </div>\n\n  <div class="profile-container">\n    <ion-row class="mt-40">\n      <ion-col class="px-0">\n        <ion-card class="about-info">\n          <ion-card-content>\n            <div class="edit-form-container">\n              <div>\n                <form [formGroup]="user" (ngSubmit)="addUserData(user.value)">\n                  <ion-item class="item-divider-header">\n                    <h5>Personal information</h5>\n                    <button ion-button outline item-end class="btn-gift-outline" [disabled]="!user.valid">save</button>\n                  </ion-item>\n                  <hr class="mt-m-10">\n                  <div class="form-container">\n                    <ion-list>\n                      <!-- <form [formGroup]="user"  (ngSubmit)="editProfile(user.value)"> -->\n                      <ion-row>\n                        <ion-col>\n                          <ion-item class="px-10">\n                            <ion-input type="text" [(ngModel)]="user.first_name" name="first_name" formControlName="first_name"  placeholder="First Name"></ion-input>\n                          </ion-item>\n                        </ion-col>\n                        <ion-col>\n                          <ion-item class="px-10">\n                            <ion-input type="text" [(ngModel)]="user.last_name" name="last_name" formControlName="last_name"  placeholder="Last Name"></ion-input>\n                          </ion-item>\n                        </ion-col>\n                      </ion-row>\n                      <ion-row>\n                        <ion-col>\n                          <ion-item class="px-10">\n                            <ion-input type="text" [(ngModel)]="user.age" name="age" formControlName="age"  placeholder="Age"></ion-input>\n                          </ion-item>\n\n                          <ion-item class="px-10">\n                            <ion-input type="text" [(ngModel)]="user.city" name="city" formControlName="city"  placeholder="City"></ion-input>\n                          </ion-item>\n                          <!-- <ion-item class="px-10">\n                            <ion-input type="text" [(ngModel)]="user.country" name="country" formControlName="country"  placeholder="Country"></ion-input>\n                          </ion-item> -->\n                          <!-- <ion-item class="px-10">\n                            <ion-select clearInput placeholder="Select your country" name="country" required formControlName="country" [(ngModel)]="user.country">\n                              <ion-option *ngFor="let cou of country_list"  value="{{cou.name}}">{{cou.name}}</ion-option>\n                            </ion-select>\n                          </ion-item> -->\n                        </ion-col>\n                      </ion-row>\n                      <!-- </form> -->\n                    </ion-list>\n\n                  </div>\n                </form>\n              </div>\n\n\n            </div>\n          </ion-card-content>\n        </ion-card>\n      </ion-col>\n    </ion-row>\n\n\n  </div>\n\n\n</ion-content>'/*ion-inline-end:"/Users/localuser/gift-prod/src/pages/user/user.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* ActionSheetController */], __WEBPACK_IMPORTED_MODULE_2__providers_session_session__["a" /* SessionProvider */],
            __WEBPACK_IMPORTED_MODULE_3__angular_forms__["a" /* FormBuilder */], __WEBPACK_IMPORTED_MODULE_4__providers_auth_auth__["a" /* AuthProvider */],
            __WEBPACK_IMPORTED_MODULE_10__ionic_native_camera__["a" /* Camera */], __WEBPACK_IMPORTED_MODULE_8__ionic_native_transfer__["a" /* Transfer */], __WEBPACK_IMPORTED_MODULE_7__ionic_native_file__["a" /* File */], __WEBPACK_IMPORTED_MODULE_9__ionic_native_file_path__["a" /* FilePath */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* ToastController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* Platform */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_5__providers_api_api__["a" /* ApiProvider */], __WEBPACK_IMPORTED_MODULE_11__ionic_storage__["b" /* Storage */]])
    ], UserPage);
    return UserPage;
}());

//# sourceMappingURL=user.js.map

/***/ }),

/***/ 195:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 195;

/***/ }),

/***/ 21:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AuthProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__api_api__ = __webpack_require__(62);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_http__ = __webpack_require__(84);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
// import { HttpClient } from '@angular/common/http';



/*
  Generated class for the AuthProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
  */
var AuthProvider = (function () {
    function AuthProvider(apiProvider, http) {
        this.apiProvider = apiProvider;
        this.http = http;
        this.post_url = this.apiProvider.init();
    }
    //post authorized data function
    //data: in array format, url: path to the function after post url
    AuthProvider.prototype.postAuthData = function (data, url) {
        var _this = this;
        var headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]();
        headers.append('Content-Type', 'application/x-www-form-urlencoded;charset=utf-8');
        // headers.append('Access-Control-Allow-Origin','*');
        return new Promise(function (resolve, reject) {
            _this.http.post(_this.post_url + url, JSON.stringify(data), { headers: headers })
                .subscribe(function (res) {
                resolve(res.json());
            }, function (error) {
                reject(error);
            });
        });
    };
    //get data function
    //url_get: url after post url
    AuthProvider.prototype.getData = function (url_get) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.http.get(_this.post_url + url_get)
                .subscribe(function (res) {
                resolve(res.json());
            }, function (error) {
                reject(error);
            });
        });
    };
    AuthProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__api_api__["a" /* ApiProvider */], __WEBPACK_IMPORTED_MODULE_2__angular_http__["b" /* Http */]])
    ], AuthProvider);
    return AuthProvider;
}());

//# sourceMappingURL=auth.js.map

/***/ }),

/***/ 239:
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"../pages/comp-view/comp-view.module": [
		727,
		14
	],
	"../pages/edit-user/edit-user.module": [
		728,
		13
	],
	"../pages/entry/entry.module": [
		729,
		12
	],
	"../pages/fgt-pass/fgt-pass.module": [
		731,
		11
	],
	"../pages/full-view/full-view.module": [
		730,
		10
	],
	"../pages/login/login.module": [
		732,
		9
	],
	"../pages/menu/menu.module": [
		733,
		8
	],
	"../pages/otp/otp.module": [
		734,
		7
	],
	"../pages/profile/profile.module": [
		735,
		6
	],
	"../pages/reset-pwd/reset-pwd.module": [
		736,
		5
	],
	"../pages/search/search.module": [
		737,
		4
	],
	"../pages/signup/signup.module": [
		738,
		3
	],
	"../pages/tutorial/tutorial.module": [
		739,
		2
	],
	"../pages/user/user.module": [
		740,
		1
	],
	"../pages/winnings/winnings.module": [
		741,
		0
	]
};
function webpackAsyncContext(req) {
	var ids = map[req];
	if(!ids)
		return Promise.reject(new Error("Cannot find module '" + req + "'."));
	return __webpack_require__.e(ids[1]).then(function() {
		return __webpack_require__(ids[0]);
	});
};
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = 239;
module.exports = webpackAsyncContext;

/***/ }),

/***/ 336:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CompanyPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_auth_auth__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__comp_view_comp_view__ = __webpack_require__(50);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/**
 * Generated class for the CompanyPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
// @IonicPage()
var CompanyPage = (function () {
    function CompanyPage(navCtrl, navParams, auth) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.auth = auth;
        this.giveaway_List = [];
        this.company = "";
        this.company_data = [];
        this.company_logo = "";
        var company_id = this.navParams.get('company_id');
        this.company = company_id;
        this.getCompanyGiveaways(company_id, 0);
    }
    CompanyPage.prototype.getCompanyGiveaways = function (company_id, offset) {
        var _this = this;
        // let offset = 0;
        var url = "giveaway/company_giveaways?com_id=" + company_id + "&offset=" + offset;
        this.auth.getData(url).then(function (giveaways) {
            console.log(giveaways);
            if (giveaways.auth) {
                giveaways.result.forEach(function (element) {
                    _this.giveaway_List.push(element);
                });
                _this.company_data = giveaways.company[0];
                console.log(_this.company_data);
                _this.company_logo = "http://giveaway.treatpick.com/uploads/company_logo/" + giveaways.company[0].com_code + "/" + giveaways.company[0].com_logo;
                console.log(_this.company_logo);
            }
            // giveaways.forEach(element => {
            //   this.giveaway_List.push(element);
            // });
        });
    };
    CompanyPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad CompanyPage');
    };
    CompanyPage.prototype.viewInfo = function (giveaway_code) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__comp_view_comp_view__["a" /* CompViewPage */], { "giveaway_code": giveaway_code });
    };
    CompanyPage.prototype.doInfinite = function (infiniteScroll) {
        console.log('Begin async operation');
        this.getCompanyGiveaways(this.company, this.giveaway_List.length);
        setTimeout(function () {
            console.log('ENd async operation');
            infiniteScroll.complete();
        }, 1500);
    };
    CompanyPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-company',template:/*ion-inline-start:"/Users/localuser/gift-prod/src/pages/company/company.html"*/'<!--\n  Generated template for the CompanyPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <ion-title>company</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content>\n  <ion-grid>\n    <ion-row>\n      <ion-col col-4>\n       \n        <img src="{{ company_logo!=\'\' ?company_logo :\'https://www.w3schools.com/w3images/avatar3.png\'}}" alt="">\n      </ion-col>\n      <ion-col>\n        <h4>{{ company_data.com_name }}  <ion-icon *ngIf="company_data.com_registration_status == \'VERIFIED\'" name="checkmark-circle" color="giftin"></ion-icon></h4>\n        <p><small>Registerd on {{ company_data.created_date}}</small></p>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n  <div padding>\n    <h5>Our Active Giveaways</h5>\n    <div class="grid-full">\n        <ion-row>\n          <ion-col col-6 *ngFor="let item of giveaway_List">\n              <ion-card (click)="viewInfo(item.giveaway_code)" class="animated fadeIn">\n                <ion-card-header class="text-giftin">\n                  {{ item.giveaway_title }}\n                  <!-- <p><small>on {{ item.created_date }} </small></p> -->\n                </ion-card-header>\n                <div class="card-image cover-bg flex-div" [ngStyle]="{\'background-image\': \'url(\'+ item.giveaway_media_link +\')\'}">\n                    <ion-spinner name="crescent" class="spinner-center"></ion-spinner>\n                </div>\n                  <!-- <img class="card-image" src="{{ item.giveaway_media_link }}"/> -->\n                  <ion-card-content>\n                      <p class="dflex-ai-center">\n                        <span style="width:50%;">\n                            <ion-icon name="heart-outline" class="text-giftin"></ion-icon>\n                            <span class="ml-5 text-giftin"> {{item.like_count}} </span>\n                        </span>\n                        <span  style="width:50%;">\n                          \n                          <!-- <ion-icon name="heart-outline" class=""></ion-icon> -->\n                          <span class="ml-5">  </span>\n                        </span>\n                           \n                      </p>\n                  </ion-card-content>\n                </ion-card>\n          </ion-col>\n          <ion-infinite-scroll (ionInfinite)="doInfinite($event)">\n            <ion-infinite-scroll-content loadingText="Loading more data..."></ion-infinite-scroll-content>\n          </ion-infinite-scroll>\n        </ion-row>\n      </div>\n  </div>\n</ion-content>'/*ion-inline-end:"/Users/localuser/gift-prod/src/pages/company/company.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__providers_auth_auth__["a" /* AuthProvider */]])
    ], CompanyPage);
    return CompanyPage;
}());

//# sourceMappingURL=company.js.map

/***/ }),

/***/ 34:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SessionProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ionic_storage__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__auth_auth__ = __webpack_require__(21);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/*
  Generated class for the SessionProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
var SessionProvider = (function () {
    function SessionProvider(storage, authProvider) {
        var _this = this;
        this.storage = storage;
        this.authProvider = authProvider;
        this.user_data = [];
        this.user_data_Ob = {};
        // console.log('Hello SessionProvider Provider');\
        this.checkLogIn().then(function (res) {
            if (res) {
                _this.getUser();
            }
        });
    }
    //check login status
    SessionProvider.prototype.checkLogIn = function () {
        return this.storage.get('logged_in').then(function (res) {
            console.log("login status goooooos", res);
            return res;
        });
    };
    //check tutorial seen status
    SessionProvider.prototype.hasSeenTutorial = function () {
        return this.storage.get('hasSeenTutorial').then(function (res) {
            // console.log("hasSeenTutorial",res);
            return res;
        });
    };
    SessionProvider.prototype.getUserId = function () {
        return this.storage.get('user_id').then(function (res) {
            return res;
        });
    };
    //get logged in user data
    SessionProvider.prototype.getUser = function () {
        var _this = this;
        return this.storage.get('user_id').then(function (res) {
            var url = "user/user?id=" + res;
            _this.authProvider.getData(url).then(function (result) {
                _this.user_data.user_code = result[0].user_code;
                _this.user_data.first_name = result[0].f_name;
                _this.user_data.last_name = result[0].l_name;
                _this.user_data.mobile_no = result[0].user_mobile_number;
                _this.user_data.age = result[0].user_age;
                _this.user_data.country = result[0].user_country;
                _this.user_data.city = result[0].city;
                _this.user_data.email = result[0].user_email;
                _this.user_data.photo = result[0].user_photo;
            });
            // this.storage.set('user_photo',this.user_data.photo);
        });
        // return this.user_data;
    };
    SessionProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__ionic_storage__["b" /* Storage */], __WEBPACK_IMPORTED_MODULE_2__auth_auth__["a" /* AuthProvider */]])
    ], SessionProvider);
    return SessionProvider;
}());

//# sourceMappingURL=session.js.map

/***/ }),

/***/ 340:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__full_view_full_view__ = __webpack_require__(177);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_auth_auth__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_session_session__ = __webpack_require__(34);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_storage__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__search_search__ = __webpack_require__(100);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var HomePage = (function () {
    function HomePage(navCtrl, navParams, popoverCtrl, authProvider, session, storage, events, toastCtrl, app, loadingCtrl) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.popoverCtrl = popoverCtrl;
        this.authProvider = authProvider;
        this.session = session;
        this.storage = storage;
        this.events = events;
        this.toastCtrl = toastCtrl;
        this.app = app;
        this.loadingCtrl = loadingCtrl;
        //giftin button
        this.showBtn = true;
        this.showEmptySlide = false;
        //index number related
        this.text_part = "";
        this.index_no = "";
        this.show_card = false;
        //cards array 
        this.cards = [];
        this.card_added = false;
        //slide related variables
        this.currentIndex = 0;
        //animation related
        this.clStatus = false;
        this.btnStatus = false;
        this.showSpinner = false;
        events.subscribe('giveaway:added', function (index) {
            _this.grabIt(index);
        });
    }
    HomePage.prototype.presentLoading = function () {
        this.loader = this.loadingCtrl.create({
            content: "Please wait...",
            duration: 3000
        });
        this.loader.present();
    };
    //initiating functions
    HomePage.prototype.ngAfterViewInit = function () {
        this.presentLoading();
        this.showSpinner = true;
        this.addNewCards(5);
        this.currentIndex = 0;
        console.log("after init current INdex", this.currentIndex);
        this.loader.dismiss();
    };
    //add new cards to the array
    HomePage.prototype.addNewCards = function (count) {
        var _this = this;
        this.storage.get('user_id').then(function (user) {
            var u_id = user;
            var url = "giveaway/giveaways?count=" + count + "&user_code=" + u_id;
            _this.authProvider.getData(url).then(function (giveaways) {
                if (giveaways.length <= 0) {
                    console.log("no more new cards");
                    _this.checkSlideLength();
                }
                else {
                    for (var i = 0; i <= giveaways.length - 1; i++) {
                        var pushed = _this.cards.push(giveaways[i]);
                    }
                }
            });
        }).then(function () {
            _this.showSpinner = false;
        });
    };
    //this calles when slide change
    HomePage.prototype.slideChanged = function () {
        this.currentIndex = this.slides.getActiveIndex();
        console.log("Active slide", this.currentIndex);
        if (this.slides.isEnd()) {
            // this.slides.slidePrev(1000,true);
            this.slides.lockSwipeToNext(true);
        }
        else {
            this.slides.lockSwipeToNext(false);
        }
    };
    HomePage.prototype.slideCheckForce = function (card_added) {
        this.currentIndex = this.slides.getActiveIndex();
        console.log("Active slide force check", this.currentIndex);
        var active_slide = this.currentIndex;
        if (this.slides.isEnd() && card_added == false) {
            this.slides.slideTo(this.slides.getActiveIndex() - 1, 1000, true);
            this.currentIndex = this.slides.getActiveIndex();
            console.log("this.currentIndex after if", this.currentIndex);
            this.slides.lockSwipeToNext(true);
        }
        else {
            this.slides.lockSwipeToNext(false);
        }
    };
    //gift in button action
    HomePage.prototype.grabIt = function (id) {
        var _this = this;
        this.clStatus = true;
        this.btnStatus = false;
        // console.log(this.cards[id]);
        this.storage.get('user_id').then(function (user_id) {
            var data = { giveaway_code: _this.cards[id].giveaway_code, user_code: user_id };
            var url = "giveaway/giveaway_accept";
            _this.authProvider.postAuthData(data, url).then(function (accept_result) {
                if (accept_result.auth == false) {
                }
                else {
                    _this.text_part = _this.cards[id].giveaway_title;
                    _this.index_no = accept_result.index_no;
                    _this.show_card = true;
                }
            }).then(function () {
                setTimeout(function () {
                    _this.clStatus = false;
                    _this.btnStatus = true;
                    _this.cards.splice(id, 1);
                    var u_id = user_id;
                    var offset = _this.cards.length;
                    var url = "giveaway/giveaways?count=" + 1 + "&user_code=" + u_id + "&offset=" + offset;
                    _this.authProvider.getData(url).then(function (result_list) {
                        if (result_list.length <= 0) {
                            _this.card_added = false;
                            // this.slideCheckForce(this.card_added);
                        }
                        else {
                            for (var i = 0; i <= result_list.length - 1; i++) {
                                var push_res = _this.cards.push(result_list[i]);
                            }
                            _this.card_added = true;
                            // this.slideCheckForce(this.card_added);
                        }
                    }).then(function () {
                        _this.slideCheckForce(_this.card_added);
                        _this.checkSlideLength();
                    });
                    _this.show_card = false;
                }, 2500);
                // this.slideCheckForce(this.card_added);
                // this.show_card = false;
            });
        });
    };
    //check slide length to identify last slide
    HomePage.prototype.checkSlideLength = function () {
        console.log("slide length = " + this.slides.length());
        console.log("cards length = " + this.cards.length);
        if (this.slides.length() == 0 || this.cards.length == 0) {
            this.showBtn = false;
            this.showEmptySlide = true;
            if (this.showEmptySlide == true) {
                this.slides.lockSwipes(true);
            }
            else {
                this.slides.lockSwipes(false);
            }
        }
        else {
            this.showBtn = true;
            this.showEmptySlide = false;
        }
    };
    //loadMore function
    HomePage.prototype.loadMore = function () {
        var _this = this;
        this.toastPresent("Loading new giveaways..");
        this.storage.get('user_id').then(function (res) {
            var u_id = res;
            var limit = 3;
            var offset = _this.cards.length;
            var url = "giveaway/more_giveaways?limit=" + limit + "&user_code=" + u_id + "&offset=" + offset;
            _this.authProvider.getData(url).then(function (result_list_more) {
                if (result_list_more.length <= 0) {
                    _this.card_added = false;
                    // this.slideCheckForce(this.card_added);
                }
                else {
                    for (var i = 0; i <= result_list_more.length - 1; i++) {
                        var push_res = _this.cards.push(result_list_more[i]);
                    }
                    _this.card_added = true;
                }
            }).then(function () {
                _this.slideCheckForce(_this.card_added);
                _this.checkSlideLength();
            });
        });
        this.toast.dismiss();
    };
    //refresh function
    HomePage.prototype.refreshCards = function () {
        this.navCtrl.setRoot(this.navCtrl.getActive().component);
    };
    HomePage.prototype.openFullView = function (g_id, currentIndex) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__full_view_full_view__["a" /* FullViewPage */], { "giveaway_id": g_id, "index": currentIndex });
    };
    HomePage.prototype.toastPresent = function (txt) {
        this.toast = this.toastCtrl.create({
            message: txt,
            // duration: 3000,
            position: 'bottom'
        });
        this.toast.present();
    };
    HomePage.prototype.goToSearch = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_6__search_search__["a" /* SearchPage */], { 'mainview': false });
        // this.app.getRootNav().push(SearchPage);
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* Slides */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* Slides */])
    ], HomePage.prototype, "slides", void 0);
    HomePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-home',template:/*ion-inline-start:"/Users/localuser/gift-prod/src/pages/home/home.html"*/'<!--\n  Generated template for the HomePage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header no-border>\n\n  <ion-navbar>\n      <button ion-button menuToggle>\n          <ion-icon name="menu"></ion-icon>\n        </button>\n    <ion-title>Home</ion-title>\n    <ion-buttons end >\n      <button ion-button icon-only color="royal" (click)="goToSearch()">\n        <ion-icon name="search"></ion-icon>\n      </button>\n      <button ion-button icon-only color="royal" (click)="loadMore()">\n        <ion-icon name="refresh"></ion-icon>\n      </button>\n    </ion-buttons>\n    <!-- <span (click)="loadMore()" >LOAD MORE</span> -->\n  </ion-navbar>\n\n</ion-header>\n<div *ngIf="clStatus || showSpinner" style="position: absolute;\nz-index: 999999;\n/* margin: auto; */\nwidth: 100%;\nheight: 100%;\ndisplay: flex;\njustify-content: center;\nalign-items: center;\ncolor:white;">\n    <ion-spinner name="crescent" color="lighter" ></ion-spinner>\n</div>\n\n<div *ngIf="showEmptySlide" style="position: absolute;\nz-index: 2;\nwidth: 100%;\nheight: 100%;\ndisplay: flex;\nflex-direction: column;\njustify-content: center;\nalign-items: center;\ncolor:white;">\n    <h3>Sorry!!</h3>  \n    <p style="color:white !important;">No More New Giveaways</p>\n    <button ion-button outline center text-center color="lighter" (click)="refreshCards()">\n      <div>Refresh</div>\n    </button>\n</div>\n\n<div class="drawIndex" *ngIf="show_card">\n  <h5>\n      Your Index Number For  {{text_part}}  is\n  </h5>\n  <h2>\n    {{ index_no }}\n  </h2>\n</div>\n\n<ion-content padding="">\n    <!-- <div class="entry-no-back" [ngClass]="clStatus ? \'\' : \'show-no\' ">\n        <h2 class="entry-no">5</h2>\n      </div> -->\n\n      \n    <ion-slides let slides pager="true" paginationType="bullets" effect="slide" centeredSlides="true" spaceBetween="25" (ionSlideDidChange)="slideChanged()"   >\n        <!-- (ionSlideReachEnd) = "endReached()" -->\n        \n        <ion-slide width="10px" *ngFor="let item of cards" [ngClass]="clStatus ? \'element-animation\' : \'\' ">\n          <div  [ngStyle]="{\'background-image\': \'url(\'+ item.giveaway_media_link +\')\'}" class="slide-image cover-bg flex-div">\n            <ion-spinner name="crescent" class="spinner-center"></ion-spinner>\n          </div>\n          <h2 class="slide-title">\n            {{ item.giveaway_title }} \n            <!-- Pick the <b>Gift {{ item }}</b> -->\n          </h2>\n          <div class="des-container">\n              \n              <p>\n                  {{ (item.description | slice:0:180) + \'...\'}}\n                  </p>\n          </div>\n          \n          <!-- <button ion-fab (click)="voteUp(i)">{{ i }}</button> -->\n          <ion-row>\n              <ion-col style="position:absolute;\n              bottom: 0px;">\n                <button ion-button outline center text-center class="btn-gift-outline" (click)="openFullView(item.giveaway_code,currentIndex)">\n                  <div>View More</div>\n                </button>\n              </ion-col>\n            </ion-row>\n          \n        </ion-slide>\n\n        \n    \n      </ion-slides>\n\n      \n      <ion-fab bottom center *ngIf="showBtn">\n        <button ion-fab (click)="grabIt(currentIndex)" class="gift-button" [ngClass]="btnStatus ? \'btn-animation\' : \'\' "> <ion-icon name="basket"></ion-icon></button>\n      </ion-fab>\n</ion-content>\n'/*ion-inline-end:"/Users/localuser/gift-prod/src/pages/home/home.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* PopoverController */],
            __WEBPACK_IMPORTED_MODULE_3__providers_auth_auth__["a" /* AuthProvider */],
            __WEBPACK_IMPORTED_MODULE_4__providers_session_session__["a" /* SessionProvider */],
            __WEBPACK_IMPORTED_MODULE_5__ionic_storage__["b" /* Storage */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* Events */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* ToastController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["c" /* App */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* LoadingController */]])
    ], HomePage);
    return HomePage;
}());

//# sourceMappingURL=home.js.map

/***/ }),

/***/ 384:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(385);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_module__ = __webpack_require__(389);


Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_1__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 389:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__(46);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_http__ = __webpack_require__(84);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_storage__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_keyboard__ = __webpack_require__(380);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_native_onesignal__ = __webpack_require__(724);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ionic_native_social_sharing__ = __webpack_require__(337);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_ionic_image_loader__ = __webpack_require__(178);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__ionic_native_network__ = __webpack_require__(381);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__app_component__ = __webpack_require__(725);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__pages_home_home__ = __webpack_require__(340);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__pages_list_list__ = __webpack_require__(726);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__pages_login_login__ = __webpack_require__(43);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__pages_signup_signup__ = __webpack_require__(99);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__pages_otp_otp__ = __webpack_require__(181);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__pages_fgt_pass_fgt_pass__ = __webpack_require__(179);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__pages_reset_pwd_reset_pwd__ = __webpack_require__(180);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__pages_profile_profile__ = __webpack_require__(182);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__pages_menu_menu__ = __webpack_require__(51);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__pages_edit_user_edit_user__ = __webpack_require__(176);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__pages_full_view_full_view__ = __webpack_require__(177);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_22__pages_entry_entry__ = __webpack_require__(98);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_23__pages_winnings_winnings__ = __webpack_require__(101);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_24__pages_comp_view_comp_view__ = __webpack_require__(50);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_25__pages_user_user__ = __webpack_require__(183);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_26__pages_company_company__ = __webpack_require__(336);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_27__pages_search_search__ = __webpack_require__(100);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_28__pages_tutorial_tutorial__ = __webpack_require__(73);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_29__ionic_native_status_bar__ = __webpack_require__(382);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_30__ionic_native_splash_screen__ = __webpack_require__(383);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_31__providers_api_api__ = __webpack_require__(62);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_32__providers_auth_auth__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_33_firebase__ = __webpack_require__(71);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_33_firebase___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_33_firebase__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_34__ionic_native_facebook__ = __webpack_require__(154);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_35__ionic_native_firebase__ = __webpack_require__(70);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_36__ionic_native_file__ = __webpack_require__(63);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_37__ionic_native_transfer__ = __webpack_require__(133);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_38__ionic_native_file_path__ = __webpack_require__(134);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_39__ionic_native_camera__ = __webpack_require__(135);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_40__providers_session_session__ = __webpack_require__(34);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

































//firebase implementation

//facebook native


//file upload and camera native





var config = {
    apiKey: "AIzaSyD2c-44AhMs9xx8Dd7EvC0F-6Dmbzz6A-0",
    authDomain: "gift-in-app.firebaseapp.com",
    databaseURL: "https://gift-in-app.firebaseio.com",
    projectId: "gift-in-app",
    storageBucket: "gift-in-app.appspot.com",
    messagingSenderId: "538347117691"
};
__WEBPACK_IMPORTED_MODULE_33_firebase___default.a.initializeApp(config);
var AppModule = (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_10__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_11__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_12__pages_list_list__["a" /* ListPage */],
                __WEBPACK_IMPORTED_MODULE_13__pages_login_login__["a" /* LoginPage */],
                __WEBPACK_IMPORTED_MODULE_14__pages_signup_signup__["a" /* SignupPage */],
                __WEBPACK_IMPORTED_MODULE_15__pages_otp_otp__["a" /* OtpPage */],
                __WEBPACK_IMPORTED_MODULE_16__pages_fgt_pass_fgt_pass__["a" /* FgtPassPage */],
                __WEBPACK_IMPORTED_MODULE_17__pages_reset_pwd_reset_pwd__["a" /* ResetPwdPage */],
                __WEBPACK_IMPORTED_MODULE_18__pages_profile_profile__["a" /* ProfilePage */],
                __WEBPACK_IMPORTED_MODULE_19__pages_menu_menu__["a" /* MenuPage */],
                __WEBPACK_IMPORTED_MODULE_20__pages_edit_user_edit_user__["a" /* EditUserPage */],
                __WEBPACK_IMPORTED_MODULE_21__pages_full_view_full_view__["a" /* FullViewPage */],
                __WEBPACK_IMPORTED_MODULE_22__pages_entry_entry__["a" /* EntryPage */],
                __WEBPACK_IMPORTED_MODULE_23__pages_winnings_winnings__["a" /* WinningsPage */],
                __WEBPACK_IMPORTED_MODULE_24__pages_comp_view_comp_view__["a" /* CompViewPage */],
                __WEBPACK_IMPORTED_MODULE_25__pages_user_user__["a" /* UserPage */],
                __WEBPACK_IMPORTED_MODULE_26__pages_company_company__["a" /* CompanyPage */],
                __WEBPACK_IMPORTED_MODULE_27__pages_search_search__["a" /* SearchPage */],
                __WEBPACK_IMPORTED_MODULE_28__pages_tutorial_tutorial__["a" /* TutorialPage */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_3__angular_http__["c" /* HttpModule */],
                __WEBPACK_IMPORTED_MODULE_4__ionic_storage__["a" /* IonicStorageModule */].forRoot(),
                __WEBPACK_IMPORTED_MODULE_8_ionic_image_loader__["b" /* IonicImageLoader */].forRoot(),
                // ionicBootstrap(MyApp, { scrollAssist: false, autoFocusAssist: false }),
                __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["g" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_10__app_component__["a" /* MyApp */], {
                    // $ionicConfigProvider
                    // scrollPadding: false,
                    scrollAssist: false,
                    autoFocusAssist: false
                }, {
                    links: [
                        { loadChildren: '../pages/comp-view/comp-view.module#CompViewPageModule', name: 'CompViewPage', segment: 'comp-view', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/edit-user/edit-user.module#EditUserPageModule', name: 'EditUserPage', segment: 'edit-user', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/entry/entry.module#EntryPageModule', name: 'EntryPage', segment: 'entry', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/full-view/full-view.module#FullViewPageModule', name: 'FullViewPage', segment: 'full-view', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/fgt-pass/fgt-pass.module#FgtPassPageModule', name: 'FgtPassPage', segment: 'fgt-pass', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/login/login.module#LoginPageModule', name: 'LoginPage', segment: 'login', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/menu/menu.module#MenuPageModule', name: 'MenuPage', segment: 'menu', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/otp/otp.module#OtpPageModule', name: 'OtpPage', segment: 'otp', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/profile/profile.module#ProfilePageModule', name: 'ProfilePage', segment: 'profile', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/reset-pwd/reset-pwd.module#ResetPwdPageModule', name: 'ResetPwdPage', segment: 'reset-pwd', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/search/search.module#SearchPageModule', name: 'SearchPage', segment: 'search', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/signup/signup.module#SignupPageModule', name: 'SignupPage', segment: 'signup', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/tutorial/tutorial.module#TutorialPageModule', name: 'TutorialPage', segment: 'tutorial', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/user/user.module#UserPageModule', name: 'UserPage', segment: 'user', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/winnings/winnings.module#WinningsPageModule', name: 'WinningsPage', segment: 'winnings', priority: 'low', defaultHistory: [] }
                    ]
                }),
            ],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["e" /* IonicApp */]],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_10__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_11__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_12__pages_list_list__["a" /* ListPage */],
                __WEBPACK_IMPORTED_MODULE_13__pages_login_login__["a" /* LoginPage */],
                __WEBPACK_IMPORTED_MODULE_14__pages_signup_signup__["a" /* SignupPage */],
                __WEBPACK_IMPORTED_MODULE_15__pages_otp_otp__["a" /* OtpPage */],
                __WEBPACK_IMPORTED_MODULE_16__pages_fgt_pass_fgt_pass__["a" /* FgtPassPage */],
                __WEBPACK_IMPORTED_MODULE_17__pages_reset_pwd_reset_pwd__["a" /* ResetPwdPage */],
                __WEBPACK_IMPORTED_MODULE_18__pages_profile_profile__["a" /* ProfilePage */],
                __WEBPACK_IMPORTED_MODULE_19__pages_menu_menu__["a" /* MenuPage */],
                __WEBPACK_IMPORTED_MODULE_20__pages_edit_user_edit_user__["a" /* EditUserPage */],
                __WEBPACK_IMPORTED_MODULE_21__pages_full_view_full_view__["a" /* FullViewPage */],
                __WEBPACK_IMPORTED_MODULE_22__pages_entry_entry__["a" /* EntryPage */],
                __WEBPACK_IMPORTED_MODULE_23__pages_winnings_winnings__["a" /* WinningsPage */],
                __WEBPACK_IMPORTED_MODULE_24__pages_comp_view_comp_view__["a" /* CompViewPage */],
                __WEBPACK_IMPORTED_MODULE_25__pages_user_user__["a" /* UserPage */],
                __WEBPACK_IMPORTED_MODULE_26__pages_company_company__["a" /* CompanyPage */],
                __WEBPACK_IMPORTED_MODULE_27__pages_search_search__["a" /* SearchPage */],
                __WEBPACK_IMPORTED_MODULE_28__pages_tutorial_tutorial__["a" /* TutorialPage */]
            ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_29__ionic_native_status_bar__["a" /* StatusBar */],
                __WEBPACK_IMPORTED_MODULE_9__ionic_native_network__["a" /* Network */],
                __WEBPACK_IMPORTED_MODULE_30__ionic_native_splash_screen__["a" /* SplashScreen */],
                __WEBPACK_IMPORTED_MODULE_34__ionic_native_facebook__["a" /* Facebook */],
                __WEBPACK_IMPORTED_MODULE_5__ionic_native_keyboard__["a" /* Keyboard */],
                __WEBPACK_IMPORTED_MODULE_35__ionic_native_firebase__["a" /* Firebase */],
                __WEBPACK_IMPORTED_MODULE_36__ionic_native_file__["a" /* File */],
                __WEBPACK_IMPORTED_MODULE_37__ionic_native_transfer__["a" /* Transfer */],
                __WEBPACK_IMPORTED_MODULE_39__ionic_native_camera__["a" /* Camera */],
                __WEBPACK_IMPORTED_MODULE_6__ionic_native_onesignal__["a" /* OneSignal */],
                __WEBPACK_IMPORTED_MODULE_7__ionic_native_social_sharing__["a" /* SocialSharing */],
                __WEBPACK_IMPORTED_MODULE_38__ionic_native_file_path__["a" /* FilePath */],
                { provide: __WEBPACK_IMPORTED_MODULE_1__angular_core__["u" /* ErrorHandler */], useClass: __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["f" /* IonicErrorHandler */] },
                __WEBPACK_IMPORTED_MODULE_31__providers_api_api__["a" /* ApiProvider */],
                __WEBPACK_IMPORTED_MODULE_32__providers_auth_auth__["a" /* AuthProvider */],
                __WEBPACK_IMPORTED_MODULE_40__providers_session_session__["a" /* SessionProvider */]
            ]
        })
    ], AppModule);
    return AppModule;
}());

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 43:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoginPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__signup_signup__ = __webpack_require__(99);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__fgt_pass_fgt_pass__ = __webpack_require__(179);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__menu_menu__ = __webpack_require__(51);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__user_user__ = __webpack_require__(183);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__tutorial_tutorial__ = __webpack_require__(73);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__angular_forms__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__providers_auth_auth__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__providers_session_session__ = __webpack_require__(34);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__ionic_storage__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11_firebase__ = __webpack_require__(71);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11_firebase___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_11_firebase__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__ionic_native_facebook__ = __webpack_require__(154);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__ionic_native_firebase__ = __webpack_require__(70);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};














/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var LoginPage = (function () {
    function LoginPage(navCtrl, navParams, loadingCtrl, alertCtrl, formBuilder, authProvider, facebook, fire, storage, session) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.loadingCtrl = loadingCtrl;
        this.alertCtrl = alertCtrl;
        this.formBuilder = formBuilder;
        this.authProvider = authProvider;
        this.facebook = facebook;
        this.fire = fire;
        this.storage = storage;
        this.session = session;
        this.submitAttempt = false;
        this.user = this.formBuilder.group({
            email: ['', __WEBPACK_IMPORTED_MODULE_7__angular_forms__["f" /* Validators */].compose([__WEBPACK_IMPORTED_MODULE_7__angular_forms__["f" /* Validators */].required, __WEBPACK_IMPORTED_MODULE_7__angular_forms__["f" /* Validators */].email])],
            password: ['', __WEBPACK_IMPORTED_MODULE_7__angular_forms__["f" /* Validators */].required],
        });
    }
    //get logged in users data
    LoginPage.prototype.getUserData = function (user_id) {
        var _this = this;
        var url = "user/user?id=" + user_id;
        console.log(url);
        this.authProvider.getData(url).then(function (result) {
            _this.storage.set('user_id', result[0].user_code);
            _this.storage.set('logged_in', true);
            console.log("user_data", result[0].user_code);
        });
    };
    //sign in function 
    LoginPage.prototype.signin = function (data) {
        var _this = this;
        var loading = this.loadingCtrl.create({
            spinner: 'crescent',
            content: 'Loading Please Wait...'
        });
        loading.present();
        var url = "user/user";
        this.submitAttempt = true;
        this.authProvider.postAuthData(data, url).then(function (result) {
            loading.dismiss();
            // console.log(result);
            if (result.auth) {
                // console.log(result.user_id);
                _this.getUserData(result.user_id);
                _this.storage.set('logged_in', true);
                _this.storage.set('user_id', result.user_id);
                _this.storage.get('fist_login').then(function (res) {
                    console.log("gotdata from signin method");
                    if (res) {
                        _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_5__user_user__["a" /* UserPage */], { 'user_code': result.user_id });
                    }
                    else {
                        _this.session.hasSeenTutorial().then(function (hasSeen) {
                            console.log("hasSeen", hasSeen);
                            if (hasSeen) {
                                _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_4__menu_menu__["a" /* MenuPage */]);
                            }
                            else {
                                _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_6__tutorial_tutorial__["a" /* TutorialPage */]);
                            }
                        });
                    }
                });
            }
            else {
                var alert_1 = _this.alertCtrl.create({
                    title: "Error",
                    subTitle: result.msg,
                    enableBackdropDismiss: false,
                    buttons: ['OK']
                });
                alert_1.present();
            }
        }).catch(function (err) {
            console.log(err);
        });
    };
    //facebook native login
    LoginPage.prototype.signupFacebook = function () {
        var _this = this;
        var loading = this.loadingCtrl.create({
            spinner: 'crescent',
            content: 'Loading Please Wait...'
        });
        loading.present();
        // this.showLoader();
        this.facebook.login(["email", "public_profile", "user_friends"]).then(function (loginResponse) {
            _this.facebook.api("/me?fields=email,name,picture,first_name,last_name,id,age_range,link,locale,gender", ["public_profile", "email", "user_friends"]).then(function (result) {
                // console.log("fb api",JSON.stringify(result));
                _this.facebook_user = {
                    first_name: result.first_name,
                    last_name: result.last_name,
                    name: result.name,
                    email: result.email,
                    photo: result.picture.data.url,
                    gender: result.gender,
                    age: result.min,
                    social_id: result.id,
                    locale: result.locale,
                    link: result.link,
                };
            });
            var credentials = __WEBPACK_IMPORTED_MODULE_11_firebase___default.a.auth.FacebookAuthProvider.credential(loginResponse.authResponse.accessToken);
            __WEBPACK_IMPORTED_MODULE_11_firebase___default.a.auth().signInWithCredential(credentials).then(function (res) {
                var data = _this.facebook_user;
                var url = "social_user/social_users";
                console.log(JSON.stringify(data));
                _this.authProvider.postAuthData(data, url).then(function (result_auth) {
                    // this.loading.dismiss();
                    console.log("ers", result_auth);
                    loading.dismiss();
                    if (result_auth.auth) {
                        //this.showAlert("Success",result_auth.msg);
                        // this.navCtrl.setRoot(MenuPage);
                        _this.session.hasSeenTutorial().then(function (hasSeen) {
                            console.log("hasSeen", hasSeen);
                            if (hasSeen) {
                                _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_4__menu_menu__["a" /* MenuPage */]);
                            }
                            else {
                                _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_6__tutorial_tutorial__["a" /* TutorialPage */]);
                            }
                        });
                    }
                    else {
                        // this.showAlert("Error",result_auth.msg);
                        var alert_2 = _this.alertCtrl.create({
                            title: "Error",
                            subTitle: result_auth.msg,
                            enableBackdropDismiss: false,
                            buttons: ['OK']
                        });
                        alert_2.present();
                    }
                }).catch(function (err) {
                    // this.loading.dismiss();
                    console.log(err);
                });
            });
        }).catch(function (err) {
            // this.loading.dismiss();
        });
    };
    //show alert function
    LoginPage.prototype.showAlert = function (title, msg) {
        var alert = this.alertCtrl.create({
            title: title,
            subTitle: msg,
            enableBackdropDismiss: false,
            buttons: ['Continue']
        });
        alert.present();
    };
    //go to signup
    LoginPage.prototype.signUp = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__signup_signup__["a" /* SignupPage */]);
    };
    //go to reset password
    LoginPage.prototype.goToFgtPass = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__fgt_pass_fgt_pass__["a" /* FgtPassPage */]);
    };
    LoginPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-login',template:/*ion-inline-start:"/Users/localuser/gift-prod/src/pages/login/login.html"*/'<!--\n  Generated template for the LoginPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n\n<ion-content padding="" >\n  <ion-grid >\n    <div>\n      <div text-center class="text-white">\n      <h4>Sign In</h4>\n      <p>Please Sign In to continue</p>\n    </div>\n      <ion-row>\n        <ion-col>\n          <ion-list>\n            <form [formGroup]="user"  (ngSubmit)="signin(user.value)" >\n              <div class="item-container">\n              <ion-item class="px-10">\n                <ion-input type="email" placeholder="Email" clearInput [(ngModel)]="user.email" name="email" formControlName="email"></ion-input>\n              </ion-item>\n              <ion-item no-lines class="error-msg" text-wrap *ngIf="!user.controls.email.valid  && (user.controls.email.dirty || submitAttempt)">\n                <small>This field is required and should be valid email</small>\n              </ion-item>\n\n              <ion-item class="px-10">\n                <ion-input type="password"  placeholder="Password" clearInput [(ngModel)]="user.password" name="password" formControlName="password"></ion-input>\n              </ion-item>\n              <ion-item no-lines class="error-msg" text-wrap *ngIf="!user.controls.password.valid  && (user.controls.password.dirty || submitAttempt)">\n                <small>This field is required and should be valid email</small>\n              </ion-item>\n            </div>\n              <ion-row>\n                <ion-col class="px-0">\n                  <button type="submit" ion-button class="btn-gift" color="light" block [disabled]="!user.valid">Sign in</button>\n                  <!-- [disabled]="!user.valid" -->\n                </ion-col>\n              </ion-row>\n            \n            </form>\n            <p text-center class="text-white">Forgot Password?\n                <span (click)="goToFgtPass()"><b>Reset Password</b></span>\n              </p>\n          </ion-list>\n        </ion-col>\n      </ion-row>\n     \n      <ion-row>\n        <ion-col text-center class="px-0">\n          <button ion-button class="social-icon social-facebook" (click)="signupFacebook()">\n            <!-- <ion-icon name="logo-facebook"></ion-icon> -->\n            <img src="assets/imgs/fb_login.png" alt="">\n          </button>\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col text-center class="text-white">\n\n          <p>Don\'t have account?\n            <span (click)="signUp()" ><b>Sign up now</b></span>\n          </p>\n          \n\n        </ion-col>\n      </ion-row>\n    </div>\n  </ion-grid>\n\n</ion-content>'/*ion-inline-end:"/Users/localuser/gift-prod/src/pages/login/login.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_7__angular_forms__["a" /* FormBuilder */],
            __WEBPACK_IMPORTED_MODULE_8__providers_auth_auth__["a" /* AuthProvider */],
            __WEBPACK_IMPORTED_MODULE_12__ionic_native_facebook__["a" /* Facebook */],
            __WEBPACK_IMPORTED_MODULE_13__ionic_native_firebase__["a" /* Firebase */],
            __WEBPACK_IMPORTED_MODULE_10__ionic_storage__["b" /* Storage */],
            __WEBPACK_IMPORTED_MODULE_9__providers_session_session__["a" /* SessionProvider */]])
    ], LoginPage);
    return LoginPage;
}());

//# sourceMappingURL=login.js.map

/***/ }),

/***/ 50:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CompViewPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_auth_auth__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_session_session__ = __webpack_require__(34);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/**
 * Generated class for the CompViewPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var CompViewPage = (function () {
    function CompViewPage(navCtrl, navParams, auth, session) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.auth = auth;
        this.session = session;
        this.show_draw_detail = false;
        this.show_winners = false;
        this.entry_index = 0;
        this.data_giveaway = [];
        this.data_media = [];
        this.data_link = [];
        this.company = [];
        this.winners_list = [];
        this.draw_data = [];
        this.session.getUser();
        this.user_id = this.session.user_data.user_code;
        this.giveaway_code = this.navParams.get('giveaway_code');
        this.getGiveawayData(this.giveaway_code);
    }
    CompViewPage.prototype.getGiveawayData = function (giveaway_code) {
        var _this = this;
        var url = "giveaway/giveaway?id=" + giveaway_code + "&user_id=" + this.user_id;
        this.auth.getData(url).then(function (res) {
            _this.data_giveaway = res.data_giveaway[0];
            _this.data_media = res.data_media[0];
            _this.data_link = res.data_link;
            _this.company = res.company[0];
            _this.like_count = res.like_count[0].like_count;
            if (res.entry_no.length > 0) {
                _this.entry_index = res.entry_no[0].entry_no;
            }
            if (res.winners.length > 0) {
                console.log("winner length", res.winners.length);
                _this.show_winners = true;
                _this.winners_list = res.winners;
            }
            if (res.draw_data.length > 0) {
                _this.show_draw_detail = true;
                _this.draw_data = res.draw_data;
            }
        });
    };
    CompViewPage.prototype.openUrl = function (link) {
        window.open(link, '_system');
    };
    CompViewPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad CompViewPage');
    };
    CompViewPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-comp-view',template:/*ion-inline-start:"/Users/localuser/gift-prod/src/pages/comp-view/comp-view.html"*/'<!--\n  Generated template for the CompViewPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <ion-title> </ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content class="comp-view-container">\n  <div class="content-container">\n\n\n    <div class="image-container flex-div mh-150">\n        <ion-spinner name="crescent" class="spinner-center"></ion-spinner>\n      <img src="{{ data_media.giveaway_media_link }}">\n    </div>\n\n    <div class="meta-container px-10">\n\n      <h3 class="text-giftin">{{data_giveaway.giveaway_title}}</h3>\n      <p>Posted by\n        <b>\n          <span class="text-giftin">{{company.com_name}}</span>\n        </b> on\n        <span class="text-giftin">{{data_giveaway.start_point}}</span>\n        Expires on <span class="text-giftin">{{data_giveaway.start_point}}</span>\n      </p>\n      <p class="dflex-ai-center text-giftin">\n        <ion-icon name="heart-outline" class="text-25 "></ion-icon>\n        <span class="ml-5"> {{like_count}} </span>\n      </p>\n      <p class="text-giftin" *ngIf="entry_index">\n          <strong>Your entry No: </strong> <span> {{entry_index}} </span>\n        </p>\n    </div>\n    <div class="info-container px-10">\n      <p>{{data_giveaway.description}}</p>\n      <hr>\n      <div class="link-container" *ngIf="data_link">\n        <button (click)="openUrl(data_giveaway.website)" clear color="light" ion-fab  class="link-icons" >\n          <i class="fa fa-globe"></i>\n        </button>\n        <button (click)="openUrl(item.link)" *ngFor="let item of data_link" clear color="light" ion-fab  class="link-icons" >\n            <i class="fa fa-{{item.link_media}}"></i>\n          </button>\n        <!-- <button clear color="light" ion-fab  class="link-icons" ><i class="fa fa-facebook-f"></i></button>\n        <button clear color="light" ion-fab  class="link-icons" ><i class="fa fa-twitter"></i></button>\n        <button clear color="light" ion-fab  class="link-icons" ><i class="fa fa-youtube"></i></button>\n        <button clear color="light" ion-fab  class="link-icons" ><i class="fa fa-instagram"></i></button> -->\n    </div>\n    </div>\n  </div>\n  <div class="draw-container px-0 mt-20" *ngIf="show_draw_detail">\n    <h6 class="px-10 text-16 text-giftin"> Draw Details</h6>\n    <ion-card>\n      <ion-card-content>\n        <div *ngFor="let lot of draw_data" class="">\n          <p>{{lot.lottery_name }} on <small>{{ lot.lottery_date }}</small></p>\n          <p class="lotteries text-giftin">\n              <span class="lot-numbers">{{lot.result}}</span>\n            <!-- <span *ngFor="let num of [31,22,3,45,6,43,76]" class="lot-numbers">{{num}}</span> -->\n          </p>\n        </div>\n      </ion-card-content>\n    </ion-card>\n  </div>\n  <div class="winner-container px-0 mt-20" *ngIf="show_winners">\n    <h6 class="px-10 text-16 text-giftin"> Winners</h6>\n    <ion-card>\n      <ion-card-content>\n        <ion-slides *ngIf="winners_list" spaceBetween="5"  autoplay="1000" slidesPerView="4" pager="true">\n          <ion-slide  *ngFor="let winner of winners_list">\n            <div class="winners" >\n              <ion-avatar>\n                <img src="{{winner.user_photo}}">\n              </ion-avatar>\n              <p class="text-elepsis">{{ winner.f_name }}</p>\n            </div>\n\n          </ion-slide>\n\n        </ion-slides>\n      </ion-card-content>\n    </ion-card>\n  </div>\n\n</ion-content>'/*ion-inline-end:"/Users/localuser/gift-prod/src/pages/comp-view/comp-view.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__providers_auth_auth__["a" /* AuthProvider */], __WEBPACK_IMPORTED_MODULE_3__providers_session_session__["a" /* SessionProvider */]])
    ], CompViewPage);
    return CompViewPage;
}());

//# sourceMappingURL=comp-view.js.map

/***/ }),

/***/ 51:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MenuPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__home_home__ = __webpack_require__(340);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__login_login__ = __webpack_require__(43);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__signup_signup__ = __webpack_require__(99);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__profile_profile__ = __webpack_require__(182);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__entry_entry__ = __webpack_require__(98);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__winnings_winnings__ = __webpack_require__(101);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__search_search__ = __webpack_require__(100);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__tutorial_tutorial__ = __webpack_require__(73);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__ionic_storage__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__providers_auth_auth__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__providers_session_session__ = __webpack_require__(34);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



// import { ListPage } from '../list/list';










/**
 * Generated class for the MenuPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var MenuPage = (function () {
    function MenuPage(navCtrl, navParams, storage, authProvider, session) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.storage = storage;
        this.authProvider = authProvider;
        this.session = session;
        this.user_photo = "";
        this.user_data = [];
        //this.session.getUser();
        this.user_data = this.session.user_data;
        // this.setUserData();
        this.homePage = __WEBPACK_IMPORTED_MODULE_2__home_home__["a" /* HomePage */];
        this.activePage = __WEBPACK_IMPORTED_MODULE_2__home_home__["a" /* HomePage */];
        // this.childNavCtrl.push(ProfilePage);
        this.pages = [
            { title: 'Home', component: __WEBPACK_IMPORTED_MODULE_2__home_home__["a" /* HomePage */], icon: 'home' },
            // { title: 'Sign in', component: LoginPage, icon: 'person' },
            // { title: 'Sign up', component: SignupPage, icon: 'person' },
            { title: 'My Profile', component: __WEBPACK_IMPORTED_MODULE_5__profile_profile__["a" /* ProfilePage */], icon: 'person' },
            // { title: 'Otp', component: OtpPage, icon: 'lock' },
            // { title: 'edit', component: EditUserPage, icon: 'lock' },
            // { title: 'Fgt pass', component: FgtPassPage, icon: 'lock' },
            // { title: 'full view', component: FullViewPage, icon: 'lock' },
            { title: 'My Basket', component: __WEBPACK_IMPORTED_MODULE_6__entry_entry__["a" /* EntryPage */], icon: 'heart' },
            // { title: 'Reset pass', component: ResetPwdPage, icon: 'lock' },
            { title: 'Winnings', component: __WEBPACK_IMPORTED_MODULE_7__winnings_winnings__["a" /* WinningsPage */], icon: 'trophy' },
            // { title: 'Completed', component: CompViewPage, icon: 'check' },
            // { title: 'User info', component: UserPage, icon: 'person' }
            { title: 'Tutorial', component: __WEBPACK_IMPORTED_MODULE_9__tutorial_tutorial__["a" /* TutorialPage */], icon: 'color-wand' },
            { title: 'Search', component: __WEBPACK_IMPORTED_MODULE_8__search_search__["a" /* SearchPage */], icon: 'search' }
        ];
        this.activePage = this.pages[0];
        // this.openPageS("home");
        // this.openPage(this.pages[8]);
    }
    MenuPage.prototype.ionViewDidLoad = function () {
        this.setUserData();
    };
    MenuPage.prototype.setUserData = function () {
        var _this = this;
        this.session.getUser().then(function (res) {
            _this.user_data = _this.session.user_data;
            console.log("user_photo", _this.user_data);
        });
    };
    MenuPage.prototype.checkActive = function (page) {
        return page == this.activePage;
    };
    MenuPage.prototype.openPage = function (page) {
        console.log(page);
        // this.childNavCtrl.push(page.component);
        // this.childNavCtrl.push(page.component);
        this.childNavCtrl.setRoot(page.component);
        // this.childNavCtrl.setRoot(CompanyPage);
        this.activePage = page;
    };
    MenuPage.prototype.openPageS = function (pageName) {
        // console.log("ac pae",this.navCtrl.getActive().component);
        switch (pageName) {
            case "home":
                this.childNavCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_2__home_home__["a" /* HomePage */]);
                break;
            case "profile":
                this.childNavCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_5__profile_profile__["a" /* ProfilePage */]);
                break;
            case "login":
                this.childNavCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_3__login_login__["a" /* LoginPage */]);
                break;
            case "signup":
                this.childNavCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_4__signup_signup__["a" /* SignupPage */]);
                break;
        }
    };
    MenuPage.prototype.logout = function () {
        this.storage.set('logged_in', false);
        this.storage.set('user_id', '');
        this.storage.remove('user_id');
        this.storage.remove('logged_in');
        this.storage.remove('fist_login');
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_3__login_login__["a" /* LoginPage */]);
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])('content'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */])
    ], MenuPage.prototype, "childNavCtrl", void 0);
    MenuPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-menu',template:/*ion-inline-start:"/Users/localuser/gift-prod/src/pages/menu/menu.html"*/'<ion-menu type="push" persistent="true" swipeEnabled="true" [content]="content">\n  <!-- <ion-header>\n    <ion-toolbar>\n      <ion-title>Menu</ion-title>\n    </ion-toolbar>\n  </ion-header> -->\n\n  <ion-content class="menu-content">\n\n    <div class="user-info-container">\n        <div class="avatar center-cropped-menu" [ngStyle]="{\'background-image\': \'url(\'+ user_data.photo +\')\'}" ></div>\n\n      <!-- <img class="avatar" src="http://demos.creative-tim.com/material-kit/assets/img/kit/faces/christian.jpg"> -->\n     <div class="mt-m-50">\n        <h4> {{ user_data.first_name }} {{ user_data.last_name }} </h4>\n              <p> {{ user_data.city }}, {{ user_data.country }} </p>\n         \n     </div>\n    </div>\n    \n\n    <ion-list class="menu-list mt-10" no-lines>\n      <!-- <ion-item-divider color="danger">Main App</ion-item-divider> -->\n      <ion-item class="menu-item" text-wrap *ngFor="let p of pages" menuClose  (click)="openPage(p)" [class.activehighlight]="checkActive(p)" >\n        <ion-icon [name]="p.icon" item-left large></ion-icon>\n        <h2> {{p.title}} </h2>\n      </ion-item>\n\n     \n\n      <!-- <ion-item color="light"  class="menu-item" text-wrap  menuClose (click)="openPage(\'about\')">\n        <ion-icon name="planet" item-left large></ion-icon>\n        <h2> About </h2>\n      </ion-item> -->\n\n      <!-- <ion-item-divider color="danger">Profile</ion-item-divider> -->\n      <!-- <ion-item color="light"  class="menu-item" text-wrap  menuClose (click)="openPage(\'login\')">\n        <ion-icon name="contact" item-left large></ion-icon>\n        <h2> Login </h2>\n      </ion-item>\n      <ion-item color="light"  class="menu-item" text-wrap  menuClose (click)="openPage(\'signup\')">\n        <ion-icon name="contact" item-left large></ion-icon>\n        <h2> Sign Up </h2>\n      </ion-item>\n\n      <ion-item color="light"  class="menu-item" text-wrap  menuClose (click)="openPage(\'profile\')">\n        <ion-icon name="contact" item-left large></ion-icon>\n        <h2> My Profile </h2>\n      </ion-item>\n\n      <ion-item color="light"  class="menu-item" text-wrap  menuClose (click) = "openPage(\'entries\')">\n        <ion-icon name="checkmark-circle" item-left large></ion-icon>\n        <h2> Entries </h2>\n      </ion-item>\n\n      <ion-item color="light"  class="menu-item" text-wrap  menuClose (click)="openPage(\'winnings\')">\n        <ion-icon name="trophy" item-left large></ion-icon>\n        <h2> Winnings </h2>\n      </ion-item> -->\n\n      <ion-item color="light"  class="menu-item" text-wrap  menuClose (click)="logout()">\n        <ion-icon name="log-out" item-left large></ion-icon>\n        <h2> Logout </h2>\n      </ion-item>\n\n    </ion-list>\n  </ion-content>\n\n</ion-menu>\n\n<ion-nav #content [root]= "homePage"></ion-nav>'/*ion-inline-end:"/Users/localuser/gift-prod/src/pages/menu/menu.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */], __WEBPACK_IMPORTED_MODULE_10__ionic_storage__["b" /* Storage */], __WEBPACK_IMPORTED_MODULE_11__providers_auth_auth__["a" /* AuthProvider */], __WEBPACK_IMPORTED_MODULE_12__providers_session_session__["a" /* SessionProvider */]])
    ], MenuPage);
    return MenuPage;
}());

//# sourceMappingURL=menu.js.map

/***/ }),

/***/ 62:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ApiProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
// import { HttpClient } from '@angular/common/http';

/*
  Generated class for the ApiProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
var ApiProvider = (function () {
    function ApiProvider() {
        this.base_url = "http://testapi.giftinapp.com/index.php/";
        //  this.base_url = "http://localhost/giftin_rest_api/index.php/";
    }
    ApiProvider.prototype.init = function () {
        return this.base_url;
    };
    ApiProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [])
    ], ApiProvider);
    return ApiProvider;
}());

//# sourceMappingURL=api.js.map

/***/ }),

/***/ 725:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__ = __webpack_require__(382);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_keyboard__ = __webpack_require__(380);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_splash_screen__ = __webpack_require__(383);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_network__ = __webpack_require__(381);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_storage__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__pages_login_login__ = __webpack_require__(43);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__pages_menu_menu__ = __webpack_require__(51);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__providers_session_session__ = __webpack_require__(34);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10_ionic_image_loader__ = __webpack_require__(178);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__pages_tutorial_tutorial__ = __webpack_require__(73);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};












var MyApp = (function () {
    function MyApp(platform, statusBar, splashScreen, session, keyboard, imageLoaderConfig, network, alertCtrl, storage) {
        this.platform = platform;
        this.statusBar = statusBar;
        this.splashScreen = splashScreen;
        this.session = session;
        this.keyboard = keyboard;
        this.imageLoaderConfig = imageLoaderConfig;
        this.network = network;
        this.alertCtrl = alertCtrl;
        this.storage = storage;
        this.initializeApp();
        this.getLoginStatus();
        // disable spinners by default, you can add [spinner]="true" to a specific component instance later on to override this
        imageLoaderConfig.enableSpinner(true);
        // set the maximum concurrent connections to 10
        imageLoaderConfig.setConcurrency(10);
        this.imageLoaderConfig.enableDebugMode();
    }
    MyApp.prototype.initializeApp = function () {
        var _this = this;
        this.platform.ready().then(function () {
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            // this.splashScreen.show();
            _this.statusBar.styleDefault();
            _this.splashScreen.hide();
            var notificationOpenedCallback = function (jsonData) {
                console.log('notificationOpenedCallback: ' + JSON.stringify(jsonData));
            };
            window["plugins"].OneSignal
                .startInit("2b1fc1c6-828c-434a-b504-cd68458f042e", "538347117691")
                .handleNotificationOpened(notificationOpenedCallback)
                .endInit();
            // this.keyboard.disableScroll(true);
        });
        this.network.onDisconnect().subscribe(function () {
            _this.alert = _this.alertCtrl.create({
                title: 'Attention',
                subTitle: 'No Internet Connection. Please switch on Wifi or Mobile Data',
                // buttons: ['OK'],
                enableBackdropDismiss: false,
            });
            console.log('network was disconnected :-(');
            _this.alert.present();
        });
        this.network.onConnect().subscribe(function () {
            console.log('network was connected :-)');
            _this.alert.dismiss();
        });
    };
    //check login status
    MyApp.prototype.getLoginStatus = function () {
        var _this = this;
        this.session.checkLogIn().then(function (status) {
            if (status) {
                _this.session.hasSeenTutorial().then(function (hasSeen) {
                    console.log("hasSeen", hasSeen);
                    if (hasSeen) {
                        _this.rootPage = __WEBPACK_IMPORTED_MODULE_8__pages_menu_menu__["a" /* MenuPage */];
                    }
                    else {
                        _this.rootPage = __WEBPACK_IMPORTED_MODULE_11__pages_tutorial_tutorial__["a" /* TutorialPage */];
                    }
                });
            }
            else {
                _this.rootPage = __WEBPACK_IMPORTED_MODULE_7__pages_login_login__["a" /* LoginPage */];
            }
        });
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* Nav */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* Nav */])
    ], MyApp.prototype, "nav", void 0);
    MyApp = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({template:/*ion-inline-start:"/Users/localuser/gift-prod/src/app/app.html"*/'<!-- <ion-menu [content]="content">\n  <ion-header>\n    <ion-toolbar>\n      <ion-title>Menu</ion-title>\n    </ion-toolbar>\n  </ion-header>\n\n  <ion-content>\n    <ion-list>\n      <button menuClose ion-item *ngFor="let p of pages" (click)="openPage(p)">\n        {{p.title}}\n      </button>\n    </ion-list>\n  </ion-content>\n\n</ion-menu>\n\n Disable swipe-to-go-back because it\'s poor UX to combine STGB with side menus -->\n<!-- <ion-nav [root]="rootPage" #content swipeBackEnabled="false"></ion-nav>  -->\n\n<ion-nav [root]="rootPage" #content swipeBackEnabled="false"></ion-nav>'/*ion-inline-end:"/Users/localuser/gift-prod/src/app/app.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* Platform */], __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__["a" /* StatusBar */],
            __WEBPACK_IMPORTED_MODULE_4__ionic_native_splash_screen__["a" /* SplashScreen */], __WEBPACK_IMPORTED_MODULE_9__providers_session_session__["a" /* SessionProvider */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_native_keyboard__["a" /* Keyboard */], __WEBPACK_IMPORTED_MODULE_10_ionic_image_loader__["a" /* ImageLoaderConfig */],
            __WEBPACK_IMPORTED_MODULE_5__ionic_native_network__["a" /* Network */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_6__ionic_storage__["b" /* Storage */]])
    ], MyApp);
    return MyApp;
}());

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 726:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ListPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ListPage = (function () {
    function ListPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        // If we navigated to this page, we will have an item available as a nav param
        this.selectedItem = navParams.get('item');
        // Let's populate this page with some filler content for funzies
        this.icons = ['flask', 'wifi', 'beer', 'football', 'basketball', 'paper-plane',
            'american-football', 'boat', 'bluetooth', 'build'];
        this.items = [];
        for (var i = 1; i < 11; i++) {
            this.items.push({
                title: 'Item ' + i,
                note: 'This is item #' + i,
                icon: this.icons[Math.floor(Math.random() * this.icons.length)]
            });
        }
    }
    ListPage_1 = ListPage;
    ListPage.prototype.itemTapped = function (event, item) {
        // That's right, we're pushing to ourselves!
        this.navCtrl.push(ListPage_1, {
            item: item
        });
    };
    ListPage = ListPage_1 = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-list',template:/*ion-inline-start:"/Users/localuser/gift-prod/src/pages/list/list.html"*/'<ion-header>\n  <ion-navbar>\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>List</ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content>\n  <ion-list>\n    <button ion-item *ngFor="let item of items" (click)="itemTapped($event, item)">\n      <ion-icon [name]="item.icon" item-start></ion-icon>\n      {{item.title}}\n      <div class="item-note" item-end>\n        {{item.note}}\n      </div>\n    </button>\n  </ion-list>\n  <div *ngIf="selectedItem" padding>\n    You navigated here from <b>{{selectedItem.title}}</b>\n  </div>\n</ion-content>\n'/*ion-inline-end:"/Users/localuser/gift-prod/src/pages/list/list.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */]])
    ], ListPage);
    return ListPage;
    var ListPage_1;
}());

//# sourceMappingURL=list.js.map

/***/ }),

/***/ 73:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TutorialPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_storage__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__menu_menu__ = __webpack_require__(51);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/**
 * Generated class for the TutorialPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var TutorialPage = (function () {
    function TutorialPage(navCtrl, navParams, storage) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.storage = storage;
        this.slides = [
            {
                title: "Hi! \n Welcome to Gift In!",
                description: "Here’s your best chance at winning giveaways.",
                image: "",
            },
            {
                title: "What is Gift In?",
                description: "<b>Gift In</b> is a free mobile app where you can stand a chance to win free giveaways from your favorite brands",
                image: "assets/img/ica-slidebox-img-2.png",
            },
            {
                title: "How Gift In Works?",
                description: "The <b>Gift In</b> is a cloud platform for managing and scaling Ionic apps with integrated services like push notifications, native builds, user auth, and live updating.",
                image: "assets/img/ica-slidebox-img-3.png",
            }
        ];
    }
    TutorialPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad TutorialPage');
    };
    TutorialPage.prototype.hasSeenTutorial = function () {
        this.storage.set('hasSeenTutorial', true);
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_3__menu_menu__["a" /* MenuPage */]);
    };
    TutorialPage.prototype.goToNextSlide = function () {
        this.slideCtrl.slideNext();
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* Slides */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* Slides */])
    ], TutorialPage.prototype, "slideCtrl", void 0);
    TutorialPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-tutorial',template:/*ion-inline-start:"/Users/localuser/gift-prod/src/pages/tutorial/tutorial.html"*/'<ion-header no-border>\n  <ion-navbar>\n    <ion-title>Tutorial</ion-title>\n    <ion-buttons end>\n      <button ion-button color="lighter" (click)="hasSeenTutorial()">Skip</button>\n    </ion-buttons>\n  </ion-navbar>\n</ion-header>\n\n<ion-content padding>\n\n  <ion-slides pager="true">\n    <ion-slide>\n      <p>You\'re good to go.</p>\n      <h2>Hi!</h2>\n      <h2>Welcome to Giftin!</h2>\n      <p>Here’s your best chance at winning giveaways.</p>\n\n    </ion-slide>\n\n    <ion-slide>\n      <p>Find the\n        <span class="h4-text">Biggest giveaways</span> in town from your\n        <span class="h4-text">Favorite Brands</span> and stand a chance to\n        <span class="h4-text">win big!</span>\n      </p>\n      <button ion-button block outline  color="lighter" (click)="goToNextSlide()">Start Tour</button>\n      <button ion-button block outline  color="lighter" (click)="hasSeenTutorial()">Skip</button>\n    </ion-slide>\n\n    <ion-slide>\n        <h2>Enter a giveaway</h2>\n        <p>Swipe away to see the current giveaways. Tap on the Giftin icon to get lucky.</p>\n    </ion-slide>\n\n    <!-- <ion-slide>\n        <h2>Enter Number</h2>\n        <p>Hold on to your entry number and wait for your winning notification.</p>\n    </ion-slide> -->\n\n    <ion-slide>\n      <h2>Giftin Basket</h2>\n      <p>You can keep track of\n        the giveaways you\n        have entered in your\n        Giftin basket.</p>\n  </ion-slide>\n\n  <ion-slide>\n    <h2>The big Win!</h2>\n    <p>Clock is ticking. Gifts\n      are cooking. Be all\n      ears for your winning\n      notification.</p>\n</ion-slide>\n\n    <ion-slide>\n        <h2>Better believe it!</h2>\n        <p>We are literally obsessed with</p>\n        <h2>Transparency!!</h2>\n        <p>Neither we nor our partners have any involvement in determining the winners.</p>\n    </ion-slide>\n\n    <ion-slide>\n      <p>\n          All winners are selected based on legitimate national lottery numbers.\n      </p>\n      <p>\n          You can check the formula on our website – www.giftinapp.com and try it for yourself!\n      </p>\n    </ion-slide>\n\n\n    <!-- <ion-slide *ngFor="let slide of slides">\n\n      <img src="http://www.giftinapp.com/image/giftin-animated_500x.gif" class="slide-image"/>\n      <h2 class="slide-title" [innerHTML]="slide.title"></h2>\n      <p [innerHTML]="slide.description"></p>\n    </ion-slide> -->\n    <ion-slide>\n\n      <!-- <img src="http://www.giftinapp.com/image/giftin-animated_500x.gif" class="slide-image" /> -->\n      <h2 class="slide-title">Ready to Win big?</h2>\n      <button ion-button block outline large  icon-end color="lighter" (click)="hasSeenTutorial()">\n        Continue\n        <ion-icon name="arrow-forward"></ion-icon>\n      </button>\n    </ion-slide>\n  </ion-slides>\n</ion-content>'/*ion-inline-end:"/Users/localuser/gift-prod/src/pages/tutorial/tutorial.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__ionic_storage__["b" /* Storage */]])
    ], TutorialPage);
    return TutorialPage;
}());

//# sourceMappingURL=tutorial.js.map

/***/ }),

/***/ 98:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return EntryPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_auth_auth__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_storage__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__comp_view_comp_view__ = __webpack_require__(50);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





/**
 * Generated class for the EntryPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var EntryPage = (function () {
    function EntryPage(loadingCtrl, navCtrl, navParams, auth, storage) {
        this.loadingCtrl = loadingCtrl;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.auth = auth;
        this.storage = storage;
        this.entry_list = [];
        this.has_entries = false;
        this.presentLoading();
        this.getEntries(0);
    }
    EntryPage.prototype.presentLoading = function () {
        this.loader = this.loadingCtrl.create({
            content: "Please wait...",
            duration: 3000
        });
        this.loader.present();
    };
    EntryPage.prototype.getEntries = function (offset) {
        var _this = this;
        this.storage.get('user_id').then(function (res) {
            var url = "user/my_entries?user_code=" + res + "&offset=" + offset;
            _this.auth.getData(url).then(function (result) {
                if (result.auth) {
                    for (var i = 0; i <= result.result.length - 1; i++) {
                        _this.entry_list.push(result.result[i]);
                    }
                    // console.log(result.result);
                }
                else {
                }
                if (_this.entry_list.length <= 0) {
                    _this.has_entries = false;
                }
                else {
                    _this.has_entries = true;
                }
            });
        }).then(function () {
            _this.loader.dismiss();
        });
    };
    EntryPage.prototype.doInfinite = function (infiniteScroll) {
        console.log('Begin async operation');
        this.getEntries(this.entry_list.length);
        setTimeout(function () {
            console.log('ENd async operation');
            infiniteScroll.complete();
        }, 500);
    };
    EntryPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad EntryPage');
    };
    EntryPage.prototype.viewInfo = function (giveaway_code) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__comp_view_comp_view__["a" /* CompViewPage */], { "giveaway_code": giveaway_code });
    };
    EntryPage.prototype.doRefresh = function (refresher) {
        var _this = this;
        setTimeout(function () {
            _this.getEntries(6);
            console.log('Async operation has ended');
            refresher.complete();
        }, 2000);
    };
    EntryPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-entry',template:/*ion-inline-start:"/Users/localuser/gift-prod/src/pages/entry/entry.html"*/'<!--\n  Generated template for the EntryPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n      <button ion-button menuToggle>\n          <ion-icon name="menu"></ion-icon>\n        </button>\n    <ion-title>My Basket</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content>\n  <p *ngIf="has_entries == false" text-center>\n    No entries yet\n  </p>\n    <ion-refresher (ionRefresh)="doRefresh($event)" >\n        <ion-refresher-content></ion-refresher-content>\n      </ion-refresher>\n      \n<div class="grid-full">\n  <ion-row>\n    <ion-col col-6 *ngFor="let item of entry_list">\n        <ion-card (click)="viewInfo(item.giveaway_code)" class="animated fadeIn">\n            \n          <ion-card-header class="text-giftin">\n            {{ item.giveaway_title }}\n            <p><small>by {{ item.com_name }}</small></p>\n          </ion-card-header>\n          <div class="card-image cover-bg flex-div" [ngStyle]="{\'background-image\': \'url(\'+ item.giveaway_media_link +\')\'}">\n              <ion-spinner name="crescent" class="spinner-center"></ion-spinner>\n          </div>\n            <!-- <img class="card-image" src="{{ item.giveaway_media_link }}"/> -->\n            <ion-card-content>\n                <p class="dflex-ai-center">\n                  <span style="width:50%;">\n                      <ion-icon name="heart" class="text-giftin"></ion-icon>\n                      <span class="ml-5 text-giftin"> {{item.like_count}} </span>\n                  </span>\n                  <span  style="width:50%;">\n                    \n                    <!-- <ion-icon name="heart-outline" class=""></ion-icon> -->\n                    <span class="ml-5">  </span>\n                  </span>\n                     \n                </p>\n            </ion-card-content>\n          </ion-card>\n    </ion-col>\n\n    <ion-infinite-scroll (ionInfinite)="doInfinite($event)">\n      <ion-infinite-scroll-content loadingText="Loading more data..."></ion-infinite-scroll-content>\n    </ion-infinite-scroll>\n\n    \n  </ion-row>\n</div>\n</ion-content>\n'/*ion-inline-end:"/Users/localuser/gift-prod/src/pages/entry/entry.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__providers_auth_auth__["a" /* AuthProvider */], __WEBPACK_IMPORTED_MODULE_3__ionic_storage__["b" /* Storage */]])
    ], EntryPage);
    return EntryPage;
}());

//# sourceMappingURL=entry.js.map

/***/ }),

/***/ 99:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SignupPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__login_login__ = __webpack_require__(43);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_forms__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_auth_auth__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_firebase__ = __webpack_require__(71);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_firebase___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_firebase__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_native_facebook__ = __webpack_require__(154);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ionic_native_firebase__ = __webpack_require__(70);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__otp_otp__ = __webpack_require__(181);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









/**
 * Generated class for the SignupPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var SignupPage = (function () {
    function SignupPage(navCtrl, navParams, formBuilder, loadCtrl, authProvider, facebook, fire, alertCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.formBuilder = formBuilder;
        this.loadCtrl = loadCtrl;
        this.authProvider = authProvider;
        this.facebook = facebook;
        this.fire = fire;
        this.alertCtrl = alertCtrl;
        this.submitAttempt = false;
        //show - hide password variable
        this.show_pass = false;
        //verification id for firebase phone code
        this.verificationId = '';
        //country list
        this.country_list = [];
        this.country_phone_code = "+";
        this.getcountries();
        //set signup validation rules
        this.user = this.formBuilder.group({
            email: ['', __WEBPACK_IMPORTED_MODULE_3__angular_forms__["f" /* Validators */].compose([__WEBPACK_IMPORTED_MODULE_3__angular_forms__["f" /* Validators */].required, __WEBPACK_IMPORTED_MODULE_3__angular_forms__["f" /* Validators */].email])],
            mobile_no: ['', __WEBPACK_IMPORTED_MODULE_3__angular_forms__["f" /* Validators */].compose([__WEBPACK_IMPORTED_MODULE_3__angular_forms__["f" /* Validators */].required, __WEBPACK_IMPORTED_MODULE_3__angular_forms__["f" /* Validators */].minLength(8),])],
            password: ['', __WEBPACK_IMPORTED_MODULE_3__angular_forms__["f" /* Validators */].required],
            country: [" ", __WEBPACK_IMPORTED_MODULE_3__angular_forms__["f" /* Validators */].required],
            phone_code: [" ", __WEBPACK_IMPORTED_MODULE_3__angular_forms__["f" /* Validators */].compose([__WEBPACK_IMPORTED_MODULE_3__angular_forms__["f" /* Validators */].required, __WEBPACK_IMPORTED_MODULE_3__angular_forms__["f" /* Validators */].pattern('[+][0-9]{1,5}')])],
        });
        //initialiaze loader
        this.loader = this.loadCtrl.create({
            spinner: 'crescent',
            content: "Sending OTP to your phone.",
        });
    }
    SignupPage.prototype.getcountries = function () {
        var _this = this;
        var url = "settings/countries";
        this.authProvider.getData(url).then(function (country_list) {
            country_list.forEach(function (element) {
                _this.country_list.push(element);
                // console.log(element);
            });
        });
    };
    SignupPage.prototype.getCountryPhone = function (country) {
        console.log("selected", country);
        // this.user.phone_code = "+" + country.phonecode;
        this.country_phone_code = "+" + country.phonecode;
    };
    SignupPage.prototype.signIn = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__login_login__["a" /* LoginPage */]);
    };
    //show hide password
    SignupPage.prototype.showPassword = function () {
        this.show_pass = !this.show_pass;
    };
    //handle phone number 
    SignupPage.prototype.createPhoneNumber = function (data) {
        var _this = this;
        this.submitAttempt = true;
        var number = data.mobile_no;
        var code = this.country_phone_code;
        var phone_number = code.substr(1) + number;
        console.log(code.substr(1), number);
        var verifyAlert = this.alertCtrl.create({
            title: "Verify Phone Number",
            subTitle: "We will be verifying the phone number : " + code + number + ". Is this OK, or would you like to edit the number?",
            enableBackdropDismiss: false,
            buttons: [{
                    text: 'EDIT',
                    role: 'cancel',
                    handler: function () {
                        console.log('Cancel clicked');
                    }
                },
                {
                    text: 'OK',
                    handler: function () {
                        _this.signup_process(data, phone_number);
                    }
                }]
        });
        verifyAlert.present();
    };
    //signup process
    SignupPage.prototype.signup_process = function (data, phoneNumber) {
        this.showLoader();
        // this.sendVerifyCode(phoneNumber);
        this.save_to_db(data, phoneNumber);
        // alert(sms_sent);
    };
    SignupPage.prototype.save_to_db = function (data, phoneNumber) {
        var _this = this;
        console.log(data);
        var url = "user/users";
        this.authProvider.postAuthData(data, url).then(function (result) {
            _this.loaderDismiss();
            console.log(result);
            if (result.auth) {
                _this.sendVerifyCode(phoneNumber);
            }
            else {
                _this.showAlert("Error", result.msg);
            }
        }).catch(function (err) {
            console.log(err);
        });
    };
    SignupPage.prototype.signup = function (data) {
        this.createPhoneNumber(data);
    };
    //signup function 
    SignupPage.prototype.signup2 = function (data) {
        var _this = this;
        this.createPhoneNumber(data);
        // return;
        this.showLoader();
        this.submitAttempt = true;
        var url = "user/users";
        this.authProvider.postAuthData(data, url).then(function (result) {
            _this.loaderDismiss();
            console.log(result);
            if (result.auth) {
                _this.sendVerifyCode(data.mobile_no);
            }
            else {
                _this.showAlert("Error", result.msg);
            }
        }).catch(function (err) {
            console.log(err);
        });
    };
    //send verification code 
    SignupPage.prototype.sendVerifyCode = function (phoneNumber) {
        var _this = this;
        this.fire.verifyPhoneNumber("+" + phoneNumber, 60).then(function (credential) {
            _this.verificationId = credential.verificationId;
            _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_8__otp_otp__["a" /* OtpPage */], { verificationid: _this.verificationId, phoneNumber: phoneNumber });
        }).catch(function (err) {
            var alert = _this.alertCtrl.create({
                title: "Error",
                subTitle: JSON.stringify(err),
                enableBackdropDismiss: true,
                buttons: ['OK']
            });
            alert.present();
            //  alert(JSON.stringify(err));
        });
    };
    //facebook native login
    SignupPage.prototype.signupFacebook = function () {
        var _this = this;
        this.showLoader();
        this.facebook.login(["email", "public_profile", "user_friends"]).then(function (loginResponse) {
            _this.facebook.api("/me?fields=email,name,picture,first_name,last_name,id,age_range,link,locale,gender", ["public_profile", "email", "user_friends"]).then(function (result) {
                _this.facebook_user = {
                    first_name: result.first_name,
                    last_name: result.last_name,
                    name: result.name,
                    email: result.email,
                    photo: result.picture.data.url,
                    gender: result.gender,
                    age: result.min,
                    social_id: result.id,
                    locale: result.locale,
                    link: result.link,
                };
            });
            var credentials = __WEBPACK_IMPORTED_MODULE_5_firebase___default.a.auth.FacebookAuthProvider.credential(loginResponse.authResponse.accessToken);
            __WEBPACK_IMPORTED_MODULE_5_firebase___default.a.auth().signInWithCredential(credentials).then(function (res) {
                var data = _this.facebook_user;
                var url = "social_user/social_users";
                _this.authProvider.postAuthData(data, url).then(function (result_auth) {
                    if (result_auth.auth) {
                        _this.showAlert("Success", result_auth.msg);
                    }
                    else {
                        _this.showAlert("Error", result_auth.msg);
                    }
                }).catch(function (err) {
                });
            });
        }).catch(function (err) {
            // this.loading.dismiss();
        });
    };
    //facebook web functionality
    SignupPage.prototype.signupFacebookWeb = function () {
        var _this = this;
        this.showLoader();
        var provider = new __WEBPACK_IMPORTED_MODULE_5_firebase___default.a.auth.FacebookAuthProvider();
        __WEBPACK_IMPORTED_MODULE_5_firebase___default.a.auth().signInWithPopup(provider).then(function (res) {
            _this.facebook_user = {
                first_name: res.additionalUserInfo.profile.first_name,
                last_name: res.additionalUserInfo.profile.last_name,
                name: res.additionalUserInfo.profile.name,
                email: res.additionalUserInfo.profile.email,
                phoneNumber: res.user.providerData.phoneNumber,
                photo: res.additionalUserInfo.profile.picture.data.url,
                gender: res.additionalUserInfo.profile.gender,
                age: res.additionalUserInfo.profile.age_range.min,
                provider: res.additionalUserInfo.providerId,
                social_id: res.additionalUserInfo.profile.id,
                locale: res.additionalUserInfo.profile.locale,
                link: res.additionalUserInfo.profile.link,
            };
            var data = _this.facebook_user;
            var url = "social_user/social_users";
            _this.authProvider.postAuthData(data, url).then(function (result_auth) {
                if (result_auth.auth) {
                    _this.showAlert("Success", result_auth.msg);
                }
                else {
                    _this.showAlert("Error", result_auth.msg);
                }
            }).catch(function (err) {
                console.log(err);
            });
        })
            .catch(function (err) {
            console.log(err);
            console.log("error", JSON.stringify(err));
        });
    };
    //show alert 
    SignupPage.prototype.showAlert = function (title, msg) {
        var alert = this.alertCtrl.create({
            title: title,
            subTitle: msg,
            enableBackdropDismiss: false,
            buttons: ['Continue']
        });
        alert.present();
    };
    //show the loader
    SignupPage.prototype.showLoader = function () {
        this.loader.present();
    };
    //loader dissmiss 
    SignupPage.prototype.loaderDismiss = function () {
        this.loader.dismiss();
    };
    SignupPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-signup',template:/*ion-inline-start:"/Users/localuser/gift-prod/src/pages/signup/signup.html"*/'<!--\n  Generated template for the SignupPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-content padding>\n  <ion-grid>\n    <div>\n      <div text-center class="text-white">\n        <h4>Sign Up</h4>\n        <p>Please Sign Up to continue</p>\n      </div>\n      <ion-row>\n        <ion-col>\n          <ion-list>\n            <form [formGroup]="user" (ngSubmit)="signup(user.value)">\n              <div class="item-container">\n\n                <!-- <div class="has-error" *ngIf="!user.controls.email.valid  && (user.controls.email.dirty || submitAttempt)"  >This field is required and should be valid email</div> -->\n                <ion-item class="px-10">\n\n                  <ion-input type="email" placeholder="Email" clearInput [(ngModel)]="user.email" name="email" formControlName="email"></ion-input>\n                </ion-item>\n\n                <ion-item no-lines class="error-msg" text-wrap *ngIf="!user.controls.email.valid  && (user.controls.email.dirty || submitAttempt)">\n                  <small>This field is required and should be valid email</small>\n                </ion-item>\n\n\n                <ion-item class="px-10">\n                  <ion-select clearInput placeholder="Select your country" required formControlName="country" [(ngModel)]="user.country">\n                    <ion-option *ngFor="let cou of country_list" (ionSelect)="getCountryPhone(cou)" value="{{cou.name}}">{{cou.name}}</ion-option>\n                  </ion-select>\n                </ion-item>\n                <ion-item no-lines class="error-msg" text-wrap *ngIf="!user.controls.country.valid  && (user.controls.country.dirty || submitAttempt)">\n                  <small>Country is required </small>\n                </ion-item>\n                <ion-row style="background:white !important;">\n                  <ion-col col-4 no-padding>\n                    <ion-item no-lines class="px-10 pb-0">\n                      <ion-input type="text" placeholder="+" readonly formControlName="phone_code"  value="{{country_phone_code}}"></ion-input>\n                    </ion-item>\n                  </ion-col>\n                  <ion-col  no-padding>\n                    <ion-item no-lines class="px-10 pb-0">\n                      <ion-input required type="number" placeholder="Contact Number" [(ngModel)]="user.mobile_no" name="mobile_no" formControlName="mobile_no"\n                        style="padding-right: 0px !important;"></ion-input>\n                    </ion-item>\n                  </ion-col>\n                </ion-row>\n\n                <!-- <ion-item class="px-10">\n                  <ion-input required type="number" placeholder="Contact Number (94712345678)" clearInput [(ngModel)]="user.mobile_no" name="mobile_no"\n                    formControlName="mobile_no"></ion-input>\n                </ion-item> -->\n                <ion-item no-lines class="error-msg" text-wrap *ngIf="!user.controls.mobile_no.valid  && (user.controls.mobile_no.dirty || submitAttempt)">\n                  <small>This field is required and should be valid mobile number Ex : 712345678</small>\n                </ion-item>\n                <ion-item class="px-10">\n                  <ion-input type="password" *ngIf="!show_pass" placeholder="Password" clearInput [(ngModel)]="user.password" name="password"\n                    formControlName="password"></ion-input>\n                  <ion-input type="text" *ngIf="show_pass" placeholder="Password" clearInput [(ngModel)]="user.password" name="password" formControlName="password"></ion-input>\n                  <button ion-button clear color="dark" type="button" item-right (click)="showPassword()">\n                    <ion-icon [name]="show_pass ? \'eye-off\' : \'eye\'"> </ion-icon>\n                  </button>\n                </ion-item>\n                <ion-item no-lines class="error-msg" text-wrap *ngIf="!user.controls.password.valid  && (user.controls.password.dirty || submitAttempt)">\n                  <small>This field is required and should be valid password</small>\n                </ion-item>\n\n              </div>\n              <ion-row>\n                <ion-col class="px-0">\n                  <button type="submit" [disabled]="!user.valid" ion-button class="btn-gift" color="light" block>Sign Up</button>\n                  <!-- [disabled]="!user.valid" -->\n                </ion-col>\n              </ion-row>\n            </form>\n          </ion-list>\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col text-center class="px-0">\n          <button ion-button class="social-icon social-facebook" (click)="signupFacebook()">\n            <!-- <ion-icon name="logo-facebook"></ion-icon> -->\n            <img src="assets/imgs/fb_login.png" alt="">\n          </button>\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col text-center class="text-white">\n\n          <p>Already have account?\n            <span (click)="signIn()">\n              <b>Sign in</b>\n            </span>\n          </p>\n\n\n        </ion-col>\n      </ion-row>\n    </div>\n  </ion-grid>\n</ion-content>'/*ion-inline-end:"/Users/localuser/gift-prod/src/pages/signup/signup.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_3__angular_forms__["a" /* FormBuilder */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_4__providers_auth_auth__["a" /* AuthProvider */],
            __WEBPACK_IMPORTED_MODULE_6__ionic_native_facebook__["a" /* Facebook */],
            __WEBPACK_IMPORTED_MODULE_7__ionic_native_firebase__["a" /* Firebase */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */]])
    ], SignupPage);
    return SignupPage;
}());

//# sourceMappingURL=signup.js.map

/***/ })

},[384]);
//# sourceMappingURL=main.js.map