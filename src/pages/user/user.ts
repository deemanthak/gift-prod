import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ActionSheetController, ToastController, Platform, LoadingController, Loading } from 'ionic-angular';

import { SessionProvider } from '../../providers/session/session';

import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { AuthProvider } from '../../providers/auth/auth';
import { ApiProvider } from '../../providers/api/api';
import { MenuPage } from '../menu/menu';

import { File } from '@ionic-native/file';
import { Transfer, TransferObject } from '@ionic-native/transfer';
import { FilePath } from '@ionic-native/file-path';
import { Camera } from '@ionic-native/camera';

import { Storage } from '@ionic/storage';
declare var cordova: any;

/**
 * Generated class for the UserPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-user',
  templateUrl: 'user.html',
})
export class UserPage {
  //profile pic related
  lastImage: string = null;
  loading: Loading;
  isServerImage: boolean = true;


  //user info varialbe
  private user: FormGroup;
  submitAttempt: boolean = false;
  country_list:any =[];

  //for user data
  // public user_data: any = [];
  user_code: any;

  constructor(public navCtrl: NavController, public navParams: NavParams,
    public actionSheetCtrl: ActionSheetController, public session: SessionProvider,
    private formBuilder: FormBuilder, public authProvider: AuthProvider,
    private camera: Camera, private transfer: Transfer, private file: File, private filePath: FilePath,
    public toastCtrl: ToastController, public platform: Platform, public loadingCtrl: LoadingController,
    public api: ApiProvider, public storage: Storage
  ) {
    // this.user_code = "64";
    this.getcountries();
    this.user_code = this.navParams.get('user_code');

    this.user = this.formBuilder.group({
      first_name: ['', Validators.compose([Validators.required,])],
      last_name: ['', Validators.compose([Validators.required,])],
      city: ['', Validators.compose([Validators.required,])],
      // country: ['', Validators.compose([Validators.required,])],
      age: ['', Validators.compose([Validators.required,])],
      user_code: this.user_code

    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad UserPage');
  }

  getcountries() {
    let url = "settings/countries"
    this.authProvider.getData(url).then((country_list: any) => {
      country_list.forEach(element => {
        this.country_list.push(element);
        // console.log(element);
      });
    })
  }

  addUserData(data: any) {
    let url = "user/update_user_info";
    //  this.submitAttempt = true;
    this.authProvider.postAuthData(data, url).then((res: any) => {

      if (res.auth) {
        this.uploadImage();
        this.presentToast("Personal Infomation updated successfully");
        this.storage.set('fist_login', false);
        this.navCtrl.setRoot(MenuPage);
      }
      //  console.log(res);
      // this.resetSession();
    });
  }

  presentActionSheet() {
    let actionSheet = this.actionSheetCtrl.create({
      title: 'Edit Profile Photo',
      buttons: [
        {
          text: 'Pick From Gallery',
          icon: 'images',
          // role: 'destructive',
          handler: () => {
            this.takePicture(this.camera.PictureSourceType.PHOTOLIBRARY);
            console.log('Destructive clicked');
          }
        }, {
          text: 'Take a Picture',
          icon: 'camera',
          handler: () => {
            this.takePicture(this.camera.PictureSourceType.CAMERA);
            console.log('Archive clicked');
          }
        }, {
          text: 'Delete',
          icon: 'trash',
          // role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Cancel',
          role: 'cancel',
          icon: 'close',
          handler: () => {
            console.log('Cancel clicked');
          }
        }
      ]
    });
    actionSheet.present();
  }

  public takePicture(sourceType) {
    // Create options for the Camera Dialog
    var options = {
      quality: 50,
      sourceType: sourceType,
      saveToPhotoAlbum: true,
      correctOrientation: true,
      allowEdit: true,

    };

    // Get the data of an image
    this.camera.getPicture(options).then((imagePath) => {
      // Special handling for Android library
      if (this.platform.is('android') && sourceType === this.camera.PictureSourceType.PHOTOLIBRARY) {
        this.filePath.resolveNativePath(imagePath)
          .then(filePath => {
            let correctPath = filePath.substr(0, filePath.lastIndexOf('/') + 1);
            let currentName = imagePath.substring(imagePath.lastIndexOf('/') + 1, imagePath.lastIndexOf('?'));
            this.copyFileToLocalDir(correctPath, currentName, this.createFileName());
          });
      } else {
        var currentName = imagePath.substr(imagePath.lastIndexOf('/') + 1);
        var correctPath = imagePath.substr(0, imagePath.lastIndexOf('/') + 1);
        this.copyFileToLocalDir(correctPath, currentName, this.createFileName());
      }
    }, (err) => {
      this.presentToast('Error while selecting image.');
    });
  }

  // Create a new name for the image
  private createFileName() {
    var d = new Date(),
      n = d.getTime(),
      newFileName = n + ".jpg";
    return newFileName;
  }

  // Copy the image to a local folder
  private copyFileToLocalDir(namePath, currentName, newFileName) {
    // this.lastImage = null;
    this.file.copyFile(namePath, currentName, cordova.file.dataDirectory, newFileName).then(success => {
      this.lastImage = newFileName;
      // this.lastImage = this.pathForImage(this.lastImage);
      this.isServerImage = false;

    }, error => {
      this.presentToast('Error while storing file.');
    });
  }

  private presentToast(text) {
    let toast = this.toastCtrl.create({
      message: text,
      duration: 3000,
      position: 'top'
    });
    toast.present();
  }

  // Always get the accurate path to your apps folder
  public pathForImage(img) {
    if (img === null) {
      return '';
    } else {
      return cordova.file.dataDirectory + img;
    }
  }

  //upload image 
  public uploadImage() {
    var base_url = this.api.init();
    // Destination URL
    var url = base_url + "file_upload/dp_upload";

    // File for Upload
    var targetPath = this.pathForImage(this.lastImage);
    //  console.log(targetPath);
    // File name only
    var filename = this.lastImage;

    var options = {
      fileKey: "file",
      fileName: filename,
      chunkedMode: false,
      mimeType: "multipart/form-data",
      params: { 'fileName': filename, 'user_code': this.user_code }
    };

    const fileTransfer: TransferObject = this.transfer.create();

    this.loading = this.loadingCtrl.create({
      content: 'Uploading...',
    });
    this.loading.present();

    // Use the FileTransfer to upload the image
    fileTransfer.upload(targetPath, url, options).then(data => {
      // console.log(data);
      this.loading.dismissAll()
      // this.presentToast('Image succesful uploaded.');
    }, err => {
      console.log(err);
      this.loading.dismissAll()
      this.presentToast('Error while uploading file.');
    });
  }


}
