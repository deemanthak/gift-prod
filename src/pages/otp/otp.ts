import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,AlertController } from 'ionic-angular';
import firebase from 'firebase';
import { Firebase } from '@ionic-native/firebase';
import { LoginPage } from '../login/login';

import { Storage } from '@ionic/storage';
/**
 * Generated class for the OtpPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

 @IonicPage()
 @Component({
 	selector: 'page-otp',
 	templateUrl: 'otp.html',
 })
 export class OtpPage {

 	verification_id: any;
 	phoneNumber:any;
 	otp:string='';


 	constructor(public navCtrl: NavController, public navParams: NavParams,private alertCtrl:AlertController,
 		private fire:Firebase,public storage:Storage) {
			 //assign verification id and the phone number 
			this.verification_id = this.navParams.get('verificationid');
			this.phoneNumber = this.navParams.get('phoneNumber');
 	}

//send verification code through the firebase and verify
roleSelection() {
	var signInCredential = firebase.auth.PhoneAuthProvider.credential(this.verification_id,this.otp);
	firebase.auth().signInWithCredential(signInCredential).then((result:any)=>{    
		console.log("otp data",JSON.stringify(result));
		if(JSON.stringify(result).length > 0 )
		{
			this.storage.set('fist_login',true);
			let alert = this.alertCtrl.create({
				title: "Success",
				subTitle: "Verified successfully",
				enableBackdropDismiss:false,
				buttons: [{
					text: 'Continue',
					handler: () => {
						this.navCtrl.push(LoginPage);
					}
				}]
			});
			alert.present();
		}
	}).catch(()=>{
		let alert = this.alertCtrl.create({
			title: "Error",
			subTitle: "Invalid OTP number please check again",
			enableBackdropDismiss:false,
			buttons: [{
				text: 'OK',
				
			}]
		});
		alert.present();
	});
}


resendCode(){
	this.fire.verifyPhoneNumber("+"+this.phoneNumber,60).then((credential)=>{
		this.verification_id = credential.verificationId;
   }).catch((err)=>{
   	alert(JSON.stringify(err));
   })

}

}
