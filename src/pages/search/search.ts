import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AuthProvider } from '../../providers/auth/auth';
import { ApiProvider } from '../../providers/api/api';
import { Http, Headers } from "@angular/http";

import { CompViewPage } from '../comp-view/comp-view';

/**
 * Generated class for the SearchPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-search',
  templateUrl: 'search.html',
})
export class SearchPage {
  giveawaysInitial: any = []; //initialize your countriesInitial array empty
  giveaways: any = []; //initialize your countries array empty
  searchString = ''; // initialize your searchCountryString string empty
  mainview:boolean = true;

  constructor(public navCtrl: NavController, public navParams: NavParams, public auth: AuthProvider, public http: Http, public api: ApiProvider) {
    let url = "giveaway/giveaway_names";

    if(this.navParams.get('mainview') == undefined){
      this.mainview = true;
    }else{
      this.mainview = this.navParams.get('mainview');

    }

    console.log("mainview",this.mainview);
    this.auth.getData(url).then((data: any) => {
      this.giveawaysInitial = data;
      this.giveaways = data;
    })
  }

  searchGiveaway(searchbar) {
    // reset countries list with initial call
    this.giveaways = this.giveawaysInitial;
    // set q to the value of the searchbar
    var q = searchbar.target.value;

    // if the value is an empty string don't filter the items
    if (q.trim() == '') {
      return;
    }

    this.giveaways = this.giveaways.filter((v) => {
      if (v.giveaway_title.toLowerCase().indexOf(q.toLowerCase()) > -1) {
        return true;
      }
      if (v.giveaway_code.toLowerCase().indexOf(q.toLowerCase()) > -1) {
        return true;
      }
      return false;
    })
  }

  viewCompView(id: any) {
    this.navCtrl.push(CompViewPage, { 'giveaway_code': id });
  }

}
