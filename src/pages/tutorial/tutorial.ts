import { Component,ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams,Slides } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { MenuPage } from '../menu/menu';
/**
 * Generated class for the TutorialPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-tutorial',
  templateUrl: 'tutorial.html',
})
export class TutorialPage {
  @ViewChild(Slides) slideCtrl: Slides;

  public slides = [
    {
      title: "Hi! \n Welcome to Gift In!",
      description: "Here’s your best chance at winning giveaways.",
      image: "",
    },
    {
      title: "What is Gift In?",
      description: "<b>Gift In</b> is a free mobile app where you can stand a chance to win free giveaways from your favorite brands",
      image: "assets/img/ica-slidebox-img-2.png",
    },
    {
      title: "How Gift In Works?",
      description: "The <b>Gift In</b> is a cloud platform for managing and scaling Ionic apps with integrated services like push notifications, native builds, user auth, and live updating.",
      image: "assets/img/ica-slidebox-img-3.png",
    }
  ];
  constructor(public navCtrl: NavController, public navParams: NavParams, 
    public storage: Storage) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TutorialPage');
  }

  hasSeenTutorial() {
    this.storage.set('hasSeenTutorial', true);
    this.navCtrl.setRoot(MenuPage);
  }

  goToNextSlide(){
    this.slideCtrl.slideNext();
  }

}
