import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { AuthProvider } from '../../providers/auth/auth';
import { SessionProvider } from '../../providers/session/session';
/**
 * Generated class for the CompViewPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-comp-view',
  templateUrl: 'comp-view.html',
})
export class CompViewPage {

  show_draw_detail: boolean = false;
  show_winners: boolean = false;

  user_id:any;
  data_giveaway: any;
  data_media: any;
  data_link: any;
  company: any;
  like_count:any;
  winners_list:any;
  draw_data:any;

  giveaway_code: any;

  entry_index:any=0;

  constructor(public navCtrl: NavController, public navParams: NavParams, 
    public auth: AuthProvider,public session:SessionProvider) {
    this.data_giveaway = [];
    this.data_media = [];
    this.data_link = [];
    this.company = [];
    this.winners_list = [];
    this.draw_data = [];
    
    this.session.getUser();
    this.user_id = this.session.user_data.user_code;

    this.giveaway_code = this.navParams.get('giveaway_code');
    this.getGiveawayData(this.giveaway_code);
  }

  getGiveawayData(giveaway_code: any) {
    let url = "giveaway/giveaway?id=" + giveaway_code + "&user_id=" + this.user_id;
    this.auth.getData(url).then((res: any) => {
      this.data_giveaway = res.data_giveaway[0];
      this.data_media = res.data_media[0];
      this.data_link = res.data_link;
      this.company = res.company[0];
      this.like_count = res.like_count[0].like_count;
     
      if(res.entry_no.length > 0 ){
        this.entry_index = res.entry_no[0].entry_no;

      }

      if(res.winners.length > 0){
        console.log("winner length",res.winners.length);
        this.show_winners = true;
        this.winners_list = res.winners;
      }

      if(res.draw_data.length > 0){
        this.show_draw_detail = true;
        this.draw_data = res.draw_data;
      }
    });
  }

  openUrl(link: any) { 
    window.open(link,'_system');
  }


  ionViewDidLoad() {
    console.log('ionViewDidLoad CompViewPage');
  }

}
