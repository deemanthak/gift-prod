import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CompViewPage } from './comp-view';

@NgModule({
  declarations: [
    CompViewPage,
  ],
  imports: [
    IonicPageModule.forChild(CompViewPage),
  ],
})
export class CompViewPageModule {}
