import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,LoadingController } from 'ionic-angular';
import {AuthProvider} from '../../providers/auth/auth';
import { Storage } from '@ionic/storage';

import {CompViewPage} from '../comp-view/comp-view';

/**
 * Generated class for the EntryPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-entry',
  templateUrl: 'entry.html',
})
export class EntryPage {
  entry_list:any = [];
  has_entries:boolean = false;

loader:any;

  constructor(public loadingCtrl:LoadingController,public navCtrl: NavController, public navParams: NavParams,public auth:AuthProvider,public storage:Storage) {
    this.presentLoading();
    this.getEntries(0);
  }

  presentLoading() {
    this.loader = this.loadingCtrl.create({
      content: "Please wait...",
      duration: 3000
    });
    this.loader.present();
  }

  getEntries(offset:number){
    this.storage.get('user_id').then((res)=>{
      
      let url = "user/my_entries?user_code=" + res + "&offset=" + offset;
      this.auth.getData(url).then((result:any)=>{
        if(result.auth){
          
          for(var i=0;i<=result.result.length-1;i++){
            this.entry_list.push(result.result[i]);
          }
          
          // console.log(result.result);
        }else{
          
        }
        if(this.entry_list.length <= 0){
          this.has_entries = false;
        }else{
          this.has_entries = true;
        }

      });
    }).then(()=>{
      this.loader.dismiss();
    })
  }

  doInfinite(infiniteScroll) {
    console.log('Begin async operation');
    this.getEntries(this.entry_list.length);
    setTimeout(() => {
      
      console.log('ENd async operation');
      infiniteScroll.complete();
    }, 500);
  }


  ionViewDidLoad() {
    console.log('ionViewDidLoad EntryPage');
  }

  viewInfo(giveaway_code:any){
    this.navCtrl.push(CompViewPage,{"giveaway_code":giveaway_code});
  }

  doRefresh(refresher) {
   

    setTimeout(() => {
      this.getEntries(6);
      console.log('Async operation has ended');
      refresher.complete();
    }, 2000);
  }

}
