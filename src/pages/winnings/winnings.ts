import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,LoadingController } from 'ionic-angular';
import { AuthProvider } from '../../providers/auth/auth';
import { Storage } from '@ionic/storage';
import {CompViewPage} from '../comp-view/comp-view';
/**
 * Generated class for the WinningsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-winnings',
  templateUrl: 'winnings.html',
})
export class WinningsPage {

  giveaway_list:any=[];
  has_winnings:boolean = false;
  loader:any;


  constructor(public loadingCtrl:LoadingController,public navCtrl: NavController, public navParams: NavParams,public auth:AuthProvider,public storage:Storage) {
  this.presentLoading();
    this.getWonList();

  }

  presentLoading() {
    this.loader = this.loadingCtrl.create({
      content: "Please wait...",
      duration: 3000
    });
    this.loader.present();
  }
  
  getWonList(){
    this.storage.get('user_id').then((user_id)=>{
      let url = "giveaway/get_won_list?user_code=" + user_id;
      this.auth.getData(url).then((list:any)=>{
        if(list.auth){
          this.has_winnings =  true;
          this.giveaway_list = list.result;
        }else{
          this.has_winnings = false;
        }
       
      })
    }).then(()=>{
      this.loader.dismiss();
    })
    
  }

  viewInfo(giveaway_code:any){
    this.navCtrl.push(CompViewPage,{"giveaway_code":giveaway_code});
  }

  doRefresh(refresher) {
    this.getWonList();

    setTimeout(() => {
      console.log('Async operation has ended');
      refresher.complete();
    }, 2000);
  }


}
