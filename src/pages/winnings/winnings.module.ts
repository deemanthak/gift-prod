import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { WinningsPage } from './winnings';

@NgModule({
  declarations: [
    WinningsPage,
  ],
  imports: [
    IonicPageModule.forChild(WinningsPage),
  ],
})
export class WinningsPageModule {}
