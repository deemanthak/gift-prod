import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Events } from 'ionic-angular';
import { AuthProvider } from '../../providers/auth/auth';
// import { Storage } from '@ionic/storage';
import { SessionProvider } from '../../providers/session/session';
import { Http } from '@angular/http';
import 'rxjs/Rx';

import { HomePage } from '../home/home';
import { CompanyPage } from '../company/company';
import { ImgLoader } from 'ionic-image-loader';
import { SocialSharing } from '@ionic-native/social-sharing';


/**
 * Generated class for the FullViewPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-full-view',
  templateUrl: 'full-view.html',
})
export class FullViewPage {

  card_id: any;
  public user_id: any = "";

  data_giveaway: any;
  data_media: any;
  data_link: any;
  company: any;
  showFabs: boolean = true;

  index: any;

  //like count and entry no
  like_count: any = 0;
  entry_index: any = 0;

  constructor(public navCtrl: NavController, public navParams: NavParams,
    public authProvider: AuthProvider, public session: SessionProvider,
    private social:SocialSharing,
    private http: Http, public events: Events) {
    this.data_giveaway = [];
    this.data_media = [];
    this.data_link = [];
    this.company = [];

    this.session.getUser();
    this.user_id = this.session.user_data.user_code;
    this.card_id = this.navParams.get('giveaway_id');
    this.index = this.navParams.get('index');

    // this.getInfo("43");
    this.getInfo(this.card_id);
  }



  onImageLoad(imgLoader: ImgLoader) {
    // do something with the loader
    console.log("do something with the loader");
  }

  getInfo(giveaway: string) {
    let url = "giveaway/giveaway?id=" + giveaway + "&user_id=" + this.user_id;
    this.authProvider.getData(url).then((res: any) => {
      this.data_giveaway = res.data_giveaway[0];
      this.data_media = res.data_media[0];
      this.data_link = res.data_link;
      this.company = res.company[0];

      if (res.like_count.length > 0) {
        this.like_count = res.like_count[0].like_count;
      }
      // this.like_count = res.like_count;
      // this.entry_index = res.entry_no;

    })
    console.log(this.data_giveaway.giveaway_title);
  }

  openUrl(link: any) {
    window.open(link, '_system');

  }

  trythis() {
    this.events.publish('giveaway:added', this.index);
    this.navCtrl.pop();
  }

  goToCompanyPage(company:any){
    this.navCtrl.push(CompanyPage,{"company_id":company});
  }

  socialShare(){
    this.social.canShareVia('facebook')
  }

  shareSheetShare() {
    // alert('ssss');
    // this.social.share("Share message", "Share subject", "URL to file or image", "A URL to share").then(() => {
    //   console.log("shareSheetShare: Success");
    // }).catch(() => {
    //   console.error("shareSheetShare: failed");
    // });

    this.social.shareWithOptions({
      message:"this is test",
      subject:"test subject",
      files:"http://via.placeholder.com/350x150",
      url:"http://giftinapp.com",
      chooserTitle:"facebook"
    }).then(()=>{

    })
  }

  facebookShare(){
    alert('facebook share');
   this.social.shareViaFacebook("message", "http://via.placeholder.com/350x150", "http://giftinapp.com").then(()=>{
     
   })
  }

}
