import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FullViewPage } from './full-view';
import { IonicImageLoader } from 'ionic-image-loader';

@NgModule({
  declarations: [
    FullViewPage,
  ],
  imports: [
    IonicPageModule.forChild(FullViewPage),
    IonicImageLoader
  ],
})
export class FullViewPageModule {}
