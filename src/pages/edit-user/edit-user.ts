import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ActionSheetController, ToastController, Platform, LoadingController, Loading,AlertController } from 'ionic-angular';
import { SessionProvider } from '../../providers/session/session';

import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { AuthProvider } from '../../providers/auth/auth';
import { ApiProvider } from '../../providers/api/api';

import { File } from '@ionic-native/file';
import { Transfer, TransferObject } from '@ionic-native/transfer';
import { FilePath } from '@ionic-native/file-path';
import { Camera } from '@ionic-native/camera';

import { Storage } from '@ionic/storage';
declare var cordova: any;


/**
 * Generated class for the EditUserPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-edit-user',
  templateUrl: 'edit-user.html',
})
export class EditUserPage {

  //profile pic related
  lastImage: string = null;
  loading: Loading;
  isServerImage: boolean = true;

  //user info varialbe
  private user: FormGroup;
  submitAttempt: boolean = false;

  //user password 
  private pwdForm: FormGroup;
  submitPwdAttempt: boolean = false;

  //exsisting user data
  public user_data: any = [];

  constructor(public navCtrl: NavController, public navParams: NavParams,
    public actionSheetCtrl: ActionSheetController, public session: SessionProvider,
    private formBuilder: FormBuilder, public authProvider: AuthProvider,
    private camera: Camera, private transfer: Transfer, private file: File, private filePath: FilePath,
    public toastCtrl: ToastController, public platform: Platform, public loadingCtrl: LoadingController,
    public api: ApiProvider, public storage: Storage,public alertCtrl:AlertController

  ) {

    // this.session.getUser().then((res:any) => {
    this.user_data = this.session.user_data;
    // });
    this.lastImage = this.user_data.photo;

    // this.storage.get('user_photo').then((res:any)=>{
    //   this.lastImage = res;
    // });

    console.log("this email", this.user_data.photo);
    // this.user.controls['email'].setValue(this.user_data.email);
    this.user = this.formBuilder.group({
      email: [Validators.compose([Validators.required, Validators.email])],
      first_name: [Validators.compose([Validators.required,])],
      last_name: [Validators.compose([Validators.required,])],
      mobile_no: [Validators.compose([Validators.required,])],
      city: [Validators.compose([Validators.required,])],
      country: [Validators.compose([Validators.required,])],
      age: [Validators.compose([Validators.required,])],
      user_code: this.user_data.user_code

    });

    this.pwdForm = this.formBuilder.group({
      user_code: this.user_data.user_code,
      oldPassword: ['', Validators.compose([Validators.required])],
      newPassword: ['', Validators.compose([Validators.required])],
      confPassword: ['', Validators.compose([Validators.required])],

    }, { validator: this.passwordMismatch })
    // this.lastImage = this.user_data.photo;
  }

  passwordMismatch(cg: FormGroup): { [err: string]: any } {
    console.log("password checking")
    let pwd1 = cg.get('newPassword');
    let pwd2 = cg.get('confPassword');
    let rv: { [error: string]: any } = {};
    if ((pwd1.touched || pwd2.touched) && pwd1.value !== pwd2.value) {
      rv['passwordMismatch'] = true;

    }
    // console.log(rv);
    return rv;
  }

  checkOldPassword(data: any) {
    let url = "user/check_password";
    this.submitPwdAttempt = true;

    this.authProvider.postAuthData(data, url).then((result: any) => {
      if (result.auth) {
        this.resetPassword(data);
      } else {
        let alert = this.alertCtrl.create({
          title: "Error",
          subTitle: result.msg,
          enableBackdropDismiss: false,
          buttons: ['OK']
        });
        alert.present();
      }
    })
  }

  resetPassword(data: any) {

    // console.log(data);
    let url = "user/reset_pwd";
    this.submitAttempt = true;

    this.authProvider.postAuthData(data, url).then((result: any) => {

      console.log(result);
      if (result.auth) {
        // this.navCtrl.push(LoginPage);
        let alert = this.alertCtrl.create({
          title: "Success",
          subTitle: result.msg,
          enableBackdropDismiss: false,
          buttons: ['OK']
        });

        alert.present();
      } else {
        let alert = this.alertCtrl.create({
          title: "Error",
          subTitle: result.msg,
          enableBackdropDismiss: false,
          buttons: ['OK']
        });

        alert.present();
      }
    }).catch((err) => {
      console.log(err);
    });

  }

  editProfile(data: any) {
    let url = "user/edit_user";
    //  this.submitAttempt = true;
    this.authProvider.postAuthData(data, url).then((res) => {
      //  console.log(res);
      this.resetSession();
      this.presentToast("You have updated your personal infomation successfully");
    });
  }

  resetSession() {
    this.session.getUser().then((res: any) => {
      this.user_data = this.session.user_data;
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad EditUserPage');
  }

  presentActionSheet() {
    let actionSheet = this.actionSheetCtrl.create({
      title: 'Edit Profile Photo',
      buttons: [
        {
          text: 'Pick From Gallery',
          icon: 'images',
          // role: 'destructive',
          handler: () => {
            this.takePicture(this.camera.PictureSourceType.PHOTOLIBRARY);
            console.log('Destructive clicked');
          }
        }, {
          text: 'Take a Picture',
          icon: 'camera',
          handler: () => {
            this.takePicture(this.camera.PictureSourceType.CAMERA);
            console.log('Archive clicked');
          }
        }, {
          text: 'Delete',
          icon: 'trash',
          // role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Cancel',
          role: 'cancel',
          icon: 'close',
          handler: () => {
            console.log('Cancel clicked');
          }
        }
      ]
    });
    actionSheet.present();
  }

  public takePicture(sourceType) {
    // Create options for the Camera Dialog
    var options = {
      quality: 50,
      sourceType: sourceType,
      saveToPhotoAlbum: true,
      correctOrientation: true,
      allowEdit: true,

    };

    // Get the data of an image
    this.camera.getPicture(options).then((imagePath) => {
      // Special handling for Android library
      if (this.platform.is('android') && sourceType === this.camera.PictureSourceType.PHOTOLIBRARY) {
        this.filePath.resolveNativePath(imagePath)
          .then(filePath => {
            let correctPath = filePath.substr(0, filePath.lastIndexOf('/') + 1);
            let currentName = imagePath.substring(imagePath.lastIndexOf('/') + 1, imagePath.lastIndexOf('?'));
            this.copyFileToLocalDir(correctPath, currentName, this.createFileName());
          });
      } else {
        var currentName = imagePath.substr(imagePath.lastIndexOf('/') + 1);
        var correctPath = imagePath.substr(0, imagePath.lastIndexOf('/') + 1);
        this.copyFileToLocalDir(correctPath, currentName, this.createFileName());
      }
    }, (err) => {
      this.presentToast('Error while selecting image.');
    });
  }

  // Create a new name for the image
  private createFileName() {
    var d = new Date(),
      n = d.getTime(),
      newFileName = n + ".jpg";
    return newFileName;
  }

  // Copy the image to a local folder
  private copyFileToLocalDir(namePath, currentName, newFileName) {
    // this.lastImage = null;
    this.file.copyFile(namePath, currentName, cordova.file.dataDirectory, newFileName).then(success => {
      this.lastImage = newFileName;
      // this.lastImage = this.pathForImage(this.lastImage);
      this.isServerImage = false;
      this.uploadImage();
    }, error => {
      this.presentToast('Error while storing file.');
    });
  }

  private presentToast(text) {
    let toast = this.toastCtrl.create({
      message: text,
      duration: 3000,
      position: 'top'
    });
    toast.present();
  }

  // Always get the accurate path to your apps folder
  public pathForImage(img) {
    if (img === null) {
      return '';
    } else {
      return cordova.file.dataDirectory + img;
    }
  }

  //upload image 
  public uploadImage() {
    var base_url = this.api.init();
    // Destination URL
    var url = base_url + "file_upload/dp_upload";

    // File for Upload
    var targetPath = this.pathForImage(this.lastImage);
    //  console.log(targetPath);
    // File name only
    var filename = this.lastImage;

    var options = {
      fileKey: "file",
      fileName: filename,
      chunkedMode: false,
      mimeType: "multipart/form-data",
      params: { 'fileName': filename, 'user_code': this.user_data.user_code }
    };

    const fileTransfer: TransferObject = this.transfer.create();

    this.loading = this.loadingCtrl.create({
      content: 'Uploading...',
    });
    this.loading.present();

    // Use the FileTransfer to upload the image
    fileTransfer.upload(targetPath, url, options).then(data => {
      this.resetSession();
      // console.log(data);
      this.loading.dismissAll()
      this.presentToast('Image succesful uploaded.');
    }, err => {
      console.log(err);
      this.loading.dismissAll()
      this.presentToast('Error while uploading file.');
    });
  }



}
