import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { EditUserPage } from '../edit-user/edit-user';
import {SessionProvider} from '../../providers/session/session';
import { AuthProvider } from '../../providers/auth/auth';
import { EntryPage } from '../entry/entry';
import {WinningsPage} from '../winnings/winnings';
/**
 * Generated class for the ProfilePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html',
})
export class ProfilePage {
  public user_data: any = [];
  entry_count:number = 0;
  win_count:number = 0;

  profile: string = "General";
  constructor(public auth:AuthProvider,public navCtrl: NavController, public navParams: NavParams,public session:SessionProvider) {
    this.session.getUser().then((res) => {
      this.user_data = this.session.user_data;
      console.log("lslslsls", this.session.user_data);

      this.getUserDashboard(this.session.user_data.user_code);
      // console.log("lslslsls",res);
    });
  }

  getUserDashboard(user_id:any){
    let url = "user/user_dash?user_code=" + user_id;
    this.auth.getData(url).then((res:any)=>{

      this.entry_count = res.entry_count[0].entry_count;
      this.win_count = res.win_count[0].win_count;
    })
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ProfilePage');
  }

  gotoEditPage(){
    this.navCtrl.push(EditUserPage);

    console.log("Edit page");
  }

  goEntry(){
    this.navCtrl.push(EntryPage);
  }

  goWinning(){
    this.navCtrl.push(WinningsPage);
  }

}
