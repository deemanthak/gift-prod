import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { AuthProvider } from '../../providers/auth/auth';
import { LoginPage } from '../login/login';
/**
 * Generated class for the ResetPwdPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-reset-pwd',
  templateUrl: 'reset-pwd.html',
})
export class ResetPwdPage {

  //form data
  private passwordData: FormGroup;
  submitAttempt: boolean = false;
  //user detail
  phoneNumber: any;
  // validator_data:any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public formBuilder: FormBuilder, public authProvider: AuthProvider,public alertCtrl:AlertController) {
    // this.phoneNumber = this.navParams.get('phoneNumber');
    this.phoneNumber = "94755653685";

    this.passwordData = this.formBuilder.group({
      new_pass: ['', Validators.compose([Validators.required])],
      conf_pass: ['', Validators.compose([Validators.required])],
      phoneNumber: [this.phoneNumber],
    }, { validator: this.passwordMismatch });
    // console.log(this.passwordData.controls);
    // this.validator_data = JSON.stringify(this.passwordData.controls);

  }


  passwordMismatch(cg: FormGroup): { [err: string]: any } {
    console.log("password checking")
    let pwd1 = cg.get('new_pass');
    let pwd2 = cg.get('conf_pass');
    let rv: { [error: string]: any } = {};
    if ((pwd1.touched || pwd2.touched) && pwd1.value !== pwd2.value) {
      rv['passwordMismatch'] = true;

    }
    // console.log(rv);
    return rv;
  }

  resetPassword(data: any) {
    // console.log(data);
    let url = "user/reset_password";
    this.submitAttempt = true;

    this.authProvider.postAuthData(data,url).then((result:any)=>{
      
      console.log(result);
      if(result.auth){
        this.navCtrl.push(LoginPage);
      }else{
        let alert = this.alertCtrl.create({
          title: "Error",
          subTitle: result.msg,
          enableBackdropDismiss:false,
          buttons: ['OK']
        });

        alert.present();
      }
    }).catch((err)=>{
      console.log(err);
    });

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ResetPwdPage');
  }

}
