import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, Slides, PopoverController, Events } from 'ionic-angular';
import { FullViewPage } from '../full-view/full-view';
import { AuthProvider } from '../../providers/auth/auth';
import { SessionProvider } from '../../providers/session/session';
import { Storage } from '@ionic/storage';
/**
 * Generated class for the HomePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
@Component({
	selector: 'page-home',
	templateUrl: 'home.html',
})
export class HomePage {
	base_url: any;
	cards: any = [];
	currentIndex: number = 0;
	prevI: number;
	last_slide: boolean = false;

	giveaway_list: any;

	liked_no: number;
	liked_val: any;

	className: string = "";
	clStatus: boolean = false;

	test_text: any;

	btnStatus: boolean = false;

	data: any = {};

	user_data: any = [];
	user_code: any;

	new_card_added: boolean = false;

	res_length: number = 0;
	@ViewChild(Slides) slides: Slides;

	constructor(public navCtrl: NavController, public navParams: NavParams,
		public popoverCtrl: PopoverController, public authProvider: AuthProvider,
		public session: SessionProvider, public storage: Storage, public events: Events
	) {

		events.subscribe('giveaway:added', (index) => {
			// console.log("got from event",index);
			this.grabIt(index);
		});

		this.session.getUser();
		this.user_data = this.session.user_data;
		this.user_code = JSON.stringify(this.user_data.user_code);
	}


	ionViewDidLoad() {

		console.log('ionViewDidLoad HomePage', );
	}

	slideChanged() {
		this.currentIndex = this.slides.getActiveIndex();
		// console.log("slide changed", this.currentIndex);
		// this.prevI = this.slides.getPreviousIndex();

		// if (this.slides.ionSlideReachEnd) {
		// 	console.log("last slide",this.slides.ionSlideReachEnd);
		// 	this.last_slide = true;
		// 	this.slides.lockSwipeToNext(true);
		// } else {
		// 	this.last_slide = false;
		// 	this.slides.lockSwipeToNext(false);
		// }

		if (this.slides.isEnd()) {
			console.log("end slide", this.slides.isEnd());
			this.last_slide = true;
			this.slides.lockSwipeToNext(true);
		} else {
			console.log("end slide", this.slides.isEnd());
			this.last_slide = false;
			this.slides.lockSwipeToNext(false);
		}
		// console.log('Current index is', currentIndex);

	}

	ngAfterViewInit() {

		this.addNewCards(5);
	}


	//add new cards to the stack
	addNewCards(count: number) {
		this.storage.get('user_id').then((res) => {
			let u_id = res;
			// let offset = this.slides.length();
			// console.log("user code",u_id );
			let url = "giveaway/giveaways?count=" + count + "&user_code=" + u_id;
			// + "&offset=" + offset;
			this.authProvider.getData(url).then((res: any) => {
				if (res.length <= 0) {
					console.log("result no")
					this.new_card_added = false;
					this.res_length = 0;
				} else {
					this.res_length = res.length;
					for (var i = 0; i <= res.length - 1; i++) {
						var push_res = this.cards.push(res[i]);
					}
					this.new_card_added = true;
				}
			});
		});
	}

	loadMore() {
		this.storage.get('user_id').then((res) => {
			let u_id = res;
			let limit = 3;
			let offset = this.slides.length();

			// console.log("slide_count",slide_count);
			// console.log("user code",u_id );
			let url = "giveaway/more_giveaways?limit=" + limit + "&user_code=" + u_id + "&offset=" + offset;
			this.authProvider.getData(url).then((res: any) => {
				if (res.length <= 0) {
					console.log("result no")
					this.new_card_added = false;
					this.res_length = 0;
				} else {
					this.res_length = res.length;
					for (var i = 0; i <= res.length - 1; i++) {
						var push_res = this.cards.push(res[i]);
					}
					this.new_card_added = true;
				}
			});
		});


	}

	endReached() {
		this.last_slide = true;
	}


	grabIt(id: any) {
		this.clStatus = true;
		this.btnStatus = false;
		let data = { giveaway_code: this.cards[id].giveaway_code, user_code: this.user_data.user_code };
		let url = "giveaway/giveaway_accept";
		this.authProvider.postAuthData(data, url).then(res => {
			setTimeout(() => {
				this.btnStatus = true;
				this.clStatus = false;

				this.cards.splice(id, 1);
			}, 1000);
				this.storage.get('user_id').then((res) => {
					let u_id = res;
					// let limit = 3;
					let offset = this.slides.length();
					let url = "giveaway/giveaways?count=" + 1 + "&user_code=" + u_id + "&offset=" + offset;
					this.authProvider.getData(url).then((res: any) => {
						if (res.length <= 0) {
							console.log("result no", this.cards.length);
							// this.slides.slideTo(this.cards.length-1, 1000);
							this.new_card_added = false;
							this.res_length = 0;
						} else {
							this.res_length = res.length;
							for (var i = 0; i <= res.length - 1; i++) {
								var push_res = this.cards.push(res[i]);
							}
							this.new_card_added = true;
						}
					});
				});

			
		}).then((es) => {
			if (this.last_slide && this.slides.isEnd()) {
				console.log("came to end", this.cards.length);

				if (this.cards.length == 5) {
					this.prevI = this.slides.getActiveIndex() + 1;
					this.slides.slideTo(this.cards.length - 1, 1000);
				} else {
					this.prevI = this.slides.getActiveIndex() - 1;
					this.slides.slideTo(this.prevI, 1000);
				}



				this.slides.lockSwipeToNext(true);
			} else {
				console.log("came to not end", this.cards.length, this.slides.getActiveIndex());
				// this.slides.slideTo(this.slides.getActiveIndex(), 1000);
				// this.slides.slideNext(1000);
			}
		});
	}


	grabIt2(id: any) {
		this.clStatus = true;
		this.btnStatus = false;

		let data = { giveaway_code: this.cards[id].giveaway_code, user_code: this.user_data.user_code };
		let url = "giveaway/giveaway_accept";
		this.authProvider.postAuthData(data, url).then((res: any) => {
			setTimeout(() => {
				this.btnStatus = true;
				this.clStatus = false;

				this.cards.splice(id, 1);

				this.slides.update(0);
				this.slides.resize();

				this.addNewCards(1);

				let added = this.new_card_added;

				this.slides.update(0);
				this.slides.resize();

				// if (this.slides.isEnd()) {

				// 	this.last_slide = true;
				// 	this.slides.lockSwipeToNext(true);
				// } else {
				// 	this.last_slide = false;
				// 	this.slides.lockSwipeToNext(false);
				// }
				console.log("res length", this.res_length);

				if (this.last_slide && this.res_length == 0) {
					this.slides.lockSwipeToNext(true);
					this.prevI = this.slides.getActiveIndex() - 1;
					this.slides.slideTo(this.prevI, 1000);
				}
				// else if(this.last_slide && this.res_length != 0 ){

				// 	this.prevI = this.slides.getActiveIndex() - 1;
				// 	this.slides.slideTo(this.prevI, 1000);
				// }
				console.log("this.slides.getActiveIndex()", this.slides.getActiveIndex());
				// if(this.last_slide)
				// if (added) {
				// 	if (this.last_slide) {
				// 		console.log("this is last slide", this.last_slide);

				// 		this.prevI = this.slides.getActiveIndex() - 1;
				// 		this.slides.slideTo(this.prevI, 1000);
				// 	} else {
				// 		this.slides.lockSwipeToNext(false);
				// 		// let next = this.slides.getActiveIndex();
				// 		// this.slides.slideTo(next,1000);
				// 	}

				// } else {
				// 	if (this.last_slide) {
				// 		this.slides.lockSwipeToNext(true);
				// 		this.prevI = this.slides.getActiveIndex() - 1;
				// 		this.slides.slideTo(this.prevI, 1000);
				// 	} else {
				// 		this.slides.lockSwipeToNext(false);
				// 		//  this.slides.slideTo(this.slides.getActiveIndex(),1000);
				// 		// this.slides.slideTo(this.slides.getActiveIndex() + 1,1000);
				// 	}
				// }
				this.slides.update(0);
				this.slides.resize();

				console.log("added", this.new_card_added);

			}, 1000)


		});
	}


	//get the index of the card and splice them.
	voteUp(id: any) {
		console.log("voteup added ");
		console.log("id index", id);
		this.clStatus = true;
		// const removedCard = this.cards.pop();
		this.btnStatus = false;
		setTimeout(() => {
			this.btnStatus = true;
			this.clStatus = false;

			this.liked_no = id;
			this.liked_val = this.cards[id];
			// console.log(this.cards[id].giveaway_code,"user code",this.user_data.user_code);
			let data = { giveaway_code: this.cards[id].giveaway_code, user_code: this.user_data.user_code };
			let url = "giveaway/giveaway_accept";
			this.authProvider.postAuthData(data, url).then((res: any) => {
				console.log(res.auth);
				this.cards.splice(id, 1);

				let added = this.addNewCards(1);
				// let added = true;
				// let added = false;
				// this.test_text = added;
				// console.log("added",added);
				if (added) {
					// this.last_slide=false;
					console.log("added", added, "last slide");
					if (this.last_slide) {
						this.slides.lockSwipeToNext(true);
						this.prevI = this.slides.getActiveIndex() - 1;
						this.slides.slideTo(this.prevI, 1000);
					} else {
						this.slides.lockSwipeToNext(false);
						// this.slides.slideTo(this.slides.getActiveIndex() + 1,1000);
					}
				} else {

					if (this.last_slide) {
						this.slides.lockSwipeToNext(true);
						this.prevI = this.slides.getActiveIndex() - 1;
						this.slides.slideTo(this.prevI, 1000);
					} else {
						this.slides.lockSwipeToNext(false);
						//  this.slides.slideTo(this.slides.getActiveIndex(),1000);
						// this.slides.slideTo(this.slides.getActiveIndex() + 1,1000);
					}
				}
				if (this.slides.ionSlideReachEnd) {
					this.slides.lockSwipeToNext(true);
				} else {
					this.slides.lockSwipeToNext(false);
				}

				this.slides.update(0);
				this.slides.resize();

				console.log("POPPED");
				console.log(this.slides.getActiveIndex());
				if (this.slides.length() <= 0) {
					this.test_text = "no more slides";
				}
			});

		}, 1000);

	}

	openFullView(g_id: any, currentIndex: any) {
		this.navCtrl.push(FullViewPage, { "giveaway_id": g_id, "index": currentIndex });
	}

	public tryme() {
		console.log("try me works");
	}

}