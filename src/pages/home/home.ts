import { Component, ViewChild } from '@angular/core';
import { App,NavController,LoadingController, NavParams, Slides, PopoverController, Events,ToastController } from 'ionic-angular';
import { FullViewPage } from '../full-view/full-view';
import { AuthProvider } from '../../providers/auth/auth';
import { SessionProvider } from '../../providers/session/session';
import { Storage } from '@ionic/storage';
import {SearchPage} from '../search/search';
@Component({
    selector: 'page-home',
    templateUrl: 'home.html',
})

export class HomePage {

    //giftin button
    showBtn:boolean = true;
    showEmptySlide:boolean = false;

    //index number related
    text_part:string="";
    index_no:string="";
    show_card:boolean = false;

    //cards array 
    cards: any = [];
    card_added:boolean = false;

    //slides array
    @ViewChild(Slides) slides: Slides;

    //slide related variables
    currentIndex: number = 0;


    //animation related
    clStatus: boolean = false;
    btnStatus: boolean = false;
    showSpinner:boolean = false;

    //other
    toast:any;
    loader:any;

    constructor(public navCtrl: NavController,
        public navParams: NavParams,
        public popoverCtrl: PopoverController,
        public authProvider: AuthProvider,
        public session: SessionProvider,
        public storage: Storage,
        public events: Events,
        private toastCtrl: ToastController,
        public app:App,
        public loadingCtrl:LoadingController,
    ) {

        
        events.subscribe('giveaway:added', (index) => {
			this.grabIt(index);
		});
    }

    presentLoading() {
        this.loader = this.loadingCtrl.create({
          content: "Please wait...",
          duration: 3000
        });
        this.loader.present();
      }

    //initiating functions
    ngAfterViewInit() {
        this.presentLoading();
        this.showSpinner = true;

        this.addNewCards(5);
        this.currentIndex = 0;
        
        console.log("after init current INdex" , this.currentIndex);
        this.loader.dismiss();
    }

    //add new cards to the array
    addNewCards(count: number) {
        this.storage.get('user_id').then((user) => {
            let u_id = user;
            let url = "giveaway/giveaways?count=" + count + "&user_code=" + u_id;
            this.authProvider.getData(url).then((giveaways: any) => {
                if (giveaways.length <= 0) {
                    console.log("no more new cards");
                    
                    this.checkSlideLength();
                } else {
                    for (var i = 0; i <= giveaways.length - 1; i++) {
                        var pushed = this.cards.push(giveaways[i]);
                    }
                   
                }
            })
        }).then(()=>{
            this.showSpinner = false;
            
        });
       
    }

    //this calles when slide change
    slideChanged() {
        this.currentIndex = this.slides.getActiveIndex();
        console.log("Active slide", this.currentIndex);

        if (this.slides.isEnd()) {
            // this.slides.slidePrev(1000,true);
            this.slides.lockSwipeToNext(true);
        } else {
            this.slides.lockSwipeToNext(false);
        }
    }

    slideCheckForce(card_added:any){
        this.currentIndex = this.slides.getActiveIndex();
        console.log("Active slide force check", this.currentIndex);
        let active_slide = this.currentIndex;
        if (this.slides.isEnd() && card_added == false) {
            this.slides.slideTo(this.slides.getActiveIndex()-1, 1000,true);
            this.currentIndex = this.slides.getActiveIndex();
            console.log("this.currentIndex after if" ,this.currentIndex);
            this.slides.lockSwipeToNext(true);
        } else {
            this.slides.lockSwipeToNext(false);
        }
    }

    //gift in button action
    grabIt(id: number) {
        this.clStatus = true;
        this.btnStatus = false;
        
        // console.log(this.cards[id]);
        this.storage.get('user_id').then((user_id) => {
            let data = { giveaway_code: this.cards[id].giveaway_code, user_code: user_id };
            let url = "giveaway/giveaway_accept";

            this.authProvider.postAuthData(data, url).then((accept_result: any) => {

                if (accept_result.auth == false) {

                }else{
                    this.text_part = this.cards[id].giveaway_title;
                    this.index_no = accept_result.index_no;
                    this.show_card = true;
                }

            }).then(() => {
                
                setTimeout(() => {
                    this.clStatus = false;
                    this.btnStatus = true;
                    this.cards.splice(id, 1);
                    let u_id = user_id;
                    let offset = this.cards.length;
                    let url = "giveaway/giveaways?count=" + 1 + "&user_code=" + u_id + "&offset=" + offset;

                    this.authProvider.getData(url).then((result_list: any) => {
                        if (result_list.length <= 0) {
                            this.card_added = false;
                            // this.slideCheckForce(this.card_added);
                        } else {
                            for (var i = 0; i <= result_list.length - 1; i++) {
                                var push_res = this.cards.push(result_list[i]);
                            }
                            this.card_added = true;
                            // this.slideCheckForce(this.card_added);
                        }
                    }).then(()=>{
                        this.slideCheckForce(this.card_added);
                        this.checkSlideLength();
                    });
                    this.show_card = false;
                }, 2500);
                // this.slideCheckForce(this.card_added);
                    // this.show_card = false;
            });
        });
    }

    //check slide length to identify last slide
    checkSlideLength(){
        console.log("slide length = " + this.slides.length());
        console.log("cards length = " + this.cards.length);

        if(this.slides.length() == 0 || this.cards.length == 0){
            this.showBtn = false;
            this.showEmptySlide = true;

            if(this.showEmptySlide == true){
                this.slides.lockSwipes(true);
            }else{
                this.slides.lockSwipes(false);
            }
        }else{
            this.showBtn = true;
            this.showEmptySlide = false;
        }
    }

    //loadMore function
    loadMore(){
        this.toastPresent("Loading new giveaways..");
        this.storage.get('user_id').then((res) => {
			let u_id = res;
			let limit = 3;
			let offset = this.cards.length;

			let url = "giveaway/more_giveaways?limit=" + limit + "&user_code=" + u_id + "&offset=" + offset;
			this.authProvider.getData(url).then((result_list_more: any) => {
                if (result_list_more.length <= 0) {
                    this.card_added = false;
                    // this.slideCheckForce(this.card_added);
                } else {
                    for (var i = 0; i <= result_list_more.length - 1; i++) {
                        var push_res = this.cards.push(result_list_more[i]);
                    }
                    this.card_added = true;
                }
			}).then(()=>{
                this.slideCheckForce(this.card_added);
                this.checkSlideLength();
            });
        });
        this.toast.dismiss();
    }

    //refresh function
    refreshCards(){
        this.navCtrl.setRoot(this.navCtrl.getActive().component);
    }

    openFullView(g_id: any, currentIndex: any) {
		this.navCtrl.push(FullViewPage, { "giveaway_id": g_id, "index": currentIndex });
    }
    
    toastPresent(txt:string){
        this.toast = this.toastCtrl.create({
            message: txt,
            // duration: 3000,
            position: 'bottom'
          });
          this.toast.present();
    }

    goToSearch(){
        this.navCtrl.push(SearchPage,{'mainview':false});
        // this.app.getRootNav().push(SearchPage);
    }
}

