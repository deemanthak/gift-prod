import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,AlertController,LoadingController } from 'ionic-angular';
import { SignupPage } from '../signup/signup';
import { FgtPassPage } from '../fgt-pass/fgt-pass';
import {ResetPwdPage} from '../reset-pwd/reset-pwd';
import { MenuPage } from '../menu/menu';
import {UserPage} from '../user/user';
import {TutorialPage} from '../tutorial/tutorial';

import {Validators, FormBuilder, FormGroup } from '@angular/forms';
import {AuthProvider} from '../../providers/auth/auth';
import {SessionProvider} from '../../providers/session/session';
import { Storage } from '@ionic/storage';

import firebase from 'firebase';
import { Facebook } from '@ionic-native/facebook';
import { Firebase } from '@ionic-native/firebase';
/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  //user info varialbe
  private user: FormGroup;
  submitAttempt:boolean = false;

  //facebook user data
  facebook_user:any;

  //user data to store at localstorage
  user_data_local:any;


  constructor(public navCtrl: NavController, public navParams: NavParams,
    public loadingCtrl: LoadingController,
    public alertCtrl: AlertController,
    private formBuilder: FormBuilder,
    public authProvider:AuthProvider,
    private facebook:Facebook,
    private fire:Firebase,
    public storage:Storage,
    public session:SessionProvider
  ) {

    this.user = this.formBuilder.group({
      email:['',Validators.compose([Validators.required, Validators.email])],
      password:['',Validators.required],

    });

  }

  //get logged in users data
  getUserData(user_id:any){

    let url = "user/user?id="+ user_id;
    console.log(url);
    this.authProvider.getData(url).then((result:any)=>{
      this.storage.set('user_id',result[0].user_code);
      this.storage.set('logged_in',true);
      console.log("user_data",result[0].user_code);

    })
  }


  //sign in function 
  signin(data:any){

    let loading = this.loadingCtrl.create({
      spinner: 'crescent',
      content: 'Loading Please Wait...'
    });

    loading.present();


    let url = "user/user";
    this.submitAttempt = true;
    this.authProvider.postAuthData(data,url).then((result:any)=>{
      loading.dismiss();
      // console.log(result);
      if(result.auth){
        // console.log(result.user_id);
        this.getUserData(result.user_id);
        this.storage.set('logged_in',true);
        this.storage.set('user_id',result.user_id);
        this.storage.get('fist_login').then((res)=>{
          console.log("gotdata from signin method");
          if(res){
            this.navCtrl.setRoot(UserPage,{'user_code':result.user_id});
          }else{
            this.session.hasSeenTutorial().then((hasSeen)=>{
              console.log("hasSeen",hasSeen);
              if(hasSeen){
                this.navCtrl.setRoot(MenuPage);
              }else{
                this.navCtrl.setRoot(TutorialPage);
              }
            })
           
          }
        })
      }else{
        let alert = this.alertCtrl.create({
          title: "Error",
          subTitle: result.msg,
          enableBackdropDismiss:false,
          buttons: ['OK']
        });

        alert.present();
      }
    }).catch((err)=>{
      console.log(err);
    });

    
  }

  //facebook native login
  signupFacebook(){
    let loading = this.loadingCtrl.create({
      spinner: 'crescent',
      content: 'Loading Please Wait...'
    });

    loading.present();
    // this.showLoader();
    this.facebook.login(["email","public_profile","user_friends"]).then((loginResponse)=>{

      this.facebook.api("/me?fields=email,name,picture,first_name,last_name,id,age_range,link,locale,gender",["public_profile","email","user_friends"]).then((result)=>{
          // console.log("fb api",JSON.stringify(result));
          this.facebook_user = {
            first_name :result.first_name,
            last_name : result.last_name,
            name:result.name,
            email:result.email,
            photo: result.picture.data.url,
            gender: result.gender,
            age:result.min,
            social_id:result.id,
            locale:result.locale,
            link:result.link,
          };

        });


      let credentials = firebase.auth.FacebookAuthProvider.credential(loginResponse.authResponse.accessToken);

      firebase.auth().signInWithCredential(credentials).then((res)=>{

        let data = this.facebook_user;
        let url = "social_user/social_users";
        console.log(JSON.stringify(data));

        this.authProvider.postAuthData(data,url).then((result_auth:any)=>{
          // this.loading.dismiss();
          console.log("ers",result_auth);
          loading.dismiss();
          if(result_auth.auth){
            //this.showAlert("Success",result_auth.msg);
            // this.navCtrl.setRoot(MenuPage);
            this.session.hasSeenTutorial().then((hasSeen)=>{
              console.log("hasSeen",hasSeen);
              if(hasSeen){
                this.navCtrl.setRoot(MenuPage);
              }else{
                this.navCtrl.setRoot(TutorialPage);
              }
            })
          }else{
            // this.showAlert("Error",result_auth.msg);
            let alert = this.alertCtrl.create({
              title: "Error",
              subTitle: result_auth.msg,
              enableBackdropDismiss:false,
              buttons: ['OK']
            });
            alert.present();
          }
        }).catch((err)=>{
          // this.loading.dismiss();
          console.log(err);
        });
      });
    }).catch((err)=>{
      // this.loading.dismiss();
    });
  }

//show alert function
  showAlert(title:any,msg:any){
    let alert = this.alertCtrl.create({
      title: title,
      subTitle: msg,
      enableBackdropDismiss:false,
      buttons: ['Continue']
    });

    alert.present();
  }

  //go to signup
  signUp(){
    this.navCtrl.push(SignupPage);
  }

  //go to reset password
  goToFgtPass(){
    this.navCtrl.push(FgtPassPage);
  }

  //

}
