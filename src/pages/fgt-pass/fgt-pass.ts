import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, LoadingController } from 'ionic-angular';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { Firebase } from '@ionic-native/firebase';
import firebase from 'firebase';
import { ResetPwdPage } from '../reset-pwd/reset-pwd';
import { LoginPage } from '../login/login';
/**
 * Generated class for the FgtPassPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-fgt-pass',
  templateUrl: 'fgt-pass.html',
})
export class FgtPassPage {

  //form data variables
  private user: FormGroup;
  submitAttempt: boolean = false;

  //otp verification process
  verificationId: any = '';
  otp: string = '';
  phone_number:any;
  //loding variable
  loader: any;


  constructor(public navCtrl: NavController, public navParams: NavParams, public fire: Firebase, public formBuilder: FormBuilder, public alertCtrl: AlertController, public loadCtrl: LoadingController) {
    this.user = this.formBuilder.group({
      // email:['',Validators.compose([Validators.required, Validators.email])],
      mobile_no: ['', Validators.compose([Validators.required, Validators.minLength(10)])],
    });
    this.loader = this.loadCtrl.create({
      spinner: 'crescent',
      content: "We are Setting things up to you.",

    });
  }

  sendCode(data: any) {
    this.showLoader();
    this.submitAttempt = true;
    let phoneNumber = data.mobile_no;
    this.phone_number = phoneNumber;
    this.fire.verifyPhoneNumber("+" + phoneNumber, 60).then((credential) => {

      this.verificationId = credential.verificationId;
      this.loaderDismiss();
      this.presentPrompt();
    }).catch((err) => {
      alert(JSON.stringify(err));
    })

  }


  //sfire base authentication 
  authFirebase() {
    var signInCredential = firebase.auth.PhoneAuthProvider.credential(this.verificationId, this.otp);
    firebase.auth().signInWithCredential(signInCredential).then((result: any) => {
      // console.log("otp data",JSON.stringify(result));
      // this.loaderDismiss();
      if (JSON.stringify(result).length > 0) {
        this.navCtrl.push(ResetPwdPage,{phoneNumber:this.phone_number});

      } else {
        let alert = this.alertCtrl.create({
          title: "Error",
          subTitle: "Wrong Passcode. Please check again",
          enableBackdropDismiss: false,
          buttons: [{
            text: 'OK',
          }]
        });

        alert.present();
      }

    }).catch((err) => {
      console.log('Erorr in OTP');
    });
  }

  presentPrompt() {
    let alert = this.alertCtrl.create({
      title: 'Enter Passcode',
      enableBackdropDismiss: false,
      inputs: [
        {
          name: 'otp',
          type: 'number',
          placeholder: 'Passcode'
        },

      ],
      buttons: [
        // {
        //   text: 'Cancel',
        //   role: 'cancel',
        //   handler: data => {
        //     console.log('Cancel clicked');
        //   }
        // },
        {
          text: 'Submit',
          handler: data => {
            // this.showLoader();

            if (data.otp != "") {
              this.otp = data.otp;
              this.authFirebase();
            } else {
              return false;
            }

          }
        }
      ]
    });
    alert.present();
  }

  showLoader() {

    this.loader.present();

  }

  loaderDismiss() {
    this.loader.dismiss();
  }

  goToLogin(){
    this.navCtrl.push(LoginPage);
  }

}
