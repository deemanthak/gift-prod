import { Component } from '@angular/core';
import {  NavController, NavParams } from 'ionic-angular';
import { AuthProvider } from '../../providers/auth/auth';
import { CompViewPage } from '../comp-view/comp-view';
/**
 * Generated class for the CompanyPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

// @IonicPage()
@Component({
  selector: 'page-company',
  templateUrl: 'company.html',
})
export class CompanyPage {

  giveaway_List: any = [];
  company:any="";
  company_data:any=[];
  company_logo:any = "";
  constructor(public navCtrl: NavController, public navParams: NavParams, public auth: AuthProvider) {
    let company_id = this.navParams.get('company_id');
    this.company = company_id;
    this.getCompanyGiveaways(company_id,0);
  }

  getCompanyGiveaways(company_id: any,offset:any) {
    // let offset = 0;
    let url = "giveaway/company_giveaways?com_id=" + company_id + "&offset=" + offset;
    this.auth.getData(url).then((giveaways:any) => {
      console.log(giveaways);
      if(giveaways.auth){
        giveaways.result.forEach(element => {
            this.giveaway_List.push(element);
          });
          this.company_data = giveaways.company[0];
         console.log(this.company_data);
         this.company_logo = "http://giveaway.treatpick.com/uploads/company_logo/" + giveaways.company[0].com_code +"/"+ giveaways.company[0].com_logo;
         console.log(this.company_logo);
        }
      // giveaways.forEach(element => {
      //   this.giveaway_List.push(element);
      // });
    })
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CompanyPage');
  }

  viewInfo(giveaway_code:any){
    this.navCtrl.push(CompViewPage,{"giveaway_code":giveaway_code});
  }

  doInfinite(infiniteScroll) {
    console.log('Begin async operation');
    this.getCompanyGiveaways(this.company,this.giveaway_List.length);
    setTimeout(() => {
      console.log('ENd async operation');
      infiniteScroll.complete();
    }, 1500);
  }


}
