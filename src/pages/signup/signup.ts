import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, LoadingController } from 'ionic-angular';
import { LoginPage } from '../login/login';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { AuthProvider } from '../../providers/auth/auth';
import firebase from 'firebase';
import { Facebook } from '@ionic-native/facebook';
import { Firebase } from '@ionic-native/firebase';
import { OtpPage } from '../otp/otp';

/**
 * Generated class for the SignupPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-signup',
  templateUrl: 'signup.html',
})
export class SignupPage {

  //variables for signup page

  //facebook user data
  facebook_user: any;

  //for form validation
  private user: FormGroup;
  submitAttempt: boolean = false;

  //show - hide password variable
  show_pass: boolean = false;

  //loader
  loader: any;

  //verification id for firebase phone code
  verificationId: any = '';

  //country list
  country_list: any = [];
  country_phone_code = "+";

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    private formBuilder: FormBuilder,
    public loadCtrl: LoadingController,
    private authProvider: AuthProvider,
    private facebook: Facebook,
    private fire: Firebase,
    public alertCtrl: AlertController,

  ) {

    this.getcountries();

    //set signup validation rules
    this.user = this.formBuilder.group({
      email: ['', Validators.compose([Validators.required, Validators.email])],
      mobile_no: ['', Validators.compose([Validators.required, Validators.minLength(8),])],//Validators.pattern('94[0-9]{9}')
      password: ['', Validators.required],
      country: [" ", Validators.required],
      phone_code: [" ", Validators.compose([Validators.required,Validators.pattern('[+][0-9]{1,5}')])],

    });

    //initialiaze loader
    this.loader = this.loadCtrl.create({
      spinner: 'crescent',
      content: "Sending OTP to your phone.",
    });

  }

  getcountries() {
    let url = "settings/countries"
    this.authProvider.getData(url).then((country_list: any) => {
      country_list.forEach(element => {
        this.country_list.push(element);
        // console.log(element);
      });
    })
  }

  getCountryPhone(country: any) {
    console.log("selected", country);
    // this.user.phone_code = "+" + country.phonecode;
    this.country_phone_code = "+" + country.phonecode;
  }

  signIn() {
    this.navCtrl.push(LoginPage);
  }

  //show hide password
  showPassword() {
    this.show_pass = !this.show_pass;
  }

  //handle phone number 
  createPhoneNumber(data:any){
    this.submitAttempt = true;
    let number = data.mobile_no;
    let code = this.country_phone_code;
    let phone_number = code.substr(1)+ number;
    console.log(code.substr(1),number);
    let verifyAlert = this.alertCtrl.create({
      title: "Verify Phone Number",
      subTitle: "We will be verifying the phone number : " + code + number + ". Is this OK, or would you like to edit the number?"  ,
      enableBackdropDismiss: false,
      buttons: [{
        text: 'EDIT',
        role: 'cancel',
        handler: () => {
          console.log('Cancel clicked');
        }
      },
      {
        text: 'OK',
        handler: () => {
         this.signup_process(data,phone_number);
        }
      }]
    });

    verifyAlert.present();
  }

  //signup process
  signup_process(data:any,phoneNumber:any){
    this.showLoader();
    // this.sendVerifyCode(phoneNumber);
    this.save_to_db(data,phoneNumber);
    // alert(sms_sent);
  }

  save_to_db(data:any,phoneNumber:any){
    console.log(data);
    let url = "user/users";
    this.authProvider.postAuthData(data, url).then((result: any) => {
      this.loaderDismiss();
      console.log(result);
      if (result.auth) {
        this.sendVerifyCode(phoneNumber);
      } else {
        this.showAlert("Error", result.msg);
      }
    }).catch((err) => {

      console.log(err);
    });
    
  }
  signup(data: any) {
    this.createPhoneNumber(data);
  }
  //signup function 
  signup2(data: any) {
    this.createPhoneNumber(data);
    // return;
    this.showLoader();
    this.submitAttempt = true;
    let url = "user/users";

    this.authProvider.postAuthData(data, url).then((result: any) => {
      this.loaderDismiss();
      console.log(result);
      if (result.auth) {
        this.sendVerifyCode(data.mobile_no);
      } else {
        this.showAlert("Error", result.msg);
      }
    }).catch((err) => {

      console.log(err);
    });

  }


  //send verification code 
  sendVerifyCode(phoneNumber: any) {
    this.fire.verifyPhoneNumber("+" + phoneNumber, 60).then((credential) => {
      
      
        this.verificationId = credential.verificationId;
      this.navCtrl.push(OtpPage, { verificationid: this.verificationId, phoneNumber: phoneNumber });
     
      
    }).catch((err) => {

      let alert = this.alertCtrl.create({
        title: "Error",
        subTitle: JSON.stringify(err),
        enableBackdropDismiss: true,
        buttons: ['OK']
      });

      alert.present();
      //  alert(JSON.stringify(err));
    })
  }

  //facebook native login
  signupFacebook() {
    this.showLoader();
    this.facebook.login(["email", "public_profile", "user_friends"]).then((loginResponse) => {
      this.facebook.api("/me?fields=email,name,picture,first_name,last_name,id,age_range,link,locale,gender", ["public_profile", "email", "user_friends"]).then((result) => {
        this.facebook_user = {
          first_name: result.first_name,
          last_name: result.last_name,
          name: result.name,
          email: result.email,
          photo: result.picture.data.url,
          gender: result.gender,
          age: result.min,
          social_id: result.id,
          locale: result.locale,
          link: result.link,
        };
      });
      let credentials = firebase.auth.FacebookAuthProvider.credential(loginResponse.authResponse.accessToken);
      firebase.auth().signInWithCredential(credentials).then((res) => {
        let data = this.facebook_user;
        let url = "social_user/social_users";
        this.authProvider.postAuthData(data, url).then((result_auth: any) => {
          if (result_auth.auth) {
            this.showAlert("Success", result_auth.msg);
          } else {
            this.showAlert("Error", result_auth.msg);
          }
        }).catch((err) => {
        });

      });
    }).catch((err) => {
      // this.loading.dismiss();
    });
  }

  //facebook web functionality
  signupFacebookWeb() {
    this.showLoader();
    let provider = new firebase.auth.FacebookAuthProvider();
    firebase.auth().signInWithPopup(provider).then((res) => {

      this.facebook_user = {
        first_name: res.additionalUserInfo.profile.first_name,
        last_name: res.additionalUserInfo.profile.last_name,
        name: res.additionalUserInfo.profile.name,
        email: res.additionalUserInfo.profile.email,
        phoneNumber: res.user.providerData.phoneNumber,
        photo: res.additionalUserInfo.profile.picture.data.url,
        gender: res.additionalUserInfo.profile.gender,
        age: res.additionalUserInfo.profile.age_range.min,
        provider: res.additionalUserInfo.providerId,
        social_id: res.additionalUserInfo.profile.id,
        locale: res.additionalUserInfo.profile.locale,
        link: res.additionalUserInfo.profile.link,
      };

      let data = this.facebook_user;
      let url = "social_user/social_users";
      this.authProvider.postAuthData(data, url).then((result_auth: any) => {

        if (result_auth.auth) {
          this.showAlert("Success", result_auth.msg);
        } else {
          this.showAlert("Error", result_auth.msg);
        }

      }).catch((err) => {
        console.log(err);
      });
    })
      .catch(function (err) {
        console.log(err);
        console.log("error", JSON.stringify(err));
      })
  }

  //show alert 
  showAlert(title: any, msg: any) {
    let alert = this.alertCtrl.create({
      title: title,
      subTitle: msg,
      enableBackdropDismiss: false,
      buttons: ['Continue']
    });

    alert.present();
  }

  //show the loader
  showLoader() {
    this.loader.present();
  }

  //loader dissmiss 
  loaderDismiss() {
    this.loader.dismiss();
  }


}
