import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { AuthProvider } from '../auth/auth';
/*
  Generated class for the SessionProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class SessionProvider {
public user_data:any = [];
public user_data_Ob:any = {};
  constructor(public storage: Storage, public authProvider: AuthProvider) {
    // console.log('Hello SessionProvider Provider');\
    this.checkLogIn().then((res)=>{
      if(res){
        this.getUser();
      }
    });

  }


  //check login status
  checkLogIn(): Promise<any> {
    return this.storage.get('logged_in').then((res) => {
      console.log("login status goooooos",res);
      return res;
    })
  }

  //check tutorial seen status
  hasSeenTutorial(): Promise<any> {
    return this.storage.get('hasSeenTutorial').then((res) => {
      // console.log("hasSeenTutorial",res);
      return res;
    })
  }


  getUserId(): Promise<any> {
    return this.storage.get('user_id').then((res) => {
      return res;
      });
  }
  
  //get logged in user data
  getUser(): Promise<any> {
    return this.storage.get('user_id').then((res) => {
      let url = "user/user?id=" + res;
      this.authProvider.getData(url).then((result: any) => {
        this.user_data.user_code = result[0].user_code;
        this.user_data.first_name = result[0].f_name;
        this.user_data.last_name = result[0].l_name;
        this.user_data.mobile_no = result[0].user_mobile_number;
        this.user_data.age = result[0].user_age;
        this.user_data.country = result[0].user_country;
        this.user_data.city = result[0].city;
        this.user_data.email = result[0].user_email;
        this.user_data.photo = result[0].user_photo;
      });
      // this.storage.set('user_photo',this.user_data.photo);
    })
    // return this.user_data;
  }

}
