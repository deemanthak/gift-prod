// import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import  {ApiProvider} from '../api/api';
import { Http,Headers } from '@angular/http';

/*
  Generated class for the AuthProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
  */
  @Injectable()
  export class AuthProvider {

  	//variables for auth provider 
  	post_url:any;


  	constructor(private apiProvider:ApiProvider,public http:Http) {
  		this.post_url = this.apiProvider.init();
  	}

  	//post authorized data function
  	//data: in array format, url: path to the function after post url

  	postAuthData(data:any,url:any){
      let headers = new Headers();
      headers.append('Content-Type','application/x-www-form-urlencoded;charset=utf-8');
      // headers.append('Access-Control-Allow-Origin','*');
      return new Promise((resolve,reject)=>{
        this.http.post(this.post_url + url, JSON.stringify(data),{headers:headers})
        .subscribe(res => {
          resolve(res.json());
           }, (error) => {
             reject(error);
           });
      });
    }


    //get data function
    //url_get: url after post url
    getData(url_get:any){
      return new Promise((resolve,reject)=>{
        this.http.get(this.post_url +url_get)
        .subscribe(res => {
          resolve(res.json());
        }, (error) => {
          reject(error);
        });
      })
    }


  }
