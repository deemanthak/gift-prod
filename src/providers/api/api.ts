// import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

/*
  Generated class for the ApiProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ApiProvider {

base_url: any;

  constructor() {
  	 this.base_url = "http://testapi.giftinapp.com/index.php/";
  	//  this.base_url = "http://localhost/giftin_rest_api/index.php/";

   
  }

    init(){
      return this.base_url;
    }

}
