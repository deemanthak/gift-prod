import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import {HttpModule} from '@angular/http';
import { IonicStorageModule } from '@ionic/storage';
import {Keyboard} from '@ionic-native/keyboard';
import { OneSignal } from '@ionic-native/onesignal';
import { SocialSharing } from '@ionic-native/social-sharing';

import { IonicImageLoader } from 'ionic-image-loader';
import { Network } from '@ionic-native/network';


import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { ListPage } from '../pages/list/list';
import { LoginPage } from '../pages/login/login';
import { SignupPage } from '../pages/signup/signup';
import { OtpPage} from '../pages/otp/otp';
import { FgtPassPage } from '../pages/fgt-pass/fgt-pass';
import { ResetPwdPage } from '../pages/reset-pwd/reset-pwd';
import { ProfilePage } from '../pages/profile/profile';
import  { MenuPage } from '../pages/menu/menu';
import { EditUserPage } from '../pages/edit-user/edit-user';
import { FullViewPage } from '../pages/full-view/full-view';
import { EntryPage } from '../pages/entry/entry';
import { WinningsPage } from '../pages/winnings/winnings';
import {CompViewPage } from '../pages/comp-view/comp-view';
import {UserPage} from '../pages/user/user';
import {CompanyPage} from '../pages/company/company';
import {SearchPage} from '../pages/search/search';
import {TutorialPage} from '../pages/tutorial/tutorial';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { ApiProvider } from '../providers/api/api';
import { AuthProvider } from '../providers/auth/auth';

//firebase implementation
import firebase from 'firebase';
//facebook native
import { Facebook } from '@ionic-native/facebook';
import { Firebase } from '@ionic-native/firebase';

//file upload and camera native
import { File } from '@ionic-native/file';
import { Transfer } from '@ionic-native/transfer';
import { FilePath } from '@ionic-native/file-path';
import { Camera } from '@ionic-native/camera';

import { SessionProvider } from '../providers/session/session';

var config = {
    apiKey: "AIzaSyD2c-44AhMs9xx8Dd7EvC0F-6Dmbzz6A-0",
    authDomain: "gift-in-app.firebaseapp.com",
    databaseURL: "https://gift-in-app.firebaseio.com",
    projectId: "gift-in-app",
    storageBucket: "gift-in-app.appspot.com",
    messagingSenderId: "538347117691"
  };

  firebase.initializeApp(config);
@NgModule({
  declarations: [
    MyApp,
    HomePage,
    ListPage,
    LoginPage,
    SignupPage,
    OtpPage,
    FgtPassPage,
    ResetPwdPage,
    ProfilePage,
    MenuPage,
    EditUserPage,
    FullViewPage,
    EntryPage,
    WinningsPage,
    CompViewPage,
    UserPage,
    CompanyPage,
    SearchPage,
    TutorialPage
  ],
  imports: [
    BrowserModule,
    HttpModule,
   
    IonicStorageModule.forRoot(),
    IonicImageLoader.forRoot(),
    // ionicBootstrap(MyApp, { scrollAssist: false, autoFocusAssist: false }),
    IonicModule.forRoot(MyApp,{
      // $ionicConfigProvider
      // scrollPadding: false,
      scrollAssist: false,
      autoFocusAssist: false
    }),
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    ListPage,
    LoginPage,
    SignupPage,
    OtpPage,
    FgtPassPage,
    ResetPwdPage,
    ProfilePage,
    MenuPage,
    EditUserPage,
    FullViewPage,
    EntryPage,
    WinningsPage,
    CompViewPage,
    UserPage,
    CompanyPage,
    SearchPage,
    TutorialPage
  ],
  providers: [
    StatusBar,
    Network,
    SplashScreen,
    Facebook,
    Keyboard,
    Firebase,
    File,
    Transfer,
    Camera,
    OneSignal,
    SocialSharing,
    FilePath,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    ApiProvider,
    AuthProvider,
    SessionProvider
  ]
})
export class AppModule {}
