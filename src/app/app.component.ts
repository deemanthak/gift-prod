import { Component, ViewChild } from '@angular/core';
import { Nav, Platform, AlertController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { Keyboard } from '@ionic-native/keyboard';
import { SplashScreen } from '@ionic-native/splash-screen';
import { OneSignal } from '@ionic-native/onesignal';


import { Network } from '@ionic-native/network';
import { Storage } from '@ionic/storage';

import { HomePage } from '../pages/home/home';
import { ListPage } from '../pages/list/list';
import { LoginPage } from '../pages/login/login';
import { SignupPage } from '../pages/signup/signup';
import { OtpPage } from '../pages/otp/otp';
import { FgtPassPage } from '../pages/fgt-pass/fgt-pass';
import { ResetPwdPage } from '../pages/reset-pwd/reset-pwd';
import { ProfilePage } from '../pages/profile/profile';
import { MenuPage } from '../pages/menu/menu';
import { EditUserPage } from '../pages/edit-user/edit-user';
import { FullViewPage } from '../pages/full-view/full-view';
import { EntryPage } from '../pages/entry/entry';
import { WinningsPage } from '../pages/winnings/winnings';
import { CompViewPage } from '../pages/comp-view/comp-view';
import { UserPage } from '../pages/user/user';
import { SessionProvider } from '../providers/session/session';
import { ImageLoaderConfig } from 'ionic-image-loader';
import { TutorialPage } from '../pages/tutorial/tutorial';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any;

  pages: Array<{ title: string, component: any }>;
  alert: any;

  constructor(public platform: Platform, public statusBar: StatusBar,
    public splashScreen: SplashScreen, public session: SessionProvider,
    public keyboard: Keyboard, private imageLoaderConfig: ImageLoaderConfig,
    private network: Network, public alertCtrl: AlertController,
    public storage: Storage) {
    this.initializeApp();
    this.getLoginStatus();

    // disable spinners by default, you can add [spinner]="true" to a specific component instance later on to override this
    imageLoaderConfig.enableSpinner(true);

    // set the maximum concurrent connections to 10
    imageLoaderConfig.setConcurrency(10);
    this.imageLoaderConfig.enableDebugMode();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      // this.splashScreen.show();
      this.statusBar.styleDefault();
      this.splashScreen.hide();

      var notificationOpenedCallback = function (jsonData) {
        console.log('notificationOpenedCallback: ' + JSON.stringify(jsonData));
      };

      window["plugins"].OneSignal
        .startInit("2b1fc1c6-828c-434a-b504-cd68458f042e", "538347117691")
        .handleNotificationOpened(notificationOpenedCallback)
        .endInit();

      // this.keyboard.disableScroll(true);

    });

    this.network.onDisconnect().subscribe(() => {
      this.alert = this.alertCtrl.create({
        title: 'Attention',
        subTitle: 'No Internet Connection. Please switch on Wifi or Mobile Data',
        // buttons: ['OK'],
        enableBackdropDismiss: false,
      });
      console.log('network was disconnected :-(');

      this.alert.present();

    });

    this.network.onConnect().subscribe(() => {

      console.log('network was connected :-)');
      this.alert.dismiss();
    });

  }

  //check login status
  getLoginStatus() {
    this.session.checkLogIn().then((status) => {
      if (status) {
        this.session.hasSeenTutorial().then((hasSeen) => {
          console.log("hasSeen", hasSeen);
          if (hasSeen) {
            this.rootPage = MenuPage;
          } else {
            this.rootPage = TutorialPage;
          }
        })
      } else {
        this.rootPage = LoginPage;
      }
    })
  }


}
